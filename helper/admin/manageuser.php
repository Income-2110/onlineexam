<?php
  require_once("header.php");
?>
    <style>
    .barredtabs
        {
            overflow:hidden;
            
        }
        
         .profile-pic{
                    height: 50px;
                    width: 50px;
             border-radius:50%;
                }
        
        .gobtn{
            margin-left: 5%;
           margin-top: 3%;
            padding-top: 1%;

        }
        
        .searchnav{
            margin-top: 2%;
            margin-bottom: 2%;
        }
        
    </style>
    <div class="container-fluid">
      <div class="row">
        <div class="panel panel-default col l6">
        <div class="col l12">
          <ul class="tabs barredtabs">
            <li class="tab col l3"><a class="active" href="#trainer">Trainer</a></li>
            <li class="tab col l3"><a  href="#trainee">Trainee</a></li>
            <li class="tab col l3"><a href="#batch">Batch</a></li>
           <li class="tab col l3"><a href="#resetaccount">Reset Account</a></li>
          </ul>
        </div>
            
            
        <div id="trainer" class="col s12">
            <div class="panel panel-default col l12">
   
          <div class="col l12 z-depth-6 card-panel">

    <nav class="col l8 searchnav">
    <div class="nav-wrapper">
      <form>
        <div class="input-field ">
          <input id="search" type="search" required>
          <label for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div> 
           
        </form>
      </div>
        </nav> &nbsp; &nbsp;
       <div class="btn waves-effect blue-color col l2 gobtn" type="submit" name="action" style="
    margin-left: 5%;
    margin-top: 3%;
    padding-top: 1%;

">Go </div>
 
        </div>
                
          <div class="col l12 z-depth-6 card-panel">

   
                <ul class="list-group">
                
                        <li class="list-group-item rank_list">
                    <div class="media">
                    <div class="media-left">
                        <a href="#">
                        <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                        </a>
                    </div>
                    
                    <div class="media-body">
                       <h4 class="media-heading">Myrna</h4>
                        <input class="with-gap" name="group2" type="radio" id="trainer_enabled"  /> 
                        <label for="trainer_enabled">Enable</label>
                        <input class="with-gap" name="group2" type="radio" id="trainer_disabled"  />
                        <label for="trainer_disabled">Disable</label>
                </div>
            <br/>
        </div>
      </li>
     
    </ul>
                </div>
  
</div>
</div>
            
            
    <div id="trainee" class="col s12">
              <div class="panel panel-default col l12">
                  
          <div class="col l12 z-depth-6 card-panel">

    <nav class="col l8 searchnav">
    <div class="nav-wrapper">
      <form>
        <div class="input-field ">
          <input id="search" type="search" required>
          <label for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div> 
           
        </form>
      </div>
        </nav> &nbsp; &nbsp;
       <div class="btn waves-effect blue-color col l2 gobtn" type="submit" name="action" style="
    margin-left: 5%;
    margin-top: 3%;
    padding-top: 1%;

">Go </div>
 
        </div>
                
          <div class="col l12 z-depth-6 card-panel">

      <ul class="list-group">
        <li class="list-group-item rank_list">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                            <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                            </a>
                        </div>

                    <div class="media-body">
                       <h4 class="media-heading">Myrna</h4>
                        <input class="with-gap" name="group2" type="radio" id="trainer_enabled"  /> 
                        <label for="trainer_enabled">Enable</label>
                        <input class="with-gap" name="group2" type="radio" id="trainer_disabled"  />
                        <label for="trainer_disabled">Disable</label>
                    </div>
                <br/>
            </div>
          </li>
           <li class="list-group-item rank_list">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                            <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                            </a>
                        </div>

                    <div class="media-body">
                       <h4 class="media-heading">Myrna</h4>
                        <input class="with-gap" name="group2" type="radio" id="trainer_enabled"  /> 
                        <label for="trainer_enabled">Enable</label>
                        <input class="with-gap" name="group2" type="radio" id="trainer_disabled"  />
                        <label for="trainer_disabled">Disable</label>
                    </div>
                <br/>
            </div>
          </li>


      </ul>
                  </div>
</div>
      </div>
            
            
    <div id="batch" class="col s12">
            <div class="panel panel-default col l12">
     
          <div class="col l12 z-depth-6 card-panel">

    <nav class="col l8 searchnav">
    <div class="nav-wrapper">
      <form>
        <div class="input-field ">
          <input id="search" type="search" required>
          <label for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div> 
           
        </form>
      </div>
        </nav> &nbsp; &nbsp;
       <div class="btn waves-effect blue-color col l2 gobtn" type="submit" name="action" style="
    margin-left: 5%;
    margin-top: 3%;
    padding-top: 1%;

">Go </div>
 
        </div>
                
          <div class="col l12 z-depth-6 card-panel">
            <ul class="list-group">
        <li class="list-group-item rank_list">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                        <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                        </a>
                    </div>
                    
                <div class="media-body">
                   <h4 class="media-heading">Myrna</h4>
                    <input class="with-gap" name="group2" type="radio" id="trainer_enabled"  /> 
                    <label for="trainer_enabled">Enable</label>
                    <input class="with-gap" name="group2" type="radio" id="trainer_disabled"  />
                    <label for="trainer_disabled">Disable</label>
                </div>
            <br/>
        </div>
      </li>
       <li class="list-group-item rank_list">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                        <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                        </a>
                    </div>
                    
                <div class="media-body">
                   <h4 class="media-heading">Myrna</h4>
                    <input class="with-gap" name="group2" type="radio" id="trainer_enabled"  /> 
                    <label for="trainer_enabled">Enable</label>
                    <input class="with-gap" name="group2" type="radio" id="trainer_disabled"  />
                    <label for="trainer_disabled">Disable</label>
                </div>
            <br/>
        </div>
      </li>
</ul>
                </div>
</div>
</div>
     
            
    <div id="resetaccount" class="col s12">
       <div class="col l12 z-depth-6 card-panel">

    <nav class="col l8 searchnav">
    <div class="nav-wrapper">
      <form>
        <div class="input-field ">
          <input id="search" type="search" required>
          <label for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div> 
           
        </form>
      </div>
        </nav> &nbsp; &nbsp;
       <div class="btn waves-effect blue-color col l2 gobtn" type="submit" name="action" style="
    margin-left: 5%;
    margin-top: 3%;
    padding-top: 1%;

">Go </div>
 
        </div>
          
          
         <!--- Put account settings--> 
 

             
                <div class="col l12 z-depth-6 card-panel">
                   
   
    <form class="col l12">
        
        
        <div class="input-field col l3">
          
          <label >Add Photo</label>
           
        </div>
             <div class="input-field col l8  width-photo ">
            <img src="noPhoto-icon.png" class="responsive-img bg-photo  " style="height:25%;"/>
                 <div class="btn waves-effect grey-color addphoto" style="height:6%;">Add Photo</div>  
          
               </div>  
    
        
        
        <div class="input-field col l7 ">
          <input id="first_name" type="text" class="validate">
          <label for="first_name">First Name</label>
        </div>
    
       
         
  
        <div class="input-field col l7">
          <input  id="password" type="password" class="validate">
          <label for="password">Old Password</label>
        </div>
       
        <div class="input-field col l7">
          <input  id="password" type="password" class="validate">
          <label for="password">New Password</label>
        </div>
        
         
        
        <div class="input-field col l7">
          <input  id="password" type="password" class="validate">
          <label for="password">Confirm new Password</label>
        </div>
      
        </form> 
                    <div class="btn waves-effect blue-color submitbutton" type="submit" name="action">Submit </div>
                     <div class="btn waves-effect grey-color clearbtn">Clear</div>
                      </div>
                   
               
 </div>

</div>
    </div>
      </div>
    
      <script>
        $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'tab_id');
  });
      </script>
	  <?php
  require_once("footer.php");
?>