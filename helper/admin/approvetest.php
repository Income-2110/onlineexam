<?php
  require_once("header.php");
?>
<div class="container-fluid">
        
  <div class="row">
    <div class="col l12">
      <ul class="tabs">
        <li class="tab col l3"><a href="#test1">Paper 1</a></li>
        <li class="tab col l3"><a class="active" href="#test2">Paper 2</a></li>
      
      </ul>
    </div>
    <div id="test1" class="col l12">Test 1
      <div class="card-panel">
               <div class="row">
                    <div class="col l8">
                         <div class="panel-heading blue-text text-darken-2 ">Answer Sheet</div>
                       
                        <div class="card-panel">
                        
                           <div class="panel-body">
                            <p>Question 1</p>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque minima nemo repudiandae rerum!                                        Aspernatur at, autem expedita id
                                       illum laudantium molestias officiis quaerat, rem sapiente sint totam velit. Enim.</p>
                               
                                <div class="red-text text-darken-2">option 1.</div></br>
                   <div class="black-text text-darken-2">option 2.</div></br>
                   <div class="green-text text-darken-2">option 3.</div></br>
                   <div class="black-text text-darken-2">option 4.</div></br>
                            </div>
                        </div>
            <div class="card-panel">
                        
                           <div class="panel-body">
                                <p>Q1] Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores cumque minima nemo repudiandae rerum!                                        Aspernatur at, autem expedita id
                                       illum laudantium molestias officiis quaerat, rem sapiente sint totam velit. Enim.</p>
                               
                   <div class="red-text text-darken-2">option 1.</div></br>
                   <div class="black-text text-darken-2">option 2.</div></br>
                   <div class="green-text text-darken-2">option 3.</div></br>
                   <div class="black-text text-darken-2">option 4.</div></br>
                            </div>
                        </div>
                     </div>
               </div>       
        
                
                    <div class="waves-effect waves-light btn closee-btn">Approve</div>
       </div></div>
    <div id="test2" class="col l12">Test 2</div>
    <div id="test3" class="col l12">Test 3</div>
    <div id="test4" class="col l12">Test 4</div>
  </div>      
       
</div>
<?php
  require_once("footer.php");
?>