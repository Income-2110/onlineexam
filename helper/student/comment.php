<style>
        /*Comment*/
         .profile-pic{
                    height: 50px;
                    width: 50px;
             border-radius:50%;
                }
        
        .text-light {
            color: #757575;
            }
        .comment{
           padding-left:10px;
        }
        .test_topic,.comment_adminname,.comment
        {
            color: #42a5f5;
        }
        /*Comment done*/
        
        
        /*Class Rank*/
        /*Individual Rank*/
         .rank-display
                {
                    padding-left:85%;
                    font-size: 1.6em;
                    color: #81C784;
                }
        
            .panel-default>.Current-rank-header
                {
                    background-color: #42A5F5;
                    Color:white;
                }
        
         .panel-default>.score-header
                {
                    background-color:#81C784;
                    Color:white;
                }
        
        
        .score_list
        {
            display: flex;
        }
        
        
        .score-display{
             padding-left: 67%;
            padding-top: 2%;
            font-size:1.2em;
            color: #42A5F5;
        }
        
        /*End rank*/
    </style>
    </head>
    
    <body>
      
             <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
          <script type="text/javascript" src="materialize-v0.97.4/materialize/js/materialize.min.js"></script>
        
            <div class="row">
            <div class="col l5">
                     <div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h4 class="comment">Comments</h4></div>
   
                
        <ul class="list-group">
    <li class="list-group-item"><div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
    </a>
  </div>
  <div class="media-body">
   
   <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
      <div class="text-light">
                          Topic: <span class ="test_topic">AngularJS </span> &nbsp; By: <span class ="comment_adminname">Adrian Demian
                        </div>
  </div>
</div></li>
     <li class="list-group-item"><div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
    </a>
  </div>
  <div class="media-body">
   
   <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
      <div class="text-light">
                          Topic: <span class ="test_topic">AngularJS </span> &nbsp; By: <span class ="comment_adminname">Adrian Demian
                        </div>
  </div>
</div></li>
  </ul>
</div>
     </div>
        </div>
        
     <!--Class Rank-->
                        <div class="container">
             <div class="row">
                 <div class="col l5">
                <div class="panel panel-default">
  <div class="panel-heading Current-rank-header"> 
      <h2 class="panel-title Current-rank-title">Class Ranking</h2></div>
                    
        <ul class="list-group">
    <li class="list-group-item rank_list">
        <div class="media">
            <div class="media-left">
                    <a href="#">
                        <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                    </a>
                </div>
             
            <div class="media-body">
               
                   <h4 class="media-heading">Myrna</h4>
                         
                            <div class="rank-display">#1</div>
                            
                       
              
                </div>
            <br/>
           
</div>
            </li>
            
             <li class="list-group-item rank_list">
        <div class="media">
            <div class="media-left">
                    <a href="#">
                        <img class="media-object profile-pic" src="D:\OnlineExamPortal\Sample Pictures\Penguins.jpg" alt="...">
                    </a>
                </div>
            <div class="media-body">
                <h4 class="media-heading">Myrna</h4>
                   
                         
                            <div class="rank-display">#1</div>
                            
                       
              
                </div>
            <br/>
           
</div>
            </li>
        </ul>
</div>
                 </div>
                    </div>
                </div>
        <!-- Class Rank end-->
        
        <!--Individual Rank-->
           <div class="container">
             <div class="row">
                 <div class="col l5">
               <div class="panel panel-default">
 
   <div class="panel-heading score-header">Test Scores</div>
 

  <!-- List group -->
  <ul class="list-group">
    <li class="list-group-item score_list"><h5 class="test_name_header">Cras justo odio</h5>
        
                            <div class="score-display">11</div>
                            
                       
      </li>
      
        <li class="list-group-item score_list">Cras justo odio
   
                            <p class="score-display">25</p>
                            
      </li>
      
                   </ul>

  </div>
</div>
                 </div>
                    </div>
                
        <!--Individual Rank end-->