<style>
        .test_end
        {
         color: #42a5f5;
            font:bold;
        }
        .submittest_msg
        {
            padding-left:5%;
        }
        
        .objective_result
        {
            
        }
        
        .trainee_score
        {
            color: #42a5f5;
            font:bold;
        }
        
        .testend_btn
        {
           padding-left:5%; 
            padding-bottom: 4%;
        }
    </style>
        </head>

        <body>
             <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
            <script type="text/javascript" src="bootstrap-3.3.6-dist/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
          <script type="text/javascript" src="materialize-v0.97.4/materialize/js/materialize.min.js"></script>
        
            
    
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Submit</button>
            <div class="row">
                <div class="col l4">
                    <div class=" modal fade bs-example-modal-lg modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title test_end" id="gridSystemModalLabel">Test Submitted</h4>
                        </div>
                            <div class="submittest_msg">
                                <p class=" commonmsg">Congratulations your test is done and this is your score</p>
                                <p class="objective_result">Objective : <span class="trainee_score" id="trainee_score">12/13</span></p>
                            </div>
                            <div class="testend_btn">
                                  <div class="waves-effect waves-light btn viewans-btn">View Answers</div> &nbsp;
                                  <div class="waves-effect waves-light btn close-btn" data-dismiss="modal">Close</div>
                            </div>
                        </div>
                </div>
    </div>