<script  src='D:\OnlineExamPortal\Chart.js-master\Chart.js'></script>
    <script>
        var buyerData = {
            labels : ["Test 1","Test 2","Test 3","Test 4"],
            datasets : [
                {
                    fillColor : "#FFB74D",
                    strokeColor : "#DECF3F ",
                    pointColor : "#B276B2",
                    pointStrokeColor : "#B276B2",
                    data : [20,18,27,16],
                    scaleStepWidth:null,
                    showScale: true,
                    scaleShowLabels: true,
                    scaleBeginAtZero: false,
                }
       
            ]
        }
         
    </script>
    
    <style>
    .testresult_graph
        {
            width:300px;
            height:300px;
        }</style>
    </head>
<body>
    <div class="card-panel">
               <div class="row">
                    <div class="col l8">
<canvas id="buyers" class="testresult_graph"></canvas>

    <script>
        var buyers = document.getElementById('buyers').getContext('2d');
        var myChart=new Chart(buyers).Line(buyerData);
        myChart.addData([17], "Test 5");
    </script>
                   </div>
        </div>
    </div>