<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Chart.js demo</title>
    <script  src='Chart.js-master/Chart.min.js'></script>
    <script>
       
            var buyerData = {
                labels: ["Test 1 ","Test 2","Test 4 ","Test 4","Test 5 ","Test 6","Test 7 "],
                datasets : [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [28, 48, 40, 19, 86, 27, 60]
                    }		]
            };

     
        function addAnotherData()
        {
            var newdataset=[];
            newdataset= {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            };
            
            alert('before psuh');
          //  newchrt.datasets.push(newdataset);
            buyerData.datasets.push(newdataset);
           // newchrt.datasets.push({buyerData.datasets.push(newdataset)});
            newchrt.update();
            alert('Updated');

        }

    </script>
    </head>
    <body>
        <!--<input type="button" value="Progress" onclick="addTestAverage()" />-->

        <canvas id="devexp" width="600" height="400"></canvas>
        <input type="button" value="Batch1" onclick="addAnotherData()"/>
        <script>

            var devx = document.getElementById('devexp').getContext('2d');
            var newchrt=new Chart(devx).Line(buyerData); 
        </script>
    </body>
</html>