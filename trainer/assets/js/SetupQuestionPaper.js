/*** JS for Setup Question Paper ***/
//Save question individually
$("#btnSaveQuestion_SetupQuestionPaper").click( function(){
	/* Get user data */
	//alert("hi");
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();
	var questionContent = $("#questionContent_SetupQuestionPaper").val().trim();

	//var difficultyLevelId = $("#difficultyLevel_SetupQuestionPaper :selected").text();
	var difficultyLevelId = $(".radio-input_difficulty").attr('id');

	var marksAlloted = $("#marksPerQuestion_SetupQuestionPaper").val();
	var isObjective = $("#radioObjectiveQuestion_SetupQuestionPaper").is(':checked'); // true/false

	//Clear all the error message lables
	$("#errorMessage_SetupQuestionPaper").empty();

	
	/* Validate user data */
	var isError = false;
	var questionReg =/^$/;
	if(questionReg.test(questionContent))
	{
		$("#errorMessage_SetupQuestionPaper").append("Question is not valid<br/>");
		isError= true;
	}
	
	var marksReg = /^[0-9]+$/;
	if(!marksReg.test(marksAlloted) || marksReg.test(marksAlloted)<0)
	{
		$("#errorMessage_SetupQuestionPaper").append("Marks not valid<br/>");
		isError= true;
	}

	var difficutyradio = $(".radio-input_difficulty").is(':checked');
	if(!difficutyradio)
	{
		$("#errorMessage_SetupQuestionPaper").append("Set the difficulty Level!");
		isError = true;
	}

	if(isObjective)
	{
		var option1 = $("#optionOne_SetupQuestionPaper").val();
		var option2 = $("#optionTwo_SetupQuestionPaper").val();
		var option3 = $("#optionThree_SetupQuestionPaper").val();
		var option4 = $("#optionFour_SetupQuestionPaper").val();
		
		var answer1 = $("#correctAnswerOne_SetupQuestionPaper").is(':checked'); 
		var answer2 = $("#correctAnswerTwo_SetupQuestionPaper").is(':checked'); 
		var answer3 = $("#correctAnswerThree_SetupQuestionPaper").is(':checked'); 
		var answer4 = $("#correctAnswerFour_SetupQuestionPaper").is(':checked'); 
	
		var optionReg =/^$/;
		if(optionReg.test(option1))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 1<br/>");
			isError= true;
		}
		if(optionReg.test(option2))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 2 <br/>");
			isError= true;
		}
		if(optionReg.test(option3))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 3<br/>");
			isError= true;
		}
		if(optionReg.test(option4))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 4<br/>");
			isError= true;
		}
		
		if((option1==option2)||(option1==option3)||(option1==option4)||(option2==option3)||(option2==option4)||(option3==option4))
		{
			$("#errorMessage_SetupQuestionPaper").append("Options cannot be same</br>");
		}
	
	
		if(answer1||answer2||answer3||answer4)
		{
			
		}
		else
		{
			$("#errorMessage_SetupQuestionPaper").append("Select the correct option!");
		}
		var questionDetail = 
			{"difficultyLevelId":difficultyLevelId,
			"marksAlloted":marksAlloted,
			"questionContent":questionContent,
			"isObjective":isObjective,
			"option1":option1,
			"option2":option2,
			"option3":option3,
			"option4":option4,
			"answer1":answer1,
			"answer2":answer2,
			"answer3":answer3,
			"answer4":answer4
			};
			 var question1 = JSON.stringify(questionDetail);
	}
	
	if(isError)
		return false;
	if(!isObjective)
	{
		var questionDetail = 
			{
		    "isObjective":isObjective,
			"difficultyLevelId":difficultyLevelId,
			"marksAlloted":marksAlloted,
			"questionContent":questionContent
			};
			var question1 = JSON.stringify(questionDetail);
			//var questionDetail1 = JSON.parse(questionDetail);
			//alert(question1);
	}
	//Set as JSON
	
	
	/* Post data to server async */
	$.post("ajax/saveSetupQuestionPaper.php",
		{
			question:question1
		},
		function(responseData){
			//alert(response);
			var data = JSON.parse(responseData);
			var response = data['response'];
			if(response == "Done"){
				alert("yay, done");
				//$("#questionPaper_SetupQuestionPaper").html(data['questionPaper']);
			}
			else{
				alert("fail");
			}
		}
	
	);
}
);

// clear question fields
$("#btnClearQuestion_SetupQuestionPaper").click( function(){

	//Clear all the fields, no ajax
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();
	var questionContent = $("#questionContent_SetupQuestionPaper").val();
	var difficultyLevelId = $("#difficultyLevel_SetupQuestionPaper").val();
	var marksAlloted = $("#marksPerQuestion_SetupQuestionPaper").val();
	var isObjective = $("#radioObjectiveQuestion_SetupQuestionPaper").is(':checked'); // true/false
	
	
	$("#questionContent_SetupQuestionPaper").val('');

	$("#difficultyLevel_SetupQuestionPaper").contents().remove();
	
	$("#marksPerQuestion_SetupQuestionPaper").val('');
	
	$("#isObjective").val('');
	
	$("#errorMessage_SetupQuestionPaper").empty();
	
	$(".radio-input_difficulty").prop('checked', false);
	

	

	//If objective question, clear those fields also

	if(isObjective){
		var option1 = $("#optionOne_SetupQuestionPaper").val();
		var option2 = $("#optionTwo_SetupQuestionPaper").val();
		var option3 = $("#optionThree_SetupQuestionPaper").val();
		var option4 = $("#optionFour_SetupQuestionPaper").val();
		
		$("#optionOne_SetupQuestionPaper").val('');
		$("#optionTwo_SetupQuestionPaper").val('');
		$("#optionThree_SetupQuestionPaper").val('');
		$("#optionFour_SetupQuestionPaper").val('');
		
		
		var answer1 = $("#correctAnswerOne_SetupQuestionPaper").is('checked'); 
		var answer2 = $("#correctAnswerTwo_SetupQuestionPaper").is(':checked'); 
		var answer3 = $("#correctAnswerThree_SetupQuestionPaper").is(':checked'); 
		var answer4 = $("#correctAnswerFour_SetupQuestionPaper").is(':checked'); 
		
		$('#correctAnswerOne_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerTwo_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerThree_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerFour_SetupQuestionPaper').attr('checked',false);
		
		$('input:radio[name="radio-category"][id="radioSubjectiveQuestion_SetupQuestionPaper"]').prop('checked', true);
		$(".options").hide();
		
		$("#correctAnswerOne_SetupQuestionPaper").removeClass("valid");
		$("#correctAnswerTwo_SetupQuestionPaper").removeClass("valid");
		$("#correctAnswerThree_SetupQuestionPaper").removeClass("valid");
		$("#correctAnswerFour_SetupQuestionPaper").removeClass("valid");
		
		
	}	

	$('label[id*="errorMessage_SetupQuestionPaper"]').text('');

	}
);

// delete question 
$("#btnDeleteQuestion_SetupQuestionPaper").click( function(){
	// delete the question //ajax//, refresh the question paper
	//alert("hi");
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();

	$.post("ajax/setupQuestionPaper.php",
		
		function(responseData){

			var data = JSON.parse(responseData);
			var response = data['response'];
				//alert(response);
			if(response == "Done"){
				//alert("yay, done");
				$("#questionPaper_SetupQuestionPaper").html(data['questionPaper']);
			}
			else{
				alert("Fail"+response);
			}
		}
	);
}
);

//Subjective question animation
$("#radioObjectiveQuestion_SetupQuestionPaper").click(function(){
	$(".options").removeClass("hide");
	$(".options").show();
});

$("#radioSubjectiveQuestion_SetupQuestionPaper").click(function(){
	$(".options").hide();
});


$('#questionPaper_SetupQuestionPaper').on('click', '.Edit_SubjQuestionPaper', function(){

//edit button in question papaer

	
	var questionid = this.id;
	alert(questionid);
	$.post("ajax/editquestionpapaper.php",
	{
		qid:questionid
	},
	function(response){
		//alert("hi");
		var data = JSON.parse(response);
		
		var mark = parseInt(data['marks']);
		$("#marksPerQuestion_SetupQuestionPaper").val(mark);
		$("#questionContent_SetupQuestionPaper").focus().val(data['question']);
		
		//var did = data['difficulty_level-id'];
		//$('#data['difficulty_level-id']').prop('checked',true);
		$(".radio-input_difficulty").val(data['difficulty_level-id']);
		$(".radio-input_difficulty").prop('checked', true);
	}
	);
});
$('.btnSavePaper_SetupQuestionPaper').click(function(){
	window.location.assign("SetupTest.php");
});