// Custom JS file

/*** JS for Trainer Dashboard ***/
/*
$(".TraineeNamePsid_Dashboard").click( function(){
	var student_Psid = this.id;
	
	var name = $("#StudentNameDisplay" + student_Psid).val();
	
	$.post("ajax/DashboardFunc.php",
	{
		name : name
	},
	function(response){
		if(response == "Done"){
			//sucessful mark and comment saved
			alert("done");
		}
		else{
			alert("fail");
		}
	}
	);
}
);
*/
	
$(".TraineeNamePsid_Dashboard").click(function(){
	marksData = {
					labels: [],
					datasets : [
						{
							fillColor: "#FF8C00",
							strokeColor: "#ACC26D",
							pointColor: "#fff",
							pointStrokeColor: "#9DB86D",
							data: [],
							showScale: true,
							scaleShowLabels: true,
							scaleBeginAtZero: true,
						}   ]
				};

	//marksId = document.getElementById("buyers").getContext("2d"); // get canvas
	//myChart = new Chart(marksId).Line(marksData,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 10, scaleSteps: marks_array['steps'] }); // make a chart
		
	
	var TraineePSID_Dashboard = this.id; //trainee id

	var x;
 
	$.post("ajax/DashboardGraph.php",
		{
			TraineePSID_Dashboard: TraineePSID_Dashboard
		},
		function(response){
			
	
			var marks_array = JSON.parse(response);
			
			marksId = document.getElementById("buyers").getContext("2d"); // get canvas
			myChart = new Chart(marksId).Line(marksData,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: marks_array['steps'], scaleSteps: marks_array['value'] }); // make a chart
	
			for(i = 0; i < marks_array['marks'].length; i++)
			{
				x = parseInt(marks_array['marks'][i]);
				myChart.addData([x],"Test" + (i+1));
			}
			
		
		}
	
	)
		
}
);


/*** JS for Setup Question Paper ***/
//Save question individually
$("#btnSaveQuestion_SetupQuestionPaper").click( function(){
	/* Get user data */
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();
	var questionContent = $("#questionContent_SetupQuestionPaper").val().trim();
	var difficultyLevelId = $("#difficultyLevel_SetupQuestionPaper").val();
	var marksAlloted = $("#marksPerQuestion_SetupQuestionPaper").val();
	
	var isObjective = $("#radioObjectiveQuestion_SetupQuestionPaper").is(':checked'); // true/false

	//Clear all the error message lables
	$("#errorMessage_SetupQuestionPaper").empty();

	
	/* Validate user data */
	var isError = false;
	var questionReg =/^$/;
	if(questionReg.test(questionContent))
	{
		$("#errorMessage_SetupQuestionPaper").append("Question is not valid<br/>");
		isError= true;
		alert("error");
	}
	
	var marksReg = /^[0-9]+$/;
	if(!marksReg.test(marksAlloted))
	{
		
		$("#errorMessage_SetupQuestionPaper").append("Marks not valid<br/>");
		isError= true;

	}


	if(isObjective)
	{
		var option1 = $("#optionOne_SetupQuestionPaper").val();
		var option2 = $("#optionTwo_SetupQuestionPaper").val();
		var option3 = $("#optionThree_SetupQuestionPaper").val();
		var option4 = $("#optionFour_SetupQuestionPaper").val();
		
		var answer1 = $("#correctAnswerOne_SetupQuestionPaper").is(':checked'); 
		var answer2 = $("#correctAnswerTwo_SetupQuestionPaper").is(':checked'); 
		var answer3 = $("#correctAnswerThree_SetupQuestionPaper").is(':checked'); 
		var answer4 = $("#correctAnswerFour_SetupQuestionPaper").is(':checked'); 
	
		var optionReg =/^$/;
		if(optionReg.test(option1))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 1<br/>");
			isError= true;
		}
		if(optionReg.test(option2))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 2 <br/>");
			isError= true;
		}
		if(optionReg.test(option3))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 3<br/>");
			isError= true;
		}
		if(optionReg.test(option4))
		{
			$("#errorMessage_SetupQuestionPaper").append("Enter the option 4<br/>");
			isError= true;
		}
		
		if((option1==option2)||(option1==option3)||(option1==option4)||(option2==option3)||(option2==option4)||(option3==option4))
		{
			$("#errorMessage_SetupQuestionPaper").append("Options cannot be same</br>");
		}
	
	
		if(answer1||answer2||answer3||answer4)
		{
			
		}
		else
		{
			$("#errorMessage_SetupQuestionPaper").append("Select the correct option!");
		}
	}
	
	if(isError)
		return false;

	//Set as JSON
	//answer should be in csv

	/*
	var questionDetail = [
			{"difficultyLevelId":difficultyLevelId},
			{"marksAlloted":marksAlloted},
			{"questionContent":questionContent},
			{"isObjective":isObjective},
			{"option1":option1},
			{"option2":option2},
			{"option3":option3},
			{"option4":option4},
			{"answer1":answer1},
			{"answer2":answer2},
			{"answer3":answer3},
			{"answer4":answer4}
			];
	*/
	/* Post data to server async */
	$.post("ajax/setupQuestionPaper.php",
		{
			question: 1
		},
		function(response){
			//alert(response);
			if(response == "Done"){
				alert("yay, done");
			}
			else{
				alert("fail" + response);
			}
		}
	
	);
}
);

// clear question fields
$("#btnClearQuestion_SetupQuestionPaper").click( function(){
	//Clear all the fields, no ajax
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();
	
	var questionContent = $("#questionContent_SetupQuestionPaper").val();
	var difficultyLevelId = $("#difficultyLevel_SetupQuestionPaper").val();
	var marksAlloted = $("#marksPerQuestion_SetupQuestionPaper").val();
	var isObjective = $("#radioObjectiveQuestion_SetupQuestionPaper").is(':checked'); // true/false
	
	$("#questionContent_SetupQuestionPaper").val('');
	$("#difficultyLevel_SetupQuestionPaper").val('0');
	$("#marksPerQuestion_SetupQuestionPaper").val('');
	$('#marksPerQuestion_SetupQuestionPaper').removeClass("validate");
	$("#isObjective").val('');
	//$(".difficultyLevels").hide();
	//$(".dropdown-content select dropdown").hide();
	//$('#difficultyLevel_SetupQuestionPaper').siblings('.option value=""').text()
	$("#difficultyLevel_SetupQuestionPaper").val('#cyo');
	

	//If objective question, clear those fields also
	if(isObjective){
		var option1 = $("#optionOne_SetupQuestionPaper").val();
		var option2 = $("#optionTwo_SetupQuestionPaper").val();
		var option3 = $("#optionThree_SetupQuestionPaper").val();
		var option4 = $("#optionFour_SetupQuestionPaper").val();
		
		$("#optionOne_SetupQuestionPaper").val('');
		$("#optionTwo_SetupQuestionPaper").val('');
		$("#optionThree_SetupQuestionPaper").val('');
		$("#optionFour_SetupQuestionPaper").val('');
		
		var answer1 = $("#correctAnswerOne_SetupQuestionPaper").is('checked'); 
		var answer2 = $("#correctAnswerTwo_SetupQuestionPaper").is(':checked'); 
		var answer3 = $("#correctAnswerThree_SetupQuestionPaper").is(':checked'); 
		var answer4 = $("#correctAnswerFour_SetupQuestionPaper").is(':checked'); 
		
		$('#correctAnswerOne_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerTwo_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerThree_SetupQuestionPaper').attr('checked',false);
		$('#correctAnswerFour_SetupQuestionPaper').attr('checked',false);
		
		$('input:radio[name="radio-category"][id="radioSubjectiveQuestion_SetupQuestionPaper"]').prop('checked', true);
		$(".options").hide();
		
		
		
		
	}	
	$('label[id*="errorMessage_SetupQuestionPaper"]').text('');
	$('input:radio[name="radio-category_difficulty"][id="difficultyLevelEasy_SetupQuestionPaper"]').prop('checked', false);
	$('input:radio[name="radio-category_difficulty"][id="difficultyLevelMedium_SetupQuestionPaper"]').prop('checked', false);
	$('input:radio[name="radio-category_difficulty"][id="difficultyLevelDifficult_SetupQuestionPaper"]').prop('checked', false);
	
}
);

// delete question 
$("#btnDeleteQuestion_SetupQuestionPaper").click( function(){
	// delete the question //ajax//, refresh the question paper
	alert("hi");
	var question_counter = $("#questionNumber_SetupQuestionPaper").val();
	
	$.post("ajax/setupQuestionPaper.php",
		{
			question: 1
		},
		function(responseData){
			//alert(response);
			var data = JSON.parse(responseData);
			var response = data['response'];
			if(response == "Done"){
				alert("yay, done");
				$("#questionPaper_SetupQuestionPaper").html(data['questionPaper]);
			}
			else{
				alert("fail" + response);
			}
		}
	);
}
);

//Subjective question animation
$("#radioObjectiveQuestion_SetupQuestionPaper").click(function(){
	$(".options").removeClass("hide");
	$(".options").show();
});

$("#radioSubjectiveQuestion_SetupQuestionPaper").click(function(){
	$(".options").hide();
});

/*** JS for Evaluate Test ***/
$(".btnSaveMarksComment").click( function(){
	/* Get user data */
	var question_counter = parseInt(this.id);
	
	var mark = $("#markAllotedEvaluate" + question_counter).val();
	var comment = $("#commentForAnswerEvaluate" + question_counter).val();
	var max_mark = $("#maxMarkEvaluate" + question_counter).val();
	
	//Clear all the error message lables
	$("#errorMessageEvaluate" + question_counter).empty();
	
	/* Validate user data */
	// Check if mark is in the correct format : not null and is a number
	var numberReg =  /^[0-9]+$/;
	if(!numberReg.test(mark)){
		$("#errorMessageEvaluate" + question_counter).append("Marks not valid");
		return false;
	}
	mark = parseInt(mark);
	max_mark=parseInt(max_mark);
	//Check if mark is within range
	if(mark < 0 )
		{
		return false;
		}
	if(mark > max_mark)
		{
			//alert("wrong");
			$("#errorMessageEvaluate" + question_counter).append("Marks alloted is greater than max marks!!");
		return false;
		}
	
	/* Post data to server async */
	$.post("ajax/evaluateTest.php",
	{
		mark: mark,
		comment: comment,
		max_mark:max_mark
	},
	function(response){
		if(response == "Done"){
			//sucessful mark and comment saved
			alert("done, yay");
		}
		else{
			alert("fail");
		}
		
		//var data = JSON.parse(response);
		//alert(data['subj_marks']);
		
		//alert(testing['status']);
		
	}
	);

}
);
$(".btnViewScores").click(function(){
     $(".scores").removeClass("hide");
	 
	 $(".scores").show();
	 $(".lscores").removeClass("hide");
	 $(".btnViewScores").hide();
	 $(".btnFinish").removeClass("hidden");
	 $(".btnScoreRefresh").removeClass("hidden");
	
});
/*$(".btnViewScores").click(function(){
 $(".btnFinish").removeClass("hidden");
 $(".btnRefresh").removeClass("hidden");
});
*/

/*$(".btnViewScores").click(function(){
	
	
	$.post("ajax/evaluateTestTotal.php",
	function(response){
		if(response == "fail"){
			//sucessful mark and comment saved
			alert("fail");
			return "failed";
			//alert("done, yay");
		}
		else{
			//$("#subj_score").empty();
			//$("#subj_score").append(response);
			
		}
		
		
		
	}
	);
}
);  */

$(".btnScoreRefresh").click(function(){
$(".lscores").removeClass("hide");
$(".lscores").show();
   $.post("ajax/evaluateTestTotal.php",
   function(response){
	  
		if(response == "Fail"){
			
			alert("fail");
			return false;
		}
		else{
			$("#subj_score").empty();
			$("#subj_score").append(response);
			var oscore = $("#obj_score").val();
			oscore = parseInt(oscore);
			
			var tscore = parseInt(response)+parseInt(oscore);
			
			tscore = parseInt(tscore);
			
			$("#total_score").empty();
			$("#total_score").append(tscore);
		}
	 //$(".refreshscores").removeClass("hide");
	 //$(".refreshscores").show();
	
});

});
$(".btnFinish").click(function()
{
	$.post("ajax/finishButton.php",
	function(response){
		
		if( response = "Done"){
			
			 window.location.assign("PendingEvaluation.php");
		}
		else
		{
			return false;
		}
});

	});
	
	
	/** JS for setup test **/
$(".setupTestPaperType" ).click(function()
{
	var testPaper_type = this.id;
	$.post("setupTest.php",
	{
	 testpaper:testPaper_type
	},
	function(response){
		
		if(response ="Done")
		{
		window.location.assign("SetupQuestionPaper.php");
		}
	});
});

/*
$(".btnExistingTest").click(function()
{
	
	$(".newtestcard").removeClass("hide");
	$(".newtestcard").show();
	$(".btnTestPaper").show();
});
*/

$(".btnExistingTest").click(function(){
	alert("go btn");
	$(".newtestcard").removeClass("hide");
	$(".newtestcard").show();
	$(".btnTestPaper").show();
	
	var testpaperid = this.id;
	$.post("ajax/setupTest2.php",
	{
		testpaper_id :testpaperid
	},
	function(response){
		var data = JSON.parse(response);
		var parsedJson =parseInt(data['tpapertime']);
		var sf = sformat(parsedJson);  //value of sf --> 00:00:24:10
		
		$("#time1AllotedForTheTest").focus().val(sf[0]);
		$("#time2AllotedForTheTest").focus().val(sf[1]);
		$("#selectedTestTopic_SetupTest").text(data['tpapername']);
		$("#selectedTestTopic_SetupTest").val(data['tpapername']);
		alert(data['tpapername']);
		alert(val(data['testid']));
		$("#storetestid").val(data['testid']);
           
		function sformat( s ) {
			var fm = [  Math.floor(Math.floor(s/60)/60)%60,                          //HOURS
						Math.floor(s/60)%60                                                //MINUTES
					];
			return(fm);
			//return $.map(fm,function(v,i) { return ( (v < 10) ? '0' : '' ) + v; }).join( ':' );
		}
	});
});

$(".btnNewTest").click(function()
{
    $(".newtestcard").removeClass("hide");
	$(".btnTestPaper").hide();
	$("#time1AllotedForTheTest").val('');
	$("#time2AllotedForTheTest").val('');
	 
	
	
	//$("#selectedTestTopic_SetupTest option").val( $("#selectedTestTopic_SetupTest option").prop('defaultSelected') );
});
	
$("#btnSaveTestDetails_SetupTest").click(function()
{
	// get data
	var ttime1 = $("#time1AllotedForTheTest").val();
	var ttime2 = $("#time2AllotedForTheTest").val();
	var ttopic = $("#selectedTestTopic_SetupTest").val();
	 
	// clear error
	$("#errorMessage").empty();
	// validate - type and values
	
	// validate the data
	var numberReg =  /^[0-9]+$/;
	var isError = false;
	if(!numberReg.test(ttime1) || !numberReg.test(ttime2)){
		$("#errorMessage").append("time not valid!!<br/>");
		isError = true;
	}
	if(ttopic == 0)
	{
		$("#errorMessage").append("Please select the test topic");
		isError = true;
	}
	if(ttime1 > 12 || ttime1 < 0 || ttime2 > 60 || ttime2 < 0 )
	{
		$("#errorMessage").append("time not valid!!<br/>");
		isError = true;
	}
	if(isError)
		return false;	 
	//$("#selectedTestTopic_SetupTest").show();
	//var name = $("#selectedTestTopic_SetupTest).val();
	//alert(name);
	
	//use the data
	time1 = parseInt(ttime1);
	time2 =parseInt(ttime2);
	//alert(time1);
	//convert time to seconds
     var sec =time1*3600+time2*60;
    alert(sec);
	// post data
	$.post("ajax/SetupTestSave.php",
	{
		timesec: sec,
		ttopic: ttopic
	},
	function(response){
		
		if(response = "Done"){
			//sucessful mark and comment saved
			alert(" Test details entered successfully, yay!!");
		}
		else{
			alert("fail");
		}
}
);

}
);
$("#btnDeleteTestDetails_SetupTest").click(function()
{
	$.post("ajax/SetupTestSave.php",
	{
		timesec: sec,
		ttopic: ttopic
	},
	function(response){
		
		if(response = "Done"){
			//sucessful mark and comment saved
			alert(" Test details entered successfully, yay!!");
		}
		else{
			alert("fail");
		}
}
); 
	
	
	//$("#selectedTestTopic_SetupTest option").val( $("#selectedTestTopic_SetupTest option").prop('defaultSelected') );
});	
	
	/*** JS for Manage Account ***/


$("#submit_btn").click( function(){
	 $("#submit_btn1").hide();
/*	var numberReg =  /^[0-9]+$/;
	if(!numberReg.test(mark)){
		$("#errorMessageEvaluate" + question_counter).append("Marks not valid");
		return false;
	}
	*/
	
	var numberReg=/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/;
	
var username=1;
	var oldPassword=$("#old_password").val();
	var newPassword=$("#new_password").val();
	var confirmPassword=$("#confirm_new_password").val();
	
	
	
	
	
	if(newPassword!=confirmPassword)
	{	$("#old_password1").empty();
     $("#new_password1").empty();
$("#confirm_new_password1").empty();

		$("#confirm_new_password1").text("Password does not match");
		//alert("Password does not match");
	  return false;
	}
	
	
	
	
	if(newPassword =='' || oldPassword =='' )
	{
$("#old_password1").empty();
     $("#new_password1").empty();
$("#confirm_new_password1").empty();
		
$("#old_password1").text("Password not Entered");

$("#new_password1").text("newPassword not Entered");
  
      return false;
	}

	if(!numberReg.test(newPassword)){
	$("#new_password1").text("password should contain one alphabet, one number, one special character and 8 characters long");
	return false;
  }
	

	$.post("ajax/AccountSettings.php",
	{
		
		username :username,
	 oldPassword :oldPassword,
	 newPassword : newPassword,
	 confirmPassword :confirmPassword
	},function(response){
         if(response == "Done"){
$("#old_password1").empty();
   $("#new_password1").empty();
$("#confirm_new_password1").empty();		
		//sucessful mark and comment saved
			alert("Password changed successfully");
		}
		else{
			alert("Password does not exist");
		}
	}
	);
}
);


$("#changePassbtn").click(function(){
  
  $(".input_data").toggle();
	
	$(".changePass").removeClass("hide");
	 $("#changePassbtn").hide();
	
});

$("#changePassbtn").click(function(){
  $("#submit1_btn").hide();
	
 $(".cancel_btn_class").removeClass("hidden");
});


$("#changePassbtn").click(function(){
$(".changePass").show();
$("#cancel_btn").show();
});

$("#cancel_btn").click(function(){
  $("#old_password1").empty();
   $("#new_password1").empty();
$("#confirm_new_password1").empty();	
	
  $(".input_data").toggle();
 $(".changePass").toggle();
  $("#changePassbtn").show();
  
});


$("#cancel_btn").click(function(){
 $("#submit1_btn").show();
 $(".cancel_btn_class").toggle();
});























/*** JS for Pending Evaluation ***/
$(".traineeNamesInPendingEvaluation").click(function(){
	var traineeid = parseInt(this.id);
	
	$.post("ajax/pendingEvaluation.php",
	{
		trainee_id: traineeid
	},
	function(response){
		if(response == "Done"){
			
			 window.location.assign("EvaluateTest.php");
		}
		else{
			alert("fail");
		}
	}
	);
}
);


/*** JS for pending papers ***/
$(".PendingPaperForEvaluation").click(function(){
	alert("hii");
		var paperid = parseInt(this.id);
	
	$.post("ajax/pendingPapers.php",
	{
		paper_id: paperid
	},
	function(response){
		if(response){
			alert(response);
			 window.location.assign("PendingEvaluation.php");
		}
		else{
			alert("fail");
		}
	}
	);
}
);