/*** JS for Evaluate Test ***/
$(".btnSaveMarksComment").click( function(){
	/* Get user data */
	var question_id = parseInt(this.id);

	var mark = $("#markAllotedEvaluate" + question_id).val();
	var comment = $("#commentForAnswerEvaluate" + question_id).val();
	var max_mark = $("#maxMarkEvaluate" + question_id).val();
	
	//Clear all the error message lables
	$("#errorMessageEvaluate" + question_id).empty();
	
	/* Validate user data */
	// Check if mark is in the correct format : not null and is a number
	var numberReg =  /^[0-9]+$/;
	if(!numberReg.test(mark)){
		$("#errorMessageEvaluate" + question_id).append("Marks not valid");
		return false;
	}
	mark = parseInt(mark);
	max_mark = parseInt(max_mark);
	//Check if mark is within range
	if(mark < 0 ){
		return false;
	}
	if(mark > max_mark){
		
		
		$("#errorMessageEvaluate" + question_id).append("Marks alloted is greater than max marks!!");
		//var mark = $("#markAllotedEvaluate" + question_id).val();
		return false;
		mark = mark;						// TODO

		}

	

	
	/* Post data to server async */
	$.post("ajax/evaluateTest.php",
	{
		mark: mark,
		comment: comment,
		max_mark:max_mark,
		sq_id : question_id
	},
	function(responseData){
		var data = JSON.parse(responseData);
		var response = data['status'];
		alert(data['response']);
		if(response == "Done"){
			//sucessful mark and comment saved
			alert("done!");
		}
		else{
			alert("fail");
		}		
	}
	);

}
);

/*
$(".btnhello").click(function(){
	alert("hi");
     $(".scores").removeClass("hide");
	 $(".scores").show();
	 $(".lscores").removeClass("hide");
	 $(".btnViewScores").hide();
	 $(".btnFinish").removeClass("hidden");
	 $(".btnScoreRefresh").removeClass("hidden");
});
*/

$(".btnViewScores").click(function(){
	$(".scores").removeClass("hidden");
	$(".lscores").removeClass("hide");
	$(".btnFinish").removeClass("hidden");
	$(".btnScoreRefresh").removeClass("hidden");
	$(".btnViewScores").hide();
});

/*
$(".btnViewScores").click(function(){
	
	
	$.post("ajax/evaluateTestTotal.php",
	function(response){
		if(response == "fail"){
			//sucessful mark and comment saved
			alert("fail");
			return "failed";
			//alert("done, yay");
		}
		else{
			//$("#subj_score").empty();
			//$("#subj_score").append(response);
			
		}
		
		
		
	}
	);
}
);  
*/

$(".btnScoreRefresh").click(function(){
	$(".lscores").removeClass("hide");
	$(".lscores").show();
	
	$.post("ajax/evaluateTestTotal.php",
		function(responseData){
			var data = JSON.parse(responseData);
			var response = data['status'];
			
			if(response == "Done"){
				alert("done, yay");
				
				$("#subj_score").empty();
				$("#subj_score").append(data['totalSubMarkScored']);
				
				var oscore = $("#obj_score").val();
				oscore = parseInt(oscore);
				alert(data['totalSubMarkScored']);
				var tscore = parseInt(data['totalSubMarkScored']) + parseInt(oscore);
				tscore = parseInt(tscore);
				
				$("#total_score").empty();
				$("#total_score").append(tscore);
			}
			else{
				alert("fail");
			}
		}
	);
});

$(".btnFinish").click(function()
{
	$.post("ajax/finishButton.php",
		{
			d : 1
		},
		function(responseData){
			var data = JSON.parse(responseData);
			var response = data['status'];

			if( response == "Done"){
				 window.location.assign("PendingEvaluation.php");
			}
			else
			{
				return false;
			}
		}
	);
});