	/** JS for setup test **/
$("#MainQuestionPaper_SetupTest" ).click(function()
{
	var selectedpaperid1 = $("#maininputhidden").val();//this.val;
	var testID1 =$("#deleteinputhidden").val();
	$.post("ajax/setupTest.php",
	{
	 testpaperid : selectedpaperid1,
	 testid: testID1
	},
	function(responseData){
			//alert(responseData);
			var data = JSON.parse(responseData);
			var response = data['response'];
		if(response == "Done")
		{
		window.location.assign("SetupQuestionPaper.php");
		}
	});
});
$("#BackupQuestionPaper_SetupTest" ).click(function()
{
	var selectedpaperid2 = $("#backupinputhidden").val();//this.val;
	var testID2 =$("#deleteinputhidden").val();
	$.post("ajax/setupTest.php",
	{
	 testpaperid:selectedpaperid2,
	 testid: testID2
	},
	function(responseData){
			var data = JSON.parse(responseData);
			var response = data['response'];
			//alert(response);
		if(response == "Done")
		{
		window.location.assign("SetupQuestionPaper.php");
		}
	});
});
/*
$(".btnExistingTest").click(function()
{
	
	$(".newtestcard").removeClass("hide");
	$(".newtestcard").show();
	$(".btnTestPaper").show();
});
*/

$(".btnExistingTest").click(function(){
	$("#btnSaveTestDetails_SetupTest").hide();
	$("#NewTestPaper_SetupTest").hide();
	$(".newtestcard").removeClass("hide");
	$(".newtestcard").show();
	$(".btnTestPaper").show();
	//var subject_id = $("#saveinputhidden_top").val();

	$( "#Panel_Dummy_Message" ).empty();

	var testpaperid = this.id; //test id
	$.post("ajax/setupTest2.php",
	{
		testpaper_id :testpaperid
	},
	function(responseData){
		//alert(responseData);
		var data = JSON.parse(responseData);
		var parsedJson =parseInt(data['tpapertime']);
		var sf = sformat(parsedJson);  //value of sf --> 00:00:24:10
		
		$("#time1AllotedForTheTest").focus().val(sf[0]);
		$("#time2AllotedForTheTest").focus().val(sf[1]);
		$("#selectedTestTopic_SetupTest").focus().text(data['tpapername']);
		$("#selectedTestTopic_SetupTest").val(data['tpapername']);
		$("#testID").focus().text(data['tpapertype']);
		$("#testID").val(data['tpapertype']);
		$('#testID').attr("disabled", true);
		
		var mainpaperid = parseInt(data['main_paper']);
		//alert(data['main_paper']);
		var paper1id = parseInt( data['paper_1']);
		var paper2id = parseInt( data['paper_2']);
	    $("#maininputhidden").val(data['main_paper']);
		if(paper1id != mainpaperid)
		{
			$("#backupinputhidden").val(paper1id);
		}
		else if(paper2id != mainpaperid)
		{
			$("#backupinputhidden").val(paper2id);
		}
		//alert(data['tpapername']);
		//alert(val(data['testid']));

		//$("#saveinputhidden_btm").val(subject_id);
          

		//$("#storetestid").val(data['testid']);
		
		$("#deleteinputhidden").val(data['testpaperid']);
           
		function sformat( s ) {
			var fm = [  Math.floor(Math.floor(s/60)/60)%60,                          //HOURS
						Math.floor(s/60)%60                                                //MINUTES
					];
			return(fm);
			//return $.map(fm,function(v,i) { return ( (v < 10) ? '0' : '' ) + v; }).join( ':' );
		}
	});
});


$(".btnNewTest").click(function()
{
	$("#testIDLabel").hide();
	$("#testID").hide();
    $(".newtestcard").removeClass("hide");
	$(".btnTestPaper").hide();
	$("#time1AllotedForTheTest").val('');
	$("#time2AllotedForTheTest").val('');
	
	$("#Panel_Dummy_Message").empty();
	
	$(".clearbtn").hide();
	
	$(".btnSendForApproval_SetupTest").hide();
	 
	
	
	//$("#selectedTestTopic_SetupTest option").val( $("#selectedTestTopic_SetupTest option").prop('defaultSelected') );
});
	
$("#btnSaveTestDetails_SetupTest").click(function()
{
	
	// get data
	var ttime1 = $("#time1AllotedForTheTest").val();
	var ttime2 = $("#time2AllotedForTheTest").val();
	var ttopic = $("#selectedTestTopic_SetupTest").val().trim();
	//var s_id = $("#saveinputhidden_btm").val();

	// clear error
	$("#errorMessage").empty();
	// validate - type and values
	var isError = false;
	var testReg=/^$/;
	if(testReg.test(ttopic)){
		$("#errorMessage").append("Enter the Test Topic! <br/>");
		isError=true;
	}
	var numberReg =  /^[0-9]+$/;
	
	if(!numberReg.test(ttime1) || !numberReg.test(ttime2)){
		$("#errorMessage").append("Time not valid!!<br/>");
		isError = true;
	}
	/*if(ttopic == 0)
	{
		$("#errorMessage").append("Please select the test topic");
		isError = true;
	}*/
	if(ttime1 > 12 || ttime1 < 0 || ttime2 > 60 || ttime2 < 0 )
	{
		$("#errorMessage").append("time not valid!!<br/>");
		isError = true;
	}
	if(isError)
		return false;	 
	//$("#selectedTestTopic_SetupTest").show();
	//var name = $("#selectedTestTopic_SetupTest).val();
	//alert(name);
	
	time1 = parseInt(ttime1);
	time2 =parseInt(ttime2);
	//alert(time1);
	//convert time to seconds
     var sec =time1*3600+time2*60;
	//var s_id=$("#saveinputhidden").val();

	$.post("ajax/SetupTestSave.php",
	{
		timesec: sec,
		subject: ttopic
	},

	
	function(response){
		var data = JSON.parse(response);
		var responseData = data['status'];
		//alert(data['status']);
		//alert(data['responseData']);
		//alert(response);


        if(response == "Done")
		{
			if(data['response'])
			{
				//sucessful mark and comment saved
				alert(" Test details entered successfully!");
			}
			else
			{
				alert("fail");
			}

		}
}
);

}
);
$("#btnDeleteTestDetails_SetupTest").click(function()
{
	
	var selectedpaperid1 = $("#deleteinputhidden").val();
	//alert(selectedpaperid1);
	$.post("ajax/SetupTestDeleteTest.php",
	{
		testid : selectedpaperid1
	},
	function(responseData){
		//alert(responseData);
		var data = JSON.parse(responseData);
		var response = data['status'];
		//alert(data['response']);
		if(response = "Done"){
			//sucessful mark and comment saved
			alert(" Test deleted successfully, yay!!");
		}
		else{
			alert("fail");
		}
}
); 
	
	
	//$("#selectedTestTopic_SetupTest option").val( $("#selectedTestTopic_SetupTest option").prop('defaultSelected') );
});	