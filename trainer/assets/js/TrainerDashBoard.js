/*** JS for Trainer Dashboard ***/
/*
$(".TraineeNamePsid_Dashboard").click( function(){
	var student_Psid = this.id;
	
	var name = $("#StudentNameDisplay" + student_Psid).val();
	
	$.post("ajax/DashboardFunc.php",
	{
		name : name
	},
	function(response){
		if(response == "Done"){
			//sucessful mark and comment saved
			alert("done");
		}
		else{
			alert("fail");
		}
	}
	);
}
);
*/
	
$(".TraineeNamePsid_Dashboard").click(function(){
	marksData = {
					labels: [],
					datasets : [
						{
							fillColor: "#FF8C00",
							strokeColor: "#ACC26D",
							pointColor: "#fff",
							pointStrokeColor: "#9DB86D",
							data: [],
							showScale: true,
							scaleShowLabels: true,
							scaleBeginAtZero: true,
						}   ]
				};
				
	$( "#Graph_Message" ).empty();

	//marksId = document.getElementById("buyers").getContext("2d"); // get canvas
	//myChart = new Chart(marksId).Line(marksData,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: 10, scaleSteps: marks_array['steps'] }); // make a chart
		
	
	var TraineePSID_Dashboard = this.id; //trainee id

	var x;
 
	$.post("ajax/DashboardGraph.php",
		{
			TraineePSID_Dashboard: TraineePSID_Dashboard
		},
		function(response){
			
	
			var marks_array = JSON.parse(response);
			
			marksId = document.getElementById("buyers").getContext("2d"); // get canvas
			myChart = new Chart(marksId).Line(marksData,{scaleOverride: true, scaleStartValue: 0, scaleStepWidth: marks_array['steps'], scaleSteps: marks_array['value'] }); // make a chart
	
			for(i = 0; i < marks_array['marks'].length; i++)
			{
				x = parseInt(marks_array['marks'][i]);
				myChart.addData([x],"Test" + (i+1));
			}
			
		
		}
	
	)
		
}
);

/* var buyerData = { 
    labels : ["Test 1","Test 2","Test 3","Test 4"],
    datasets : [{
        fillColor : "#FFB74D",
        strokeColor : "#DECF3F ",         pointColor : "#B276B2",           pointStrokeColor : "#B276B2",
        data : [20,18,27,16],             scaleStepWidth:null,               showScale: true,                   scaleShowLabels: true,
        scaleBeginAtZero: false,
    }
               ]
} */
//var buyers = document.getElementById('buyers').getContext('2d');
//        var myChart=new Chart(buyers).Line(buyerData);
//        myChart.addData([17], "Test 5");