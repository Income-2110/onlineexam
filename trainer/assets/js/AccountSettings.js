$("#submit_btn").click( function(){
	//$("#submit_btn").hide();
	
	//$(".input_data").toggle();
	//$(".changePass").toggle();
	//$("#changePassbtn").show();
	
	var numberReg=/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/;
	
    var username=1;
	var oldPassword=$("#old_password").val();
	var newPassword=$("#new_password").val();
	var confirmPassword=$("#confirm_new_password").val();
	$("#error_label").empty();
	$("#error_label2").empty();
	if(newPassword!=confirmPassword)
	{	
		$("#error_label2").append("Password does not match");
		//alert("Password does not match");
		return false;
	}
	if(newPassword=='' || oldPassword=='' )
	{
		$("#error_label2").append("Password not entered");
  		return false;
	}
	if(!numberReg.test(newPassword))
	{
		$("#error_label2").append("Password should contain one alphabet, one number, one special character and 8 characters long");
		return false;
	}
	//$(".input_data").toggle();
	
	$.post("ajax/AccountSettings.php",
	{
		username :username,
		oldPassword :oldPassword,
		newPassword : newPassword,
		confirmPassword :confirmPassword
	},function(responseData){
		var data = JSON.parse(responseData);
		var response = data['status'];
		//alert(data['response']);
        if(response == "Done")
		{
			if(data['response'])
			{
				//sucessful mark and comment saved
				$("#old_password1").hide();
				$("#old_password").hide();
				$("#new_password").hide();
				$("#new_password1").hide();
				$("#confirm_new_password").hide();
				$("#confirm_new_password1").hide();
				$("#submit_btn").hide();
				$("#cancel_btn").hide();
				$("#error_label").append("Password changed successfully!");
				
			//alert("Password changed successfully");
			}
			else
			{
				$("#error_label2").append("Password does not exist");
				//alert("Password does not exist");
			}
		}
		
		//$("#changePassbtn").show();
		$("#old_password").val('');
		$("#new_password").val('');
		$("#confirm_new_password").val('');
		}
	);
}
);
$("#changePassbtn").click(function(){
	//$(".input_data").toggle();
	$(".changePass").removeClass("hide");
	$("#changePassbtn").hide();
	$(".cancel_btn_class").removeClass("hidden");
	$(".changePass").show();
	$("#submit_btn").show();
	$("#cancel_btn").show();
	}
);

$("#cancel_btn").click(function(){
	//$(".cancel_btn_class").removeClass("hidden");
	$(".changePass").hide();
	$("#changePassbtn").show();
	//$("#submit1_btn").show();
	//(".cancel_btn_class").hide();
	$(".input_data").show();
	
	$("#old_password").val('');
	$("#new_password").val('');
	$("#confirm_new_password").val('');
	$("#submit_btn").hide();
	$("#cancel_btn").hide();
	$("#error_label").hide();
	$("#error_label2").hide();
});
