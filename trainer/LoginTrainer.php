<!DOCTYPE html>
<html>
<head>
	<div class="nav-wrapper blue lighten-2" style="font-size:14pt" >
		Trainer Login
	</div>
    <link type="text/css" rel="stylesheet" href="assets/materialize/css/materialize.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/bootstrap-3.3.6/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="assets/font-awesome-4.5.0/css/font-awesome.min.css" />
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col l3 z-depth-6 card-panel">   

            <div class="input-field col l12">
				<input name="psid_txt"  id="TraineePsid" type="text" class="validate">
				<label for="first_name">PSID</label>
            </div>

			<div class="row">
				<div class="input-field col l12">
					<input name="passwd_txt" id="password" type="password" class="validate">
					<label for="password">Password</label>
				</div>
			</div>
       
			<button id="login_btn" class="waves-effect waves-light btn">Login</button>
            <br/> <br/>
            
			<a href="#">Forgot Password?</a>
        </div>
    </div>
</div>
<script src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/bootstrap-3.3.6/js/bootstrap.min.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>

<script>
$("#login_btn").click(function() {
	var username = $("#TraineePsid").val();
	var password = $("#password").val();
	
	alert(username + " " + password);
	$.post("ajax/LoginTrainer.php",
	{
		userid : username,
		passwd : password 
	}, function(responseData){
		var data = JSON.parse(responseData);
		var response = data['status'];
		alert(data['response']);
		if(response == "Done")
		{
			window.location.replace("TrainerDashBoard.php");
		}
		else
		{
			// wrong password
			alert("Invalid username or password");
			
		}
		
	});
});
</script>

<footer class="page-footer"  style="margin-bottom:0%;">
	<div class="footer-copyright">
        <div class="container">
            &copy; 2014 ITC INFOTECH
        </div>
    </div>
</footer>

</body>
</html>