<?php 

require_once("helper/dalFunctions.php");

?>


    <main >
    <div class="container" style="width:100%" >
     
          <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
             <div class="panel panel-default testpanel" style="width:100%">
                            <div class="panel-heading score-header">Trainee names</div>
                            <!-- List group -->
                            <ul class="list-group">
							<?php
								$studentNameAndPsid = dal_getAllTraineeNamesAndId($user_name); 		#TODO use session variable
								foreach($studentNameAndPsid as $Name_PSID_row)
								{
							?>
                                <li class="list-group-item score_list" id="<?php echo $Name_PSID_row[1] ?>">
									<h5 id="StudentNameDisplay" class="test_name_header">
									<?php
										echo htmlspecialchars($Name_PSID_row[0]);
									?>
									</h5>
                                    <div class="score-display">11</div>
                                </li>
                                <?php
								}
							?>
                            </ul>
                        </div> 
              </div>
              <div class="col s12 m12 l6">
                  <div class="card-panel cyan darken-1 graphpanel">
                                    <div class="container-fluid">  <canvas id="buyers" class="testresult_graph"></canvas></div>
                                          
                                      
                                </div>
              </div>
           
              </div>
        
                    <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
            
                        <div class="panel panel-default classpanel">
                            <div class="panel-heading Current-rank-header"> 
                                <h2 class="panel-title Current-rank-title">Class Ranking</h2>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-3.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-6.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                            </ul>
                        </div>
            
              </div>
            
        
        
              </div>
              
        </div>
      
              
        </div>
 
        </main>
  <script>
        var buyers = document.getElementById('buyers').getContext('2d');
        var myChart=new Chart(buyers).Line(buyerData,{responsive: true,maintainAspectRatio: true,});
        </script>
        <script>
    $(".button-collapse").sideNav();
  // Show sideNav
  $('.button-collapse').sideNav('show');
  // Hide sideNav
  $('.button-collapse').sideNav('hide');
        
    </script>
    
    
    
</body>
</html>

