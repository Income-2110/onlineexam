<main >
    <div class="container" style="margin-top:10%" >
     
      <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
             <div class="panel panel-default testpanel" style="width:100%;margin-left:2%;margin-top:auto">
                            <div class="panel-heading score-header">Trainee Names</div>
						 <!-- List group -->
						<ul class="list-group" >
							<?php
								$studentNameAndPsid = dal_getAllTraineeNamesAndId($user_name); 		#TODO use session variable
								
								if(is_bool($studentNameAndPsid)){
								if($studentNameAndPsid == true)
								{
							?>
								<h4>Currently there are no students under this trainer</h4>
							<?php				
								}
								elseif($studentNameAndPsid == false)
								{
							?>
								<h4>Oops! There occurred an issue with the Database</h4>
							<?php
								}}
								elseif(is_array($studentNameAndPsid))
								{
									foreach($studentNameAndPsid as $Name_PSID_row)
									{
							?>
							<li id="<?php echo $Name_PSID_row["trainee_id"]; ?>" class="list-group-item score_list TraineeNamePsid_Dashboard">
								<a onmouseover="" style="cursor: pointer;">
									<h5 id="StudentNameDisplay" class="test_name_header">                                        
										<?php
											echo htmlspecialchars($Name_PSID_row["name"]);
										?>
									</h5>
								</a>
							</li>
							<?php
									}
								}
								
							?>
						</ul>
				</div> 
        </div>
        <div class="col s12 m12 l6" style="margin-top:auto">
			<div class="panel-heading graph_title">
				Graph 
			</div>
			<div id="graph_container" class="card-panel graphpanel graph_style" >
				<!--<p id="Graph_Message_TraineeName" style="margin-left:20%;margin-top:0%; color: blue; size: 60px"></P> -->
				<p id="Graph_Message" style="margin-left:20%;margin-top:25%; color: blue;"> Click on any student name to view the graph </p>
				<canvas id="buyers" class="testresult_graph"></canvas>  
			</div>
		</div>	
	  </div>	

	  <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
            
                    <div class="panel panel-default classpanel">
                            <div class="panel-heading Current-rank-header"> 
                                <h2 class="panel-title Current-rank-title">Class Ranking</h2>
                            </div>	
					    <ul class="list-group" style="display:inline">
							<li class="list-group-item rank_list">
							<div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        
                                    </a>
								</div>
                                
								<div class="media-body">
									<?php
										$trainee_Rank=1; 					#TODO use session variable
										$trainee_Name= dal_getTraineeRankingFromCurrentBatch($user_name );
										if(is_bool($trainee_Name)){
										if($trainee_Name == true)
										{
									?>
											<h4>There were no tests conducted. Ranking unavailable</h4>
									<?php
										}
										elseif($trainee_Name == false)
										{
									?>
											<h4>Oops! There occurred an issue with the Database</h4>
									<?php
										}}
										else
										{
											foreach($trainee_Name as $trainee_Name_forRank)
											{
									?>
											<div class="row">
												<div class="col l4 m4 s4">
													<img class="media-object profile-pic" src="images/<?php echo $trainee_Name_forRank["profile_pic"]; ?>" />
												</div>
												<div class="col l4 m4 s4" style="height:20px">
													<h4 class="media-heading text-center txt_style" ></h4>
													<?php echo htmlspecialchars($trainee_Name_forRank["name"]);?>
												</div>
												<div class="col l4 m4 s4" style="height:20px">
													<h4 class="rank-display rankDisplay_style" >
														<?php
															echo htmlspecialchars('#');
															echo htmlspecialchars($trainee_Rank++);
														?>
													</h4>
												</div>
											</div>
									<?php
											}
										}
									?>
								</div>
                                <br/>
                            </div>
							</li>
						</ul>
					</div>
		</div>
	  </div>
	</div>
 </main>	

  



	
