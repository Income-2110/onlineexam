<!DOCTYPE html>
<?php 
require_once("helper/dalFunctionsQ.php");
?>
<html>
<head>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <!--<link type="text/css" rel="stylesheet" href="assets/materialize/css/materialize.min.css" /> -->
    <!--<link type="text/css" rel="stylesheet" href="assets/bootstrap-3.3.6/css/bootstrap.min.css" />-->
	<!--<link type="text/css" rel="stylesheet" href="assets/font-awesome-4.5.0/css/font-awesome.min.css" />-->
    <!--<link rel="stylesheet" type="text/css" href="assets/css/TrainerDashBoard.css"/>-->
	<!--<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css"/> 
    <!--
	<style>
        html,body
            {               
                width:100%;
                height: 100%;
            }
    </style>
	-->
	
	<link href="assets/importTrainer.css" rel="stylesheet"/> 
    
	<style>
	    header, main,container, footer {
      padding-left: 240px;
    }
	@media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
      }
    }
	.drop{
		background: white;
		color: #48A7F2;
		box-shadow: none;
		padding: 0;
		padding-right: 30%;
	}
	.drop-btn{
		margin-top: 20%;
	}
	.drop-btn:hover{
		background: white;
		box-shadow: none;
	}
	.dropdown-content li>a, .dropdown-content li>span{
		color: #48A7F2;
	}
	.pagesection{
		border-bottom: 1px solid #ebebeb;
	}
	.row1{
		position: fixed;
		margin-left: -2%;
		z-index: 10000;
		background-color: white;
	}
	.testresult_graph
        {
            width:350px;
            height:350px;
        }
	.right
	{
		font-size:16pt;
	}	
	.side-nav li:hover{
		background-color:#141414;
	}
	</style>
</head>

<body>
	<!--<div class="row">
        <div class="page-section half bg-white pagesection">  
            <div class="collapse navbar-collapse nav-main" id="main-nav">
                <ul class="nav navbar-nav">
                    <li><img src="images/itclogo.jpg" width="320px" height="120px"></li>
                </ul>
            </div>               
        </div>
    </div>-->
	
	<header>
      <nav>
		<div class="nav-wrapper blue lighten-2" >
			<a href="#" class="brand-logo left" style="height:inherit;"><img src="images/itcLogoTransparent.png" style="height:inherit;margin-left:14%; "></a> 
			<?php //print_r($_SESSION); ?>);
			<ul id="nav-mobile" class="right" >
				<!--<li><a style="font-size:14pt" href="#">Profile</a></li> -->
				<li><a style="font-size:14pt" href="TrainerDashBoard.php">Home</a></li>
				<li><a style="font-size:14pt" href="AccountSettings.php">Settings</a></li>
				<li><a  style="font-size:14pt" href="..\login.php">Logout</a></li>
			</ul>
		</div>
      </nav>
    </header>
	  <?php
				$user_name = $_SESSION['user_id'];									#TODO use session variable
				$trainerpictureandname = dal_getTrainerPictureAndName($user_name);
			?>
		<ul id="slide-out" class="side-nav fixed grey darken-4 " >
			<li style="margin-top:20px;">
				<div class="profile-userpic">
					<img src="images/<?php echo $trainerpictureandname["profile_pic"]; ?>" class="img-responsive" alt="">
				</div>
			</li>
			<li>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name" >
						<?php 
								echo htmlspecialchars($trainerpictureandname["name"]);
						?>
					</div>
                <div class="profile-usertitle-job" >
                    Trainer
                </div>
				</div>
			</li>
			<li>
				<a id="<?php echo $Trainer_Picture_And_Name[0] ?>" href="AccountSettings.php" style="font-size:10pt;color:grey;" >
                 <i class="glyphicon glyphicon-user Acc_Settings_Dashboard"></i>
                  Account Settings
				</a>
			</li>
			<li>
                <a href="SetupTest.php" style="font-size:10pt;color:grey;">
                 <i class="glyphicon glyphicon-flag"></i>
                   Setup Test
                </a>
            </li>
             <li>
                <a href="PendingPapers.php" style="font-size:10pt;color:grey;">
                 <i class="glyphicon glyphicon-flag"></i>
                   Evaluate Test
                </a>
            </li>
            <li id="Logout_Id">
                <a id="test" href="ajax\LogoutFunc.php" style="font-size:10pt;color:grey;" >
                    <i class="glyphicon glyphicon-flag"></i>
						Logout
                </a>
            </li>
		</ul>
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
		
	