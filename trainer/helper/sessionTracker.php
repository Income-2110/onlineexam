<?php
	if(!isset($_SESSION)) {
		session_start();
		#TODO Correct the links
		if (isset($_SESSION['user_id'])) {
			# redirect to the respective page
			switch ($_SESSION['page_name']) {
				case 'TrainerDashBoard':
					header("Location: TrainerDashBoard.php");
					break;
				case 'SetupTest':
					header("Location: SetupTest.php");
					break;
				case 'EvaluateTest':
					header("Location: EvaluateTest.php");
					break;
				case 'AccountSettings':
					header("Location: AccountSettings.php");
					break;
				default:
					header("Location: login.php");
					break;
			}
		}
		else{
			# redirect to login page
			header("Location: login.php");
		}
	}
?>