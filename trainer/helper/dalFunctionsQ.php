<?php
/* This file contains the DAL layer functions*/

  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

	/*
	Connects to database and returns the connection handle.
	Stops execution if database connection is not successful
	*/
	function dbConnect()
	{
		$con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		return $con;
	}

	
	/* functions start from here */
	/* 
	This function gets the names and trainee Ids of all the trainees belonging to the current batch of the trainer that has logged in 
	
	Returns:	array:	An array of all the trainees names and IDs under the logged in trainer's current batch
				true:	If query is successful, but there are no trainees under the logged in trainer's current batch
				false:	If the query is unsuccessful
	
	*/
	function dal_getAllTraineeNamesAndId($trainerId)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainerId = mysqli_real_escape_string($con,$trainerId);
		//Query
		$query = "Select trainee.name, trainee.trainee_id from trainee, batch_trainee, batch_info
			where trainee.trainee_id = batch_trainee.trainee_id and
			batch_trainee.batch_id = batch_info.batch_id and
			batch_info.trainer_id = $trainerId and batch_info.active= 1 order by trainee.trainee_id"; 
			//Here batch_info.active = 1 implies current batch of the logged in trainer
		$query_result = mysqli_query($con,$query);
	
		if($query_result)
		{

			if(mysqli_num_rows($query_result)>0)
			{
				$traineeName = array();
				while($resultSet = mysqli_fetch_array($query_result))
				{
					$traineeName_Detail = array();
					$traineeName_Detail["name"] = $resultSet["name"];
					$traineeName_Detail["trainee_id"] = $resultSet["trainee_id"];
					$traineeName[] = $traineeName_Detail;
				}
				mysqli_close($con);
				return $traineeName;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}



	/* 
	This function gets the total marks of each subject test taken by the trainee belonging to the current batch of the logged in trainer
	
	Returns:	array:	An array of all the marks scored by the trainee belonging to the current batch of the logged in trainer
				true:	If query is successful, but there are no marks scored by the trainee  in the database
				false:	If the query is unsuccessful
	
	*/
	function dal_getTotalMarksOfEachTest($trainee_id) //Data for graph
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con, $trainee_id);
		//Query
		$query = "Select total_marks from result, batch_trainee, batch_info
				 where result.trainee_id = $trainee_id and result.total_marks > -1 AND
				 result.trainee_id = batch_trainee.trainee_id AND
				 batch_trainee.batch_id = batch_info.batch_id AND
				 batch_info.active = 1";
		$query_result = mysqli_query($con, $query);
	 
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$marks = array();
				while($resultSet = mysqli_fetch_array($query_result))
				{
					$marks[] = $resultSet["total_marks"];
				}
				mysqli_close($con);
				return $marks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}


	/* 
	This function gets the subjective questions ID of a particular paper
	
	Returns:
				true:	If query is successful, but there are no subjective questions
				array:	An array of all the subjective questions ID of a particular paper
				false:	If the query is unsuccessful
	*/
	function dal_getSubjectiveQuestionsId($paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_id = mysqli_real_escape_string($con,$paper_id);
		//Query
		$query="Select sq_id from test_question_subj where paper_id=$paper_id";
		$query_result=mysqli_query($con,$query);
	
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$sq_Id = array();
				while($resultSet = mysqli_fetch_array($query_result))
				{
					$sq_Id_Detail = array();
					$sq_Id_Detail["sq_id"]=$resultSet["sq_id"];
					$sq_Id[] = $sq_Id_Detail;
				}
				mysqli_close($con);
				return $sq_Id;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
		
	

	/*
	This function gets the subjective answers ID of a particular paper and of a particular trainee
	
	Returns:
				true:	If query is successful, but there are no subjective answers
				int:	Returns the ID of the subjective answer paper of a particular trainee 
						and of a particular question paper 
				false:	If the query is unsuccessful
	*/
	function dal_getSubjectiveAnswersId($trainee_id,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con, $trainee_id);
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		//Query
		$query = "Select sanswer_id from answers_subj where trainee_id = $trainee_id and paper_id = $paper_id";
		$query_result=mysqli_query($con,$query);
		
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$sanswer_Id= $resultSet['sanswer_id'];
				mysqli_close($con);
				return $sanswer_Id;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}

	/*
	This function gets the Subjective Question content, marks, difficulty level name 
	and ID  of a particular subjective question
	
	Returns:
				true:	If query is successful, but there is no subjective question content
				array:	Returns the subjective question content, marks, difficulty level name 
						and ID of a particular subjective question
				false:	If the query is unsuccessful
	*/
	function dal_getSubjectiveQuestionContent($sq_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$sq_id = mysqli_real_escape_string($con, $sq_id);
		//Query
		$query = "Select questions_subj.question, questions_subj.difficulty_level, questions_subj.marks,  difficulty_levels.level_name
				from questions_subj, difficulty_levels where questions_subj.sq_id=$sq_id
				AND  difficulty_levels.dlevel_id = questions_subj.difficulty_level";
		$query_result=mysqli_query($con,$query);
		
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$question_SubjDetail = array();
				$question_SubjDetail[] = $resultSet['question'];
				$question_SubjDetail[] = $resultSet['marks'];
				$question_SubjDetail[] = $resultSet['difficulty_level'];
				$question_SubjDetail[] = $resultSet['level_name'];
					
				mysqli_close($con);
				return $question_SubjDetail;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/*
	This function gets the Subjective Answer Content of a particular 
	question from a particular paper written by the given trainee
	
	Returns:
				true:	If query is successful, but there is no subjective answer content
				string:	Returns the subjective answer content
				false:	If the query is unsuccessful
	*/
	function dal_getSubjectiveAnswerContent($sq_id,$traineeId,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$sq_id = mysqli_real_escape_string($con, $sq_id);
		$traineeId = mysqli_real_escape_string($con, $traineeId);
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		//Query
		$query = "Select answer from answer_detail_subj where sq_id=$sq_id and sanswer_id = 
				(Select sanswer_id from answers_subj where trainee_id=$traineeId and paper_id=$paper_id)";
		$query_result=mysqli_query($con,$query);
		
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				
				$resultSet=mysqli_fetch_array($query_result);
				$answer_Subj = $resultSet['answer'];
				mysqli_close($con);
				return $answer_Subj;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/*
	This function gets the Maximum marks allotted for a particular subjective question
	
	Returns:
				true:	If query is successful, but there is no marks allotted
				int:	Returns the allotted marks of a particular subjective question
				false:	If the query is unsuccessful
	*/
	function dal_getMaxMarksOfSubjectiveQuestion($sq_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$sq_id = mysqli_real_escape_string($con, $sq_id);
		//Query
		$query = "Select marks from questions_subj where sq_id=$sq_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$marks_alloted = $resultSet['marks'];
				mysqli_close($con);
				return $marks_alloted;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
				
	}
	
	
	/* 
	This function gets the marks assigned and comments written by the trainer for a particular 
	subjective answer written by the given trainee of a particular paper
	
	Returns:
				true:	If query is successful, but there is no marks assigned or comments written
				array:	Returns the assigned marks and comments written for a particular subjective question of given trainee
				false:	If the query is unsuccessful
	*/
	function dal_getSubjAssignedMarksAndComment($sq_id,$traineeId,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$sq_id = mysqli_real_escape_string($con, $sq_id);
		$traineeId = mysqli_real_escape_string($con, $traineeId);
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		//Query
		$query = "Select marks, comments from answer_detail_subj where sq_id=$sq_id and sanswer_id = (Select sanswer_id from answers_subj
				 where trainee_id=$traineeId and paper_id = $paper_id)";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$assignedMarks_Comment = array();
				$resultSet=mysqli_fetch_array($query_result);
				$assignedMarks_Comment["marks"] = $resultSet["marks"];
				$assignedMarks_Comment["comment"] = $resultSet["comments"];
				mysqli_close($con);
				return $assignedMarks_Comment;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/* 
	This function gets the names of trainees in the current batch of the trainer that has logged in, in the order of their ranking
	
	Returns:
				true:	If query is successful, but there are no marks in the database
				array:	Returns the names of trainee as per their rank
				false:	If the query is unsuccessful
				
				
				----If the marks scored by 2 trainees are same, then sorting is done between them 
									acccording to their trainee ID's ---
	*/
	function dal_getTraineeRankingFromCurrentBatch($trainerId)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainerId = mysqli_real_escape_string($con,$trainerId);
		//Query
		$query = "select trainee.name, trainee.profile_pic, avg(result.total_marks) as average
				 from trainee, result, batch_info, batch_trainee
				 where trainee.trainee_id = result.trainee_id AND
				 trainee.trainee_id = batch_trainee.trainee_id AND
				 batch_trainee.batch_id = batch_info.batch_id AND
				 batch_info.trainer_id =  $trainerId AND batch_info.active = 1 AND result.total_marks>-1
				 AND result.exam_completed =1 group by result.trainee_id order by average desc";
		
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$traineeName = array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$traineeNameR = array();
					$traineeNameR["name"] = $resultSet["name"];
					$traineeNameR["profile_pic"] = $resultSet["profile_pic"];
					$traineeNameR["average"] = $resultSet["average"];
					$traineeName[] = $traineeNameR;
				}
				mysqli_close($con);
				return $traineeName;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/* 
	This function gets the subject names and ID's of the current batch of the logged in trainer containing papers which are not evaluated yet
	
	Returns:
				true:	If query is successful, but there are no batches in the database
				array:	Returns the subject names and IDs of the current batch of the logged in trainee containing papers which are not evaluated yet
				false:	If the query is unsuccessful
	*/
	function dal_getTheSubjectContainingPapersToBeEvaluated($trainer_id) 		
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainer_id = mysqli_real_escape_string($con,$trainer_id);
		//Query
		$query= "SELECT subject_detail.subject_name, subject_detail.subject_id 
				FROM subject_detail,batch_info,result,master_test
				WHERE result.total_marks = -1 AND result.exam_completed = 1 AND
				result.test_id = master_test.test_id AND
				master_test.subject_id = subject_detail.subject_id AND
				master_test.batch_id = batch_info.batch_id AND
				batch_info.active = 1 AND
				batch_info.trainer_id = $trainer_id
				group by subject_detail.subject_name";
				
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$SubjectName=array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$SubjectName_Detail = array();
					$SubjectName_Detail["subject_name"] = $resultSet["subject_name"];
					$SubjectName_Detail["subject_id"] = $resultSet["subject_id"];
					$SubjectName[] = $SubjectName_Detail;
				}
				mysqli_close($con);
				return $SubjectName;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}		
			
	}
	
	
	
	/* 
	This function gets the trainee Id's and total marks of all trainees who have written test of a particular subject
	
					---- Consider -1 as not evaluated and any other value as evaluated ---
	
	Returns:
				true:	If query is successful, but there are no marks in the database
				array:	Returns the trainee Id's and total marks of all trainees who have written test of a particular subject
				false:	If the query is unsuccessful
	*/
	function dal_getTraineeIDsWithPendingEvaluation($subject_id) 				
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$subject_id = mysqli_real_escape_string($con,$subject_id);
		//Query
		$query="SELECT result.trainee_id, result.total_marks from result, master_test, subject_detail 
				where result.test_id = master_test.test_id AND
				master_test.subject_id = subject_detail.subject_id AND
				subject_detail.subject_id = $subject_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$pendingEvaluation=array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$pendingEvaluation_Detail = array();
					$pendingEvaluation_Detail["trainee_id"] = $resultSet["trainee_id"];
					$pendingEvaluation_Detail["total_marks"] = $resultSet["total_marks"];
					$pendingEvaluation[] = $pendingEvaluation_Detail;
				}
				mysqli_close($con);
				return $pendingEvaluation;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}		
		
	}
	
	
	/* 
	This function gets the total subjective marks scored by a trainee for a particular paper
	
	Returns:
				true:	If query is successful, but there are marks in the database
				int:	Returns the total subjective marks scored by a trainee for a particular paper
				false:	If the query is unsuccessful
	*/
	function dal_getTotalSubjectiveMarksScored($trainee_Id,$paper_Id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_Id = mysqli_real_escape_string($con,$trainee_Id);
		$paper_Id = mysqli_real_escape_string($con,$paper_Id);
		//Query
		$query="Select marks_subj from answers_subj 
				where answers_subj.trainee_id= $trainee_Id AND answers_subj.paper_id=$paper_Id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$totalScore= $resultSet["marks_subj"];
				mysqli_close($con);
				return $totalScore;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}		
		
	}
	
	/*
	This function gets the subjective marks scored by a trainee in a particular paper
	
	Returns:
			int:	The subjective marks scored by the trainee in a particular paper
			true:	If query is successful, but there are no marks in the database
			false:	If query is unsuccessful
	*/
	function dal_getScoredMarksSubj($trainee_id,$paper_id) 
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con,$trainee_id);
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		//Query
		$query="Select sum(marks) as sum from answer_detail_subj where sanswer_id = (Select sanswer_id from answers_subj
				where trainee_id = $trainee_id and paper_id = $paper_id)";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$scoredMarks = $resultSet["sum"];
				mysqli_close($con);
				return $scoredMarks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
		
	/*
	This function sets(updates) the total subjective marks scored by a trainee in a particular paper into the database
	
	Returns:
			true:	If update query is successful
			false:	If update/select query is unsuccessful
	*/	
	function dal_setScoredMarksSubj($trainee_id,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con,$trainee_id);
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		//Query
		$query="Select sum(marks) as sum from answer_detail_subj where sanswer_id = 
				(Select sanswer_id from answers_subj
				where trainee_id = $trainee_id and paper_id = $paper_id)";
		$query_result=mysqli_query($con,$query);
		if(!$query_result)
		{
			if(!mysqli_num_rows($query_result)>0)
			{
				mysqli_close($con);
				return false;
			}
			mysqli_close($con);
			return false;
		}
		else
		{
			$resultSet = mysqli_fetch_array($query_result);
			$scoredMarks = $resultSet["sum"];
			
			$update_query = "Update answers_subj set marks_subj = $scoredMarks where trainee_id = $trainee_id and paper_id = $paper_id";
			$update_query_result = mysqli_query($con,$update_query);
			if($update_query_result)
			{
				mysqli_close($con);
				return true;
			}
			else
			{
				mysqli_close($con);
				return false;
			}
		}
	}
	
	/*
	This function gets the total marks(subjective + objective) scored by a trainee in a particular paper
	
	Returns:
			int:	The total marks scored by the trainee in a particular paper
			true:	If query is successful, but there are no marks in the database
			false:	If query is unsuccessful
	*/
	function dal_getTotalMarksScored($trainee_id,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con,$trainee_id);
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		//Query
		$query="Select (marks_subj + marks_obj) as total_marks from answers_subj, answers_obj
				where answers_subj.paper_id = answers_obj.paper_id AND
				answers_subj.trainee_id = answers_obj.trainee_id AND
				answers_subj.trainee_id = $trainee_id AND
				answers_subj.paper_id = $paper_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$totalMarks = $resultSet['total_marks'];
				mysqli_close($con);
				return $totalMarks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/*
	This function sets(updates) the total marks(subjective + objective) scored by a trainee in a particular paper into the database
	
	Returns:
			true:	If update query is successful
			false:	If update/select query is unsuccessful
	*/
	function dal_setScoredTotalMarks($trainee_id,$paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id = mysqli_real_escape_string($con,$trainee_id);
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		//Query
		$query="Select (marks_subj + marks_obj) as total_marks from answers_subj,answers_obj
				where answers_subj.paper_id = answers_obj.paper_id AND
				answers_subj.trainee_id = answers_obj.trainee_id AND
				answers_subj.trainee_id = $trainee_id AND
				answers_subj.paper_id = $paper_id";
		$query_result=mysqli_query($con,$query);
		if(!$query_result)
		{
			if(!mysqli_num_rows($query_result)>0)
			{
				mysqli_close($con);
				return false;
			}
			mysqli_close($con);
			return false;
		}
		else
		{
			$resultSet = mysqli_fetch_array($query_result);
			$totalMarks = $resultSet['total_marks'];
			
			$update_query = "Update result set total_marks = $totalMarks where trainee_id = $trainee_id and paper_id = $paper_id";
			$update_query_result = mysqli_query($con,$update_query);
			if($update_query_result)
			{
				mysqli_close($con);
				return true;
			}
			else
			{
				mysqli_close($con);
				return false;
			}
		}
		
	}
	
	
	/*
	This function gets the total subjective marks of a particular question paper
	
	Returns:
			int:	The total subjective marks of a particular question paper
			true:	If query is successful, but there are no marks allotted in the database
			false:	If query is unsuccessful
	*/
	function dal_getTotalSubjectiveMarksOfAPaper($paper_Id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_Id= mysqli_real_escape_string($con,$paper_Id);
		//Query
		$query="Select sum(marks) as total from questions_subj where sq_id in (Select sq_id from test_question_subj
				where paper_id = $paper_Id)";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$maxSubjPaperMarks= $resultSet['total'];
				mysqli_close($con);
				return $maxSubjPaperMarks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}		
	}
	
	
	/*
	This function gets the total objective marks of a particular question paper
	
	Returns:
			int:	The total objective marks of a particular question paper
			true:	If query is successful, but there are no marks allotted in the database
			false:	If query is unsuccessful
	*/
	function dal_getTotalObjectiveMarksOfAPaper($paper_Id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_Id= mysqli_real_escape_string($con,$paper_Id);
		//Query
		$query="Select sum(marks) as total from questions_obj where question_id in (Select question_id from test_question_obj
				where paper_id = $paper_Id)";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$maxObjPaperMarks= $resultSet['total'];
				mysqli_close($con);
				return $maxObjPaperMarks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}		
	}
	
	
	
	
	
	/*
	This function gets the total marks set for all the papers in a batch
	
	Returns:
			int:	The total marks set for all the papers in a batch
			true:	If query is successful, but there are no marks allotted in the database
			false:	If query is unsuccessful
	*/
	
	
	function dal_getTotalMarksOfQuestionPaper($batch_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$batch_id= mysqli_real_escape_string($con,$batch_id);
		//Query
		$query = "select batch_info.max_marks from batch_info where batch_id = $batch_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$totalQuestionPaperMarks= $resultSet['max_marks'];
				mysqli_close($con);
				return $totalQuestionPaperMarks;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	/*
	This function executes when you click on the save button for a new test.
	Inserts values into to the paper and the master_test table
	
	Returns:
			true:	If Insert query is successful
					A new test_id is generated. 
					Also, 2 paper_id's are generated among which one is the main paper
			false:	If the query is unsuccessful or no data in the resultset
	*/
	function dal_save($time_duration, $subject_name, $trainer_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$time_duration= mysqli_real_escape_string($con,$time_duration);
		$subject_name= mysqli_real_escape_string($con,$subject_name);
		$trainer_id= mysqli_real_escape_string($con,$trainer_id);
		
		
		//TRANSACTION
		//Query to turn auto transaction off
		mysqli_autocommit($con,FALSE);
		
		//Insert Query for first paper ID
		$query1 = "Insert into paper(trainer_id) values ($trainer_id)";
		$query_result1=mysqli_query($con,$query1);
		if(!$query_result1)
		{
			mysqli_close($con);
			return "Insert 1 failed";
		}
		else
		{
			$paper1 = mysqli_insert_id($con);//Gets the last inserted paper_id
			//Insert query for second paper id
			$query2 = "Insert into paper(trainer_id) values ($trainer_id)";
			$query_result2=mysqli_query($con,$query2);
			if(!$query_result2)
			{
				mysqli_close($con);
				return "Insert 2 failed";
			}
			else
			{
				$paper2 = mysqli_insert_id($con); //Gets the last inserted paper_id
				
				//Query for getting batch Id
				$queryS = "Select batch_id from batch_info where trainer_id = $trainer_id and active=1";
				$query_resultS=mysqli_query($con,$queryS);
				if(!$query_resultS)
				{
					mysqli_close($con);
					return "Select failed";
				}
				else
				{
					$resultSet = mysqli_fetch_array($query_resultS);
					$batch_id = $resultSet["batch_id"];
					
					$query = "Insert into subject_detail(subject_name) values ($subject_name)";
					$query_result=mysqli_query($con,$query);
					if(!$query_result)
					{
						mysqli_close($con);
						return "Subject not inserted";
					}
					else
					{
						$resultSet = mysqli_fetch_array($query_result);
						$subject_id = mysqli_insert_id($con);
						//Query for Inserting the new test details
						$queryI ="Insert into master_test(subject_id, paper_1, paper_2,main_paper,batch_id,status_id, time_duration)
						 values($subject_id, $paper1, $paper2, $paper1, $batch_id ,1, $time_duration)";
						$query_resultI=mysqli_query($con,$queryI);
						if($query_resultI)
						{
							mysqli_commit($con);
							if(mysqli_commit($con))
							{
								mysqli_close($con);
								return true;
							}
							else
							{
								mysqli_close($con);
								return "Commit failed";
							}
						}
						else
						{
							mysqli_close($con);
							return "Insert 3 failed";
						}
					}
				}
			}
		}	
	}
	
	function dal_save2($time_duration, $subject_name, $trainer_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$time_duration= mysqli_real_escape_string($con,$time_duration);
		$subject_name= mysqli_real_escape_string($con,$subject_name);
		$trainer_id= mysqli_real_escape_string($con,$trainer_id);
		//Query
		$query1 = "Insert into paper(trainer_id) values ($trainer_id),($trainer_id)";
		$query_result1=mysqli_query($con,$query1);
		if(!$query_result1)
		{
			mysqli_close($con);
			return "Insert into paper failed";
		}
		else
		{
			$query2 = "Select (max(paper_id)-2) as paper1, (max(paper_id)-1) as paper2 from paper";
			$query_result2=mysqli_query($con,$query2);
			if(!$query_result2)
			{
				mysqli_close($con);
				return "Select last insert failed"; 
			}
			else
			{
				$resultSet = mysqli_fetch_array($query_result2);
				$paper1 = $resultSet["paper1"];
				$paper2 = $resultSet["paper2"];
				$queryS = "Select batch_id from batch_info where trainer_id = $trainer_id and active=1";
				$query_resultS=mysqli_query($con,$queryS);
				if(!$query_resultS)
				{
					mysqli_close($con);
					return "Selecting batch_id failed";
				}
				else
				{
					$resultSet = mysqli_fetch_array($query_resultS);
					$batch_id = $resultSet["batch_id"];
					
					$query = "Insert into subject_detail(subject_name) values ('$subject_name')";
					$query_result=mysqli_query($con,$query);
					if(!$query_result)
					{
						mysqli_close($con);
						return "Subject not inserted";
					}
					else
					{
						
						$subject_id = mysqli_insert_id($con);
						$query ="Insert into master_test(subject_id, paper_1, paper_2,main_paper,batch_id,status_id, time_duration)
						 values($subject_id, $paper1, $paper2, $paper1, $batch_id ,1, $time_duration)";
						$query_result=mysqli_query($con,$query);
						if($query_result)
						{
							mysqli_close($con);
							return true;
						}
						else
						{
							echo $subject_id;
							echo $paper1;
							echo $paper2;
							echo $paper1;
							echo $batch_id;
							echo $time_duration;
							mysqli_close($con);
							return "Insert into master failed";
						}
					}
				}
			
			}
		
		}
	}
	
	
	/*
	This function gets the details of a particular test Id
	
	Returns:
			array:	An array containing the subject Id, name and time duration
			true:	If query is successful, but there are test Id in the database
			false:	If query is unsuccessful
	*/
	function dal_getTestDetails($test_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$test_id= mysqli_real_escape_string($con,$test_id);
		//Query
		$query = "Select subject_detail.subject_id, subject_detail.subject_name, master_test.time_duration,
				master_test.paper_1, master_test.paper_2, master_test.main_paper
				from subject_detail, master_test where subject_detail.subject_id = master_test.subject_id
				AND master_test.test_id = $test_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$testDetails = array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$testDetails_Details = array();
					$testDetails_Details["subject_id"] = $resultSet["subject_id"];
					$testDetails_Details["subject_name"] = $resultSet["subject_name"];
					$testDetails_Details["time_duration"] = $resultSet["time_duration"];
					$testDetails_Details["paper_1"] = $resultSet["paper_1"];
					$testDetails_Details["paper_2"] = $resultSet["paper_2"];
					$testDetails_Details["main_paper"] = $resultSet["main_paper"];
					$testDetails [] = $testDetails_Details;	
				}
				mysqli_close($con);
				return $testDetails_Details;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
			
	
	/*
	This function gets the subjects which contains test papers which are not yet approved
	
	Returns:
			array:	An array containing the subject Id, name and test_id's
			true:	If query is successful, but no data in the database
			false:	If query is unsuccessful
	*/
	function dal_getTestPapersToBeApproved($trainer_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainer_id= mysqli_real_escape_string($con,$trainer_id);
		//Query
		$query = "Select subject_detail.subject_id, subject_detail.subject_name, master_test.test_id
				 from subject_detail, master_test where master_test.subject_id = subject_detail.subject_id 
				 AND master_test.batch_id in (SELECT batch_id from batch_info where trainer_id = $trainer_id)
				 AND master_test.status_id = 1";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$toBeApprovedPapers = array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$toBeApprovedPapers_Detail = array();
					$toBeApprovedPapers_Detail["subject_id"] = $resultSet["subject_id"];
					$toBeApprovedPapers_Detail["subject_name"] = $resultSet["subject_name"];
					$toBeApprovedPapers_Detail["test_id"] = $resultSet["test_id"];
					$toBeApprovedPapers[] = $toBeApprovedPapers_Detail;
				}
				mysqli_close($con);
				return $toBeApprovedPapers;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/*
	This function deletes a test record
	
	Returns:
			true:	Delete Successful
			false:	If query is unsuccessful or no data in the database
	*/
	function dal_delete($test_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$test_id= mysqli_real_escape_string($con,$test_id);
		//Query
		$query1 = "Select paper_1, paper_2 from master_test where test_id = $test_id";
		$query_result1=mysqli_query($con,$query1);
		if(!$query_result1)
		{
			mysqli_close($con);
			return false;
		}
		else
		{
			$resultSet=mysqli_fetch_array($query_result1);
			$paperId1 = $resultSet["paper_1"];
			$paperId2 = $resultSet["paper_2"];
			
			$query2 = "Delete from master_test where test_id = $test_id";
			$query_result2=mysqli_query($con,$query2);
			if(!$query_result2)
			{
				mysqli_close($con);
				return false;
			}
			else
			{
				$query3 = "Delete from paper where paper_id = $paperId1 AND paper_id = $paperId2";
				$query_result3=mysqli_query($con,$query3);
				if($query_result3)
				{
					mysqli_close($con);
					return true;
				}
				else
				{
					mysqli_close($con);
					return false;
				}
			}
		}
	}
	
	/* 
	This function returns the student name
	
	Returns:
			string: Returns the name
			true: If query executes but no data in database
			false: Query did not execute
	*/
	function dal_getTraineeName($trainee_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainee_id= mysqli_real_escape_string($con,$trainee_id);
		//Query
		$query = "Select trainee.name from trainee where trainee.trainee_id = $trainee_id";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$traineename = $resultSet["name"];
				mysqli_close($con);
				return $traineename;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	/*This function gets the current batch name and ID of the trainer that has logged in
	
	Returns:
			array: current batch name and ID of the trainer that has logged in
			true: If query executes, but no data in the database
			false: If the query did not execute
	*/
	function dal_getCurrentBatch($trainer_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$trainer_id= mysqli_real_escape_string($con,$trainer_id);
		//Query
		$query = "Select batch_name, batch_id from batch_info where trainer_id = $trainer_id and active = 1";
		$query_result=mysqli_query($con,$query);
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$currentBatch = array();
				while($resultSet=mysqli_fetch_array($query_result))
				{
					$currentBatch_Detail = array();
					$currentBatch_Detail["batch_name"] = $resultSet["batch_name"];
					$currentBatch_Detail["batch_id"] = $resultSet["batch_id"];
					$currentBatch[] = $currentBatch_Detail;
				}
				mysqli_close($con);
				return $currentBatch;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	
	/* 
	This function gets the objective questions ID of a particular paper
	
	Returns:
				true:	If query is successful, but there are no objective questions
				array:	An array of all the objective questions ID of a particular paper
				false:	If the query is unsuccessful
	*/
	function dal_getObjectiveQuestionId($paper_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		//Query
		$query="Select question_id from test_question_obj where paper_id=$paper_id";
		$query_result=mysqli_query($con,$query);
	
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$question_Id = array();
				while($resultSet = mysqli_fetch_array($query_result))
				{
					$question_Id_Detail = array();
					$question_Id_Detail["question_id"] = $resultSet["question_id"];
					$question_Id[] = $question_Id_Detail;
				}
				mysqli_close($con);
				return $question_Id ;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	/*
	This function gets the Objective Question Content, marks, difficulty ID and level name, 4 options and correct answer 
	of a particular Objective question
	
	Returns:
				true:	If query is successful, but there is no objective question content
				array:	Returns the objective question Content, marks, difficulty ID, 4 options and correct answer  
						and level name of a particular objective question
				false:	If the query is unsuccessful
	*/
	function dal_getObjectiveQuestionContent($question_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$question_id = mysqli_real_escape_string($con, $question_id);
		//Query
		$query = "Select questions_obj.question, questions_obj.difficulty_level, questions_obj.marks, difficulty_levels.level_name,
				questions_obj.op_1, questions_obj.op_2, questions_obj.op_3, questions_obj.op_4, questions_obj.answers
				from questions_obj, difficulty_levels where question_id=$question_id
				AND questions_obj.difficulty_level = difficulty_levels.dlevel_id";
		$query_result=mysqli_query($con,$query);
		
		if($query_result)
		{
			if(mysqli_num_rows($query_result)>0)
			{
				$resultSet=mysqli_fetch_array($query_result);
				$question_ObjDetail = array();
				$question_ObjDetail[] = $resultSet['question'];
				$question_ObjDetail[] = $resultSet['marks'];
				$question_ObjDetail[] = $resultSet['difficulty_level'];
				$question_ObjDetail[] = $resultSet['level_name'];
				$question_ObjDetail[] = $resultSet['op_1'];
				$question_ObjDetail[] = $resultSet['op_2'];
				$question_ObjDetail[] = $resultSet['op_3'];
				$question_ObjDetail[] = $resultSet['op_4'];
				$question_ObjDetail[] = $resultSet['answers'];
					
				mysqli_close($con);
				return $question_ObjDetail;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}
	
	/*
							-----Only those questions can be deleted that are present 
									in a test paper that is in the preparing stage -----
	
	This function will delete a subjective question of a particular paper
	
	Returns:
			true: If delete successful
			false: If query failed
	*/
	function dal_deleteSubjectiveQuestion($paper_id,$sq_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		$sq_id = mysqli_real_escape_string($con, $sq_id);
		
		//Query for deleting from child table
		$query = "Delete from test_question_subj, master_test where 
				 test_question_subj.paper_id = $paper_id and 
				 test_question_subj.sq_id = $sq_id and
				 (test_question_subj.paper_id = master_test.paper_1 OR test_question_subj.paper_id = master_test.paper_2) and
				 master_test.status_id = 1"; //Checks if paper in "preparing" status
		$query_result=mysqli_query($con,$query);
		
		if(!$query_result)
		{
			return false;
		}
		else
		{
			//Query for deleting from parent table
			$query2 = "Delete from questions_subj where sq_id = $sq_id";
			$query_result2=mysqli_query($con,$query2);
		
			if($query_result2)
			{
				return true;
			}
			else
			{
				return false;
			}
		}			
	}
	
	
	/*
							-----Only those questions can be deleted that are present 
									in a test paper that is in the preparing stage -----
	
	This function will delete a objective question of a particular paper
	
	Returns:
			true: If delete successful
			false: If query failed
	*/
	function dal_deleteObjectiveQuestion($paper_id,$question_id)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$paper_id = mysqli_real_escape_string($con, $paper_id);
		$question_id = mysqli_real_escape_string($con, $question_id);
		
		//Query for deleting from child table
		$query = "Delete from test_question_obj, master_test where 
				 test_question_obj.paper_id = $paper_id and 
				 test_question_obj.question_id = $question_id and
				 (test_question_obj.paper_id = master_test.paper_1 OR test_question_obj.paper_id = master_test.paper_2) and
				 master_test.status_id = 1"; //Checks if paper in "preparing" status
		$query_result=mysqli_query($con,$query);
		
		if(!$query_result)
		{
			return false;
		}
		else
		{
			//Query for deleting from parent table
			$query2 = "Delete from questions_obj where question_id = $question_id";
			$query_result2=mysqli_query($con,$query2);
		
			if($query_result2)
			{
				return true;
			}
			else
			{
				return false;
			}
		}			
	}
	
	
	/* 
	Function to change trainer password
	This function will accept trainerId, oldPassword, newPassword.
	Checks whether the entered old password matches the one stored in database.
	
	Returns : 
			false  - Query error
	        true   - if password is updated successfully
	*/  	  
	function dal_changePassword($trainerId, $oldPassword,$newPassword)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$trainerId= mysqli_real_escape_string($con,$trainerId);
		$oldPassword= mysqli_real_escape_string($con,$oldPassword);
		$newPassword= mysqli_real_escape_string($con,$newPassword);
		//Query
		//checking if old password matches with the database
		$check_query="SELECT trainer_id FROM trainer WHERE passwd='$oldPassword'";
		$result = mysqli_query($con,$check_query);
		if($result)
		{
			if(mysqli_num_rows($result)>0)
			{
				$update_query = "Update trainer set passwd='$newPassword' where trainer_id = $trainerId";
				$result1 = mysqli_query($con,$update_query);
				if($result1)
				{
					mysqli_close($con);
					return true;
				}
				else
				{
					mysqli_close($con);
					return false;
				}
			}
			else
			{
				mysqli_close($con);
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/* 
	Function to change trainer profile picture
	This function will accept trainerId, newPic.
	Returns : 
			false  - in case of query error
	        true   - if Profile picture is updated successfully
	*/
	
	function dal_changeProfilePicture($newPic, $trainerId)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$newPic= mysqli_real_escape_string($con,$newPic);
		$trainerId= mysqli_real_escape_string($con,$trainerId);
		//Query
		$query = "UPDATE trainer SET profile_pic = '$newPic' where trainer_id = $trainerId";
		$result = mysqli_query($con,$query);
		if($result)
		{
			mysqli_close($con);
			return true;
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}

	/*
	Function to display paper status
	This function will return the paper status i.e. Prepared/Approved/Cancelled
	Returns :
			false  - in case of query error
	        true   - No rows present, which match the query
          	string - the status of the paper
	*/

	function dal_getPaperStatus($paperId)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$paperId= mysqli_real_escape_string($con,$paperId);
		//Query
		$query = "SELECT status from paper_status WHERE pstat_id = $paperId";
		$result = mysqli_query($con, $query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				$result_set = mysqli_fetch_array($result);
				$pstatus = $result_set["status"];
				mysqli_close($con);
				return $pstatus;
			}
			else
			{
				// No rows present, which match the select query
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			// If the query fails
			mysqli_close($con);			
			return false;
		}
	}
	
	/*
	This  function returns the difficulty level of the question
	Returns:
			false -incase of query error
			true-if query executes, but resultset has no value
			array- array of difficulty levels
	*/

	function dal_getDifficultyLevel()
	{
		// connect to database
		$con=dbConnect();
		//Query
		$q="SELECT * FROM difficulty_levels";
		$exeQuery=mysqli_query($con,$q);
		if($exeQuery)
		{
			if(mysqli_num_rows($exeQuery)>0)
			{
				$diffLevel=array(); 
				while($result_set=mysqli_fetch_array($exeQuery))
				{
					$diffLevelDetail= array(); 

					$diffLevelDetail["lid"]=$result_set["dlevel_id"];
					$diffLevelDetail["level"]=$result_set["level_name"];

					$diffLevel[]= $diffLevelDetail;
				}
				mysqli_close($con);	
				return $diffLevel;
			}
			else
			{
				mysqli_close($con);	
				return true;
			}
		}
		else
		{
			mysqli_close($con);	
			return false;
		}
	}
	
	/*
	This function inserts the objective questions  to the database
	Returns:
			false- incase of query error
		    true-if insert is successful
		   
	*/

	function dal_setObjectiveQuestion($question,$op_1,$op_2,$op_3,$op_4,$difficulty_level,$answer,$marks)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$question= mysqli_real_escape_string($con,$question);
		$op_1= mysqli_real_escape_string($con,$op_1);
		$op_2= mysqli_real_escape_string($con,$op_2);
		$op_3= mysqli_real_escape_string($con,$op_3);
		$op_4= mysqli_real_escape_string($con,$op_4);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		$answer= mysqli_real_escape_string($con,$answer);
		$marks= mysqli_real_escape_string($con,$marks);
		//Query
		$q="INSERT INTO questions_obj(question,op_1,op_2,op_3,op_4,difficulty_level,answers,marks)
			VALUES('$question','$op_1','$op_2','$op_3','$op_4',$difficulty_level,'$answers',$marks)"; 
		$exeQuery=mysqli_query($con,$q);
		if($exeQuery)
		{
			mysqli_close($con);	
			return true;
		}
		else
		{
			mysqli_close($con);	
			return false;
		}
	}
	
	/*
	This function inserts the subjective questions  to the database
	Returns:
			false- incase of query error
			true-if insert is successful
		
	*/
	
	function dal_setSubjectiveQuestions($question,$marks,$difficulty_level)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$question= mysqli_real_escape_string($con,$question);
		$marks= mysqli_real_escape_string($con,$marks);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		//Query
		$q= "INSERT INTO questions_subj(question,marks,difficulty_level) 
			VALUES('$question',$marks,$difficulty_level)";
		$exeQuery=mysqli_query($con,$q);
		if($exeQuery)
		{
			mysqli_close($con);	
			return true;
		}
		else
		{
			mysqli_close($con);	
			return false;
		}
	}

	/*
	This function8 gets the maximum objective score of a particular paper
	Returns :
			int-total marks
  		    true - if query executes successfully without returning any values
		    false- for unsuccessful execution of query	
	*/

	function dal_getMaximumObjectiveScore($paperid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$paperid= mysqli_real_escape_string($con,$paperid);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query="select sum(qo.marks) as marks from test_question_obj tqo, questions_obj qo
            where tqo.paper_id=$paperid
            and tqo.question_id=qo.question_id";
        $result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				$resultDetail = array();
				$resultSet = mysqli_fetch_array ($result);
				$resultDetail['marks'] = $resultSet['marks'];
				mysqli_close($con);
				return $resultDetail;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}

	/*
	This function accepts paper id and trainee id as a parameter and
	Returns : 
			int-total marks scored by the trainee
			true - if query executes successfully without returning any values
			false- for unsuccessful execution of query	
	*/
	function dal_getObjectiveScore($traineeid,$paperid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$traineeid= mysqli_real_escape_string($con,$traineeid);
		$paperid= mysqli_real_escape_string($con,$paperid);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}

		$query = "select sum(ado.marks) as scoredMarks from answers_obj ao,answer_detail_obj ado
              where ao.trainee_id=$traineeid
              and ao.paper_id=$paperid
              and ao.answer_id = ado.answer_id";
        $result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{

				$resultDetail = array();
				$resultSet = mysqli_fetch_array ($result);
				$resultDetail['scoredMarks'] = $resultSet['scoredMarks'];
				mysqli_close($con);
				return $resultDetail;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}	
	}
	
	/*
	This functions accepts paper id,question id and trainee id 
    Returns:
			array-array with values "question", "answer" and "marks" if query returns a array_count_values,
			true - if query does not return anything but executes successfully
			false- if query does not execute successfully
	*/

	function dal_getSubjectiveAnswer($pid,$qid,$trainee_id)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$pid= mysqli_real_escape_string($con,$pid);
		$qid= mysqli_real_escape_string($con,$qid);
		$trainee_id= mysqli_real_escape_string($con,$trainee_id);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query="select q.question,a.answer,q.marks from question_subj q,answer_detail_subj a,answers_subj asb
				where q.sq_id=a.sq_id
				and asb.sanswer_id=a.answer_id
				and q.sq_id=$qid
				and asb.trainee_id=$trainee_id
				and asb.paper_id=$pid";
		$result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				$subjarray=array();
				$result_set = mysqli_fetch_array($result);
				$subjarray["question"]=$result_set["question"];
				$subjarray["answer"]=$result_set["answer"];
				$subjarray["marks"]=$result_set["marks"];
				mysqli_close($con);
				return $subjarray;
			}
			else
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}

	/*
		This function returns the trainee profile picture and name
		Returns:
				array-array with keys as"trainee_name" and "profile_pic"
				true  - if query executes successfully but does not return any value
				false - if query does not execute successfully
	*/

	function dal_getTraineePictureAndName($tid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$tid= mysqli_real_escape_string($con,$tid);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query="select name,profile_pic from trainee where trainee_id=$tid";
		$result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				$traineearray=array();
				$result_set = mysqli_fetch_array($result);
				$trainerarray["trainee_name"]=$result_set["name"];
				$trainerarray["profile_pic"]=$result_set["profile_pic"];
				mysqli_close($con);
				return $traineearray;
			}
			else 
			{
				mysqli_close($con);
				return true;
			}	
		}
		else
		{
			mysqli_close($con);
			return false;
		}
    }

	/*
		This function gets the trainer details
		Returns :
			array-array with indexes "trainer_name","phone_no","email_id","profile_pic" if select query returns values
			true  - if query executes successfully but does not return any value
			false - if query does not execute successfully 
	*/
	function dal_getTrainerDetail($tid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$tid= mysqli_real_escape_string($con,$tid);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query="select name,profile_pic,ph_no,email from trainer where trainer_id=$tid";
		$result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				$trainerarray=array();
				$result_set = mysqli_fetch_array($result);
				$trainerarray["trainer_name"]=$result_set["name"];
				$trainerarray["phone_no"]=$result_set["ph_no"];
				$trainerarray["email_id"]=$result_set["email"];
				$trainerarray["profile_pic"]=$result_set["profile_pic"];
				mysqli_close($con);
				return $trainerarray;
			}
			else 
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
    }
	
	/*
		This function gets the trainer picture and name 
		Returns:
			array-array with keys as"trainer_name" and "profile_pic"
			true -if query executes successfully but does not return any value
			false-if query does not execute successfully
	*/
	function dal_getTrainerPictureAndName($tid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$tid= mysqli_real_escape_string($con,$tid);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query="select name,profile_pic from trainer where trainer_id=$tid";
		$result=mysqli_query($con,$query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
				
				$result_set = mysqli_fetch_array($result);
				$trainerarray=array();
				$trainerarray["name"]=$result_set["name"];
				$trainerarray["profile_pic"]=$result_set["profile_pic"];
				mysqli_close($con);
				return $trainerarray;
			}
			else 
			{
				mysqli_close($con);
				return true;
			}
		}
		else
		{
			mysqli_close($con);
			return false;
		}
    }

	/*
    This function updates the marks and comments for a particular subjective question
    Returns :
			true - if updated successfully
            false- if updation is not successful
	*/
	function dal_setMarkAndComment($trainee_id,$sq_id,$paper_id,$marks,$comment)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$trainee_id= mysqli_real_escape_string($con,$trainee_id);
		$sq_id= mysqli_real_escape_string($con,$sq_id);
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		$marks= mysqli_real_escape_string($con,$marks);
		$comment= mysqli_real_escape_string($con,$comment);
		//Query
		if(mysqli_connect_errno())
		{
			die("Could not connect to Database"); 
		}
		$query ="Update answer_detail_subj set marks=$marks, comments='$comment' where 
				sanswer_id=(SELECT sanswer_id from answers_subj WHERE trainee_id=$trainee_id and paper_id=$paper_id) and
				sq_id=(SELECT sq_id from questions_subj where sq_id=$sq_id)";
		$result = mysqli_query($con,$query);
		if($result)
		{
			mysqli_close($con);
			return true;
		}
		else
		{
			mysqli_close($con);
			return false;
		}
	}  

/*function dal_getAllTestTopics($username)
	{
		$subjects = array 
		(
			array("subject_name"=>"C#","subject_id"=>101),
			array("subject_name"=>"SQL","subject_id"=>102),
			array("subject_name"=>"HTML","subject_id"=>103),
			array("subject_name"=>"ADO.NET","subject_id"=>104)
		);
		return $subjects;
	}
*/
	
/* Traniee Login Verifier */
	function trainee_login($username,$password){
    
		// connect to database, and get the connection handle
		$con = dbConnect();
		// scrubbing user input
		$username = mysqli_real_escape_string($con,$username);
		$password = mysqli_real_escape_string($con,$password);
		// forming a query
		$select_query = "SELECT * FROM ".DB_NAME.".trainer WHERE trainer_id ='$username' AND passwd='$password' "; 
		//  echo $select_query;
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) == 1)
			{
			   // Correct details, login
			   $result_set = mysqli_fetch_array($result);
			   $user_id = $result_set["trainer_id"];
			   $name= $result_set["name"];
			   $profile_pic= $result_set["profile_pic"];
			   $status=$result_set["active"];
			   
			   
			   session_start();
			   $_SESSION['user_id'] = $user_id;
			   $_SESSION['name'] = $name;
			   $_SESSION['profile_pic'] = $profile_pic;
			   $_SESSION['status'] = $status;
			   $_SESSION['type'] = "Trainer";
			   
			   
			   return 1; //true;
			}
			else
			{
				return 2;//false;
			}
		}
		else
		{
			return 3; //false;
		}
	}
	
	
	
	/* 
	Function to change the paperstatus when sent for approval.
	This function will accept testid
	Returns : 
			false  - in case of query error
	        true   - if paperstatus is updated successfully
	*/
	function dal_sendForApproval($testid)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$testid= mysqli_real_escape_string($con,$testid);
		
		//Query
		$query = "UPDATE master_test set status_id=2 where test_id=$testid";
		$result = mysqli_query($con,$query);
		if($result)
		{
			mysqli_close($con);
			return true;
		}
		else
		{
			mysqli_close($con);
			return false;
		}
		
	}
	/* 
	This function updates the objective question  to the database
	Returns:
			false- incase of query error
		    true-if insert is successful
	*/
	
	function dal_updateObjectiveQuestion($question_id,$question,$op_1,$op_2,$op_3,$op_4,$difficulty_level,$answer,$marks)
	{
		$con=dbConnect();
		//Sanitization
		$question_id= mysqli_real_escape_string($con,$question_id);
		$question= mysqli_real_escape_string($con,$question);
		$op_1= mysqli_real_escape_string($con,$op_1);
		$op_2= mysqli_real_escape_string($con,$op_2);
		$op_3= mysqli_real_escape_string($con,$op_3);
		$op_4= mysqli_real_escape_string($con,$op_4);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		$answer= mysqli_real_escape_string($con,$answer);
		$marks= mysqli_real_escape_string($con,$marks);
		//Query
		$query="update questions_obj set question='$question',op_1='$op_1',op_2='$op_2',op_3='$op_3',
			op_4='$op_4',difficulty_level=$difficulty_level,answers='$answer',marks=$marks
			where question_id=$question_id"; 
		$exeQuery=mysqli_query($con,$query);
		if($exeQuery)
		{
			mysqli_close($con);	
			return true;
		}
		else
		{
			mysqli_close($con);	
			return false;
		}
		
	}
	
	
	
	
	/*
	This function inserts the objective question  to the database
	
	Returns:
			true:	If insert query is successful
			false:	If query is unsuccessful
	*/
	function dal_insertQuestionObjective($paper_id, $question,$op_1,$op_2,$op_3,$op_4,$difficulty_level,$answers,$marks)
	{
		//Connect to Database
		$con=dbConnect();
		//Sanitization
		$question = mysqli_real_escape_string($con,$question);
		$op_1= mysqli_real_escape_string($con,$op_1);
		$op_2= mysqli_real_escape_string($con,$op_2);
		$op_3= mysqli_real_escape_string($con,$op_3);
		$op_4= mysqli_real_escape_string($con,$op_4);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		$answers= mysqli_real_escape_string($con,$answers);
		$marks= mysqli_real_escape_string($con,$marks);
		//Query
		$query="INSERT INTO questions_obj(question,op_1,op_2,op_3,op_4,difficulty_level,answers,marks)
			VALUES('$question','$op_1','$op_2','$op_3','$op_4',$difficulty_level,'$answers',$marks)"; 
		$query_result=mysqli_query($con,$query);
		if(!$query_result)
		{
			mysqli_close($con);
			return false;
		}
		else
		{
			$question_id = mysqli_insert_id($con);//Last inserted value
			$insert_query = "Insert into test_question_obj(paper_id) values($paper_id) where question_id = $question_id";
			$insert_query_result = mysqli_query($con,$insert_query);
			if($insert_query_result)
			{
				mysqli_close($con);
				return true;
			}
			else
			{
				mysqli_close($con);
				return false;
			}
		}
		
	}	
	
	/*
	This function inserts the subjective question  to the database
	
	Returns:
			true:	If insert query is successful
			false:	If query is unsuccessful
	*/
	function dal_insertSubjectiveQuestions($paper_id,$question,$marks,$difficulty_level)
	{
		// connect to database
		$con=dbConnect();
		//Sanitization
		$paper_id= mysqli_real_escape_string($con,$paper_id);
		$question= mysqli_real_escape_string($con,$question);
		$marks= mysqli_real_escape_string($con,$marks);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		//Query
		$q= "INSERT INTO questions_subj(question,marks,difficulty_level) 
			VALUES('$question',$marks,$difficulty_level)";
		$query_result=mysqli_query($con,$q);
		if(!$query_result)
		{
			mysqli_close($con);
			return false;
		}
		else
		{
			$sq_id = mysqli_insert_id($con);//Last inserted value
			$insert_query = "Insert into test_question_obj(paper_id) values($paper_id) where sq_id = $sq_id";
			$insert_query_result = mysqli_query($con,$insert_query);
			if($insert_query_result)
			{
				mysqli_close($con);
				return true;
			}
			else
			{
				mysqli_close($con);
				return false;
			}
		}
		
	}	
	
	/* 
	This function updates the subjective question  to the database
	Returns:
			false- incase of query error
		    true-if insert is successful
	*/
	
	function dal_updateSubjectiveQuestion($sq_id,$question,$marks,$difficulty_level)
	{
		$con=dbConnect();
		//Sanitization
		$sq_id= mysqli_real_escape_string($con,$sq_id);
		$question= mysqli_real_escape_string($con,$question);
		$marks= mysqli_real_escape_string($con,$marks);
		$difficulty_level= mysqli_real_escape_string($con,$difficulty_level);
		//Query
		$query="update questions_subj set question='$question',difficulty_level=$difficulty_level,marks=$marks
			where sq_id=$sq_id"; 
		$exeQuery=mysqli_query($con,$query);
		if($exeQuery)
		{
			mysqli_close($con);	
			return true;
		}
		else
		{
			mysqli_close($con);	
			return false;
		}
		
	}
	
?>

