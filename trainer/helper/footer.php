<script src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/bootstrap-3.3.6/js/bootstrap.min.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script>
// $('#slide-out').removeClass('hidden-xs');  
 $("#slide-out").toggleClass("toggled");
  $(".button-collapse").sideNav();
  // Show sideNav
   //$(".button-collapse").sideNav('show');
  // Hide sideNa
 // $('.button-collapse').sideNav('hide');  
</script>

<?php
	/* Choose the js files based on which page is rendered */
	switch ($pageTitle_Trainer) {
		case "AccountSettings_Trainer":
		?>
			<script src="assets/js/AccountSettings.js"></script>
			<script src="assets/js/dropzone.min.js"></script>
			<script src="assets/css/dropzone.min.css"></script>
			<script type="text/javascript">
				Dropzone.options.mydz = {
					accept: function(file, done) {
							console.log(file);
							if (file.type != "image/jpeg" && file.type !="image/jpg" && file.type !="image/png" && file.type !="image/gif") {
								done("Error! Files of this type are not accepted");
							}
							else { done(); }
							},
					init: function() {
							this.on("addedfile", function() {
							  if (this.files[1]!=null){
								this.removeFile(this.files[0]);
							  }
							});
					},
					success: function(file, response){
						
							//dictInvalidFileType : "Invalid file format"
							location.reload();        
					//alert(file.name); // <---- here is your filename
					}
				}
			</script>
		<?php
			break;
		case "EvaluateTest_Trainer":
		?>
			<script src="assets/js/EvaluateTest.js"></script>
		<?php
			break;
		case "PendingEvaluation_Trainer":
		?>
			<script src="assets/js/PendingEvaluation.js"></script>
		<?php
			break;
		case "PendingPapers_Trainer":
		?>
			<script src="assets/js/PendingPaper.js"></script>
		<?php
			break;
		case "SetupQuestionPaper_Trainer":
		?>
			<script src="assets/js/SetupQuestionPaper.js"></script>
		<?php
			break;
		case "SetupTest_Trainer":
		?>
			<script src="assets/js/SetupTest.js"></script>
		<?php
			break;
		case "TrainerDashBoard_Trainer":
		?>
			<script src="assets/js/TrainerDashBoard.js"></script>
			<script src='assets/Chart.js-master/Chart.js'></script>
		<?php
			break;
		default:
			echo htmlspecialchars("default");
	}
?>


<footer class="blue page-footer" style= "margin-bottom:0%;">
	<div class="container" >
        <div class="row" style="height=20%; ">
            <div class="col l6 s12">
				<h5 class="white-text" style="margin-left:15%;">  © ITC INFOTECH LTD </h5>
			</div>
		</div>
	</div>
</footer>

</body>
</html>