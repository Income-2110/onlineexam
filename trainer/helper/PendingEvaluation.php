<main >
<div class="container">
    <div class="row">
        <div class="col l5">
            <div class="panel panel-default">
				<div class="panel-heading score-header">Student PSID</div>
                        <!-- List group -->
                    <ul class="list-group" >
                        <?php
							//$subject_id= 1; // $subject_id=$_SESSION['paperid_PendingEvaluation'];

							$subject_id=$_SESSION['paperid_PendingEvaluation']; //3;
							
                            $traineeNamePsidStatus = dal_getTraineeIDsWithPendingEvaluation($subject_id); #TODO obtain correct trainerID
                            
							/*if(is_bool($traineeNamePsidStatus))
								{
									if($traineeNamePsidStatus == true)
									{
									?>
										<h4>Currently there are no papers to be evaluated</h4>
									<?php				
									}
									elseif($traineeNamePsidStatus == false)
									{
									?>
										<h4>Oops! There occurred an issue with the Database</h4>
									<?php
									}
								}
							elseif(is_array($traineeNamePsidStatus))
								{
									/*$t_name=$traineeNamePsidStatus[$i][0];
									$trainee_name=$_SESSION['t_name'];*/
                                   
							
									foreach($traineeNamePsidStatus as $traineeStatus)
										{
										?>
											<li class="list-group-item score_list">
												<h5 class="test_name_header">                                        
                         
													<div class="media">
														<div class="media-body">
															<h4 class="media-heading">
																<p><?php echo htmlspecialchars($traineeStatus["trainee_id"]); ?>
																	<a id="<?php echo $traineeStatus["trainee_id"]; ?>" class="btn waves-effect waves-light traineeNamesInPendingEvaluation" style="background-color:#47A7F3; margin-left:120pt" type="submit" name="action">Evaluate</a>
											<?php
												if($traineeStatus["total_marks"]!= -1)
												{
											?>
													<i class="fa fa-check" style="color:green;margin-left:100pt;"></i>
											<?php
												}
											?>
																</p>
															</h4>
														</div>
								
												</h5>
											</li>
										<?php
										/*}*/
								}
										?>
                    </ul>
                </div> 
            </div>
		</div>
	</div>
</div>
</main>