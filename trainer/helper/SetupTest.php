<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<main>
<div class="container">								
    <div class="row">
        <div class="col l12 s12">
            <div class="panel panel-default">
				<div class="panel-heading score-header">Setup Test</div>
                     <div class ="card-panel l12 s12">
						<div class="row">
							<div class="col l12 s8">
								<div class="panel-heading blue-text text-darken-2 ">Existing Tests</div>
								<div class ="card-panel 112 s12">
									<ul class="list-group" >
									<?php
										$username = $_SESSION['user_id'];
										$TestsToBeApproved=dal_getTestPapersToBeApproved($username);
										if(is_bool($TestsToBeApproved))
										{
											if($TestsToBeApproved == true)
											{
									?>
												<h4>Currently there are no students under this trainer</h4>
									<?php				
											}	
											elseif($TestsToBeApproved == false)
											{
									?>
												<h4>Oops! There occurred an issue with the Database</h4>
									<?php
											}
										}
										elseif(is_array($TestsToBeApproved))
										{
											foreach($TestsToBeApproved as $test_ToBeApproved)
											{
									?> 
												<li class="list-group-item score_list">
													<h5 class="test_name_header">                                        
														<div class="media">
															<div class="media-body">
																<p><?php echo htmlspecialchars ($test_ToBeApproved["subject_name"]);?></p>

																<input id="<?php echo $test_ToBeApproved["subject_id"]?>" class="saveinputhidden_top" type="hidden" style="border-color:transparent" value="<?php echo $test_ToBeApproved["subject_id"]?>"></input>
																	<div align="center" >
																	<a id="<?php echo $test_ToBeApproved["test_id"]; ?>" class="btn waves-effect waves-light btnExistingTest" style="background-color:#47A7F3;" type="submit" name="action">GO</a>
																	</div>

															</div>
														</div>
													</h5>
												</li>
									<?php
											}
										}
									?>
									</ul>
								</div>
							</div>
						</div>
						<div class="btn waves-effect waves-light btnNewTest" id="NewTestPaper_SetupTest" style="background-color:#47A7F3;width:140pt" type="submit" name="action">New Test</div>
					</div> 

					<div class="card-panel">
						<p id="Panel_Dummy_Message" style="margin-left:0%;margin-top:0%; color: blue;"> Click on any button to add a new test or edit the existing test details </p>
						<div class="row validate hide newtestcard">
							<div class="panel-heading blue-text text-darken-2 ">Test Details</div>
							<div class="col l12 s8">
								<div class="panel-body ">
									<div class="input-field col l6 s6 selectedTestTopic" id="reset">
										<input type="text" id="selectedTestTopic_SetupTest">
			
											<label style="font-size:8pt">Test topics</label>
										</input>
									</div>
									<?php
										$current_batch = dal_getCurrentBatch($username);
										$current_batch_id = $current_batch[0]["batch_id"];
										$TotalMarksOfEachTest = dal_getTotalMarksOfQuestionPaper($current_batch[0]["batch_id"]);
										
									?>
									<div class="input-field col l6 s6">
										<div class="input-field col l1 m1 s1"><label>Time:</label></div>
											<div class="input-field col l1 m1 s1">
												<p><input placeholder="hours" id="time1AllotedForTheTest" type="text" class="tdetails" />
													<label for="hours"></label>
											</div>
											<div class="input-field col l1 m1 s1"><input placeholder="mins" id="time2AllotedForTheTest" type="text" class="tdetails"/>
												<label for="mins"></label>
											</div>
													<!--<div class="input-field col s4"><input placeholder="mins" id="time2AllotedForTheTest" type="text" class="tdetails"/>
														<label for="mins"></label>
													</div>-->
												</p>
										
									</div><br/>
									<div class = "row">
									<div class="input-field col l1 m1 s1">
										<label for="testID" id="testIDLabel">Test ID:</label>
									</div>
									<div class="input-field col l0 m0 s0">
										<input id="testID"><?php ?></input>
										
									</div></div>
									<div class = "row">
									<div class="input-field col l1 m1 s1">
										<label for="testmarks">Total Marks:</label>
									</div>
									<div class="input-field col l0 m0 s0">
										<label id="marksPerQuestionPaperOne_SetupTest<?php echo $questionCounter_PaperOne ?>"><?php echo $TotalMarksOfEachTest ?></label>
										<br/>
									</div></div>
									
									
										<!--<div class="input-field col s12">
											Time: <input type="note-editable" style="width:20pt" id="time1AllotedForTheTest" value="" class="validate tdetails"></input>
											hr <input type="note-editable" style="width:20pt" id="time2AllotedForTheTest" class="validate tdetails"></input>min
											<!--<label style="font-size:8pt" for="timeAllotedForTheTest">Time:</label>
											
										</div>-->
										
									<div class="row col l12 s12 btnTestPaper">
									<br/>
										<div class="btn waves-effect waves-light setupTestPaperType" id="MainQuestionPaper_SetupTest" style="background-color:#47A7F3;width:140pt" type="submit" name="action">Main Paper</div>
										<input id="maininputhidden" type="hidden" style="border-color:transparent"></input>
										
										<div class="btn waves-effect waves-light setupTestPaperType" id="BackupQuestionPaper_SetupTest" style="background-color:#47A7F3;width:140pt" type="submit" name="action">Backup Paper</div>
										<input id="backupinputhidden" type="hidden" style="border-color:transparent"></input>

										
										<input id="saveinputhidden_btm" type="hidden" style="border-color:transparent" value="<?php echo $test_ToBeApproved["subject_id"]?>"></input>

										<input id="deleteinputhidden" type="hidden" style="border-color:transparent"></input>

									</div>
								</div>
							</div>
								
							<label id="errorMessage" style="font-size:12pt; color:red;margin-left:40pt"></label>
			            <div class="row col l12 s12">
							<div class="row col l6 s6" >
								<div class="blue waves-effect waves-light btn save-btn" id="btnSaveTestDetails_SetupTest">Save</div>&nbsp;&nbsp;
								<div class="blue waves-effect waves-light btn clearbtn" id="btnDeleteTestDetails_SetupTest">Delete test</div>					
							</div>	
							<br/><br/>
							<div   class="blue waves-effect waves-light btn save-btn btnSendForApproval_SetupTest">Send For Approval</div>     
								<input class="hide" id="storetestid" value="" type="text"></input>
						</div>
						</div>
					
					</div>
			</div>
				
		</div>
	</div>
</div>
</main>
<script>
$(document).ready(function() {
	$('select').material_select();
});
</script>