<main >
<div class="container" >

	<!--  For Tab -->
	<div class="col l3 m3 s3 z-depth-6 card-panel  blue-color">
		<span class="black-text text-darken-2" style="font-Size:190%">Manage Account</span>
	</div>
  
	<!-- The form-->
	<div class="col l9 m3 s3 z-depth-6 card-panel">	
		<div class="row">

			<div class="row">		
				<div class="input-field col l4 m2 s2  width-photo ">
					<?php
						$username =  $_SESSION['user_id']; #TODO use session variable
						$trainerpictureandname=dal_getTrainerPictureAndName($username);
						$trainer_detail=dal_getTrainerDetail($username);
						$current_batch=dal_getCurrentBatch($username);
					?>
					<div class="profile-userpic">
						<img src="images/<?php echo $trainerpictureandname["profile_pic"]; ?>" class="img-responsive" alt="There occured an error in the system">
					</div>
					<div class="profile-usertitle">
						<div class="file-path-wrapper">
							
							<!--<input class="file-path validate" type="text">-->
							<!--<label for="files"> <span class="btn" style="margin-top:60pt;">Select Photo</span></label> 
							<input style="visibility: hidden; position: absolute;" name="image" id="files" class="form-control" type="file" name="files" accept="image/*" >
							<br/>
							<input type="submit"/>
						-->
							<div class="dropzone" action="upload.php" id="mydz"> 
								<div class="dz-message" data-dz-message><div class="btn waves-effect blue-color" id="addphoto" >Add Photo</div></div>
							</div>
						</div>
					</div>
				</div>
				
					<div class="row input_data col l8">
						
							<!--<input placeholder=""  id="first_name" type="text" style="font-weight: bold;color:black;" class="validate"/>-->
							<div class="row">
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%">Name:</label>
								</div>
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%"><?php echo htmlspecialchars($trainerpictureandname["name"]);?> </label>								
									<!--<input  id="trainer_name" type="text" class="class=validate" value=<?php /*echo htmlspecialchars($trainerpictureandname["name"]);*/?> style="width:300pt;font-size:12pt;" readonly> </input> --> 
								</div>
							</div>
							<div class="row">
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%">Contact:</label>
								</div>
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%"><?php echo htmlspecialchars($trainer_detail["phone_no"]);?> </label>
									<!--<input value= id="mobile_no" type="text" class="class=validate" style="width:300pt;font-size:12pt;" readonly/> --> 
								</div>
							</div>
							<div class="row">
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%">Email:</label>
								</div>
								<div class="input-field col l2">
								<label class ="black-text text-darken-2" style="font-Size:120%"><?php echo htmlspecialchars($trainer_detail["email_id"]);?> </label>
									<!--<input value=<?php /*echo htmlspecialchars($trainer_detail["email_id"]);*/?>  id="email_id" type="text" class="class=validate"  style="width:300pt;font-size:12pt;" readonly/> -->
								</div>
							</div>
							<div class="row">
								<div class="input-field col l2">
									<label class ="black-text text-darken-2" style="font-Size:120%">Batch:</label>
								</div>
								<div class="input-field col l2">
								<label class ="black-text text-darken-2" style="font-Size:120%"><?php echo htmlspecialchars($current_batch[0]["batch_name"])?> </label>
									<!--<label id="currentBatch" for="Phone Number 1" style="font-size:12pt; color:Black;" class="Phone No" ><?php /*echo htmlspecialchars($current_batch[0]["batch_name"]);*/?></label>-->
									<!--<input value=  id="current_batch" type="text" class="class=validate"  style="width:300pt;font-size:12pt;" readonly/> -->
								</div>
							</div>
					</div>
			</div>
					<div class="row">
					<div class="input-field col l12">
					<div>
						<button class="btn waves-effect blue-color" type="change_password" id="changePassbtn" name="action" style="margin-top:10pt;margin-left:60%">
						Change password
						</button>
					</div>			
							
					</div>	
					</div>

				
			<label  id="error_label"  for="error_label" style="font-size:12pt; color:red;" class="password_class" ></label>
				<div class="row">
					<div class="input-field col l6 m6 s6">
					
					  <input placeholder="Old Password" id="old_password" type="password" class="class=validate hide changePass"/>
					  <label id="old_password1" for="old_password1" style="font-size:12pt; color:red;" class="password_class"></label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col l6 m6 s6">
						<input  placeholder="New Password" id="new_password" type="password" class="validate hide changePass"/>
						<label id="new_password1" for="new_password1" style="font-size:12pt; color:red;" class="password_class" ></label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col l6 m6 s6">
						<input  placeholder="Confirm new Password" id="confirm_new_password" type="password" class="validate hide changePass"/>
						<label  id="confirm_new_password1"  for="confirm_new_password" style="font-size:12pt; color:red;" class="password_class" ></label>
						
					</div>
				</div>
				<div class="row">
				<label  id="error_label2"  for="error_label2" style="font-size:12pt; color:red;" class="password_class" ></label>
				
				</div>

				<!--<button class="btn waves-effect blue-color" type="change_password" id="changePassbtn" name="action" style="margin-top:10pt;" float="right">
					 Change password
				</button><br/><br/><br/>-->
				<!--Button -->
				<button class="btn waves-effect blue-color hidden cancel_btn_class" type="submit" name="action" id="submit_btn">
				 Save Password
				</button>	
				<!--<button class="btn waves-effect blue-color" type="submit" name="action" id="submit1_btn">
				  Submit
				</button> -->
				<button class="btn waves-effect blue-color hidden cancel_btn_class" type="submit" name="action" id="cancel_btn" >
				 Cancel
				</button>
		</div>
	<!-- container-->
	</div>

</div>
</main>
