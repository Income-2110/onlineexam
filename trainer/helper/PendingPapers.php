<main >
<div class="container">
    <div class="row">
        <div class="col l5">
            <div class="panel panel-default">

				<div class="panel-heading score-header">Pending Papers</div>
                        <!-- List group -->
                    <ul class="list-group" >
                        <?php	
							$trainer_id = $_SESSION['user_id'];
                            $PendingPaperStatus = dal_getTheSubjectContainingPapersToBeEvaluated($trainer_id); #TODO obtain correct trainerID
							if(is_bool($PendingPaperStatus))
							{
								if($PendingPaperStatus == true)
								{
								?>
									<h4>Currently there are no papers to be evaluated</h4>
								<?php				
								}
								elseif($PendingPaperStatus == false)
								{
								?>
									<h4>Oops! There occurred an issue with the Database</h4>
								<?php
								}
							}
							else if(is_array($PendingPaperStatus))
								{
									foreach($PendingPaperStatus as $PendingPaper)
									{
									?>
										<li class="list-group-item score_list">
											<h5 class="test_name_header">                                        
												<div class="media-body">
													<h4 class="media-heading">
														<p><?php echo htmlspecialchars($PendingPaper["subject_name"]);?>
															<a id="<?php echo $PendingPaper["subject_id"]; ?>" class="btn waves-effect waves-light PendingPaperForEvaluation" style="background-color:#47A7F3; margin-left:90%" >GO</a>
														</p>
													</h4>
												</div>
								
											</h5>
										</li>
								<?php
									}
								}
								?>
                    </ul>
                 
            </div>
		</div>
	</div>
</div>
</main>