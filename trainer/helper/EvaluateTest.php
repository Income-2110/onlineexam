<style>
	.container-fluid{
		margin-left: inherit;
		margin-right: inherit;
	}
	#trainee_details
	{
		padding-left:4%;
		color:#81C784;
		font-size:18px;
	}
	.trainee_name,.psid
	{
		color:#26a69a;
		font-size:18px;
	}
	.SubjectiveQuestion
	{
		font-size:14px;
	}
	.scores{
		font-size:18px;
		
		color:#ee6e73;
	}
	.lscores
	{
		font-size:18px;
		color: #42A5F5;
	}
	.refreshscores{
		font-size:18px;
		
		color:#ee6e73;
	}
</style>		 

<main >
<div class="container-fluid">
	<div class="row">
		<div class="col l12 s12 z-depth-6 card-panel">
			<!--<div class="col s12 card-panel">-->
				<div class="row">
					<div class="col l12 s10" id="trainee_details" >
						<label  class="trainee_name"><br/>

							<?php
								$traineeId = $_SESSION['user_id']; #TODO use session variable
								$paper_id =  $_SESSION['paperid_PendingEvaluation']; #TODO from session
								$traineeId = $_SESSION['traineeid_PendingEvaluation'];
								$trainee_Name = dal_getTraineeName($traineeId);
								if(is_bool($trainee_Name)){
								if($trainee_Name == true)
								{
							?>
								<h4>There are no pending papers to be approved</h4>
							
							<?php				
								}
								elseif($trainee_Name == false)
								{
							?>
								<h4>Database problem</h4>
							<?php
								}}
								else
								{
									echo htmlspecialchars($trainee_Name);
								}
							?>
						</label><br/>
						<label class="psid">
							<?php
								echo htmlspecialchars($traineeId);
							?>
						</label>
						<!--<div class="blue-text text-darken-2 obj_text" style="font-size:14pt">
							Objective score is:
							<label for="ObjectiveScore" style="font-size:14pt" >
								<!--?php
									$assigned_mark = dal_getObjectiveScore($username, $paper_id);
									
									$max_mark = dal_getMaximumObjectiveScore($paper_id);
									echo $assigned_mark."/".$max_mark;
								?
							</label>
						</div>-->
					</div>
				</div>
			<!--</div>-->
        </div>
    </div>

    <div class="row">
        <div class="col l12 s12">
            <div class="panel-heading blue-text text-darken-2 ">
                <h4>Answer Sheet</h4>
                <?php
					$squestion_array = dal_getSubjectiveQuestionsId($paper_id);
				?>
            </div>
            <div class="card-panel">
				<?php 
					$no_of_questions = sizeof($squestion_array);
					$question_counter = 0;
					foreach($squestion_array as $squestion_id){
				?>
                <div class="panel-body">
                    <p for="SubjectiveQuestion">
                       <?php
							$question_id = $squestion_id['sq_id'];
							$question_content_array = dal_getSubjectiveQuestionContent($question_id); 
							$answer_content = dal_getSubjectiveAnswerContent($question_id, $traineeId,$paper_id);
							$marks_alloted = dal_getMaxMarksOfSubjectiveQuestion($question_id);
							$question_counter++;
							$subjective_mark_and_comment=dal_getSubjAssignedMarksAndComment($question_id,$traineeId,$paper_id);
							echo htmlspecialchars($question_counter."] ".$question_content_array[0]);
						?>
                    </p><br/>
                    <p style="font-weight:bold">Answer:
                    <label style="font-size:10pt;font-weight:200;color:black" for="SubjectiveAnswer">
                        <?php
                            echo htmlspecialchars($answer_content);
                        ?>
                    </label></p>
                     <p style="font-weight:bold">Marks: 
                    <input id="markAllotedEvaluate<?php echo $question_id ?>" name="mark<?php echo $question_id ?>" type="note-editable" style="width:20pt" value=<?php  echo $subjective_mark_and_comment['marks']  ?> ></input>&nbsp;
                    <input type="note-editable" class="maxmark" id="maxMarkEvaluate<?php echo $question_id ?>" value=<?php echo $marks_alloted;?> style="border-color:transparent; width:20pt; font-size:12pt; font-weight:bold; font-color:black" readonly >
                    </input> 
					<label id="errorMessageEvaluate<?php echo $question_id ?>" style="font-size:12pt; color:red;"></label></p>
                    

                    <div class="input-field col l5 s5">
                        <textarea id="commentForAnswerEvaluate<?php echo $question_id ?>" name="comment<?php echo $question_id ?>" class="materialize-textarea" placeholder="Add Comment" style="font-size: 14px;"  ><?php  echo htmlspecialchars($subjective_mark_and_comment['comment']);  ?></textarea>
                        <div class="row">
                            <div class="col l3 s3">
								<a id="<?php echo $question_id ?>" class="btn waves-effect waves-light btnSaveMarksComment" style="background-color:#47A7F3" type="submit" name="action">Done</a>
                            </div>
                        </div>
                    </div>
                </div>
				<?php
				 
					}
				?>
				
				<div class="btn waves-effect waves-light btnViewScores" style="background-color:#26a69a;margin-left:1%;width:100pt" type="submit" name="action">View Scores</div><br/>
				<?php
					$assigned_objmark = dal_getObjectiveScore($traineeId, $paper_id);
					$max_objmark = dal_getMaximumObjectiveScore($paper_id);
					$assigned_subjmark = dal_getTotalSubjectiveMarksScored($traineeId,$paper_id);
					$max_subjmark = dal_getTotalSubjectiveMarksOfAPaper($paper_id);
				?>
				
				  <label class="validate scores hidden">Objective score:&nbsp; </label><input id="obj_score" type="note-editable" class="validate hide lscores" style="border-color:transparent;width:20pt" value="<?php echo $assigned_objmark['scoredMarks']?>" readonly></input> <label id="mobj_score" class="validate hide lscores"> <?php echo "/".$max_objmark['marks'] ?></label><br/><br/>
	
				  <label class="validate scores hidden">Subjective score:&nbsp; </label><label  style="color: #42A5F5;" id="subj_score"  class="validate hide lscores" ><?php echo htmlspecialchars($assigned_subjmark);?></label><label class="validate hide  lscores"><?php echo htmlspecialchars("/".$max_subjmark); ?></label><br/><br/>
			
			
				 <label class="validate scores hidden">Total score:&nbsp; </label> <label id="total_score" class="validate hide lscores"><?php echo htmlspecialchars(($assigned_objmark['scoredMarks']+$assigned_subjmark));?></label><label class="validate hide lscores"><?php echo htmlspecialchars("/". ($max_objmark['marks']+$max_subjmark));?></label><br/><br/>
		  <!-- Subjective marks:
			//<label id="lblTotalMarks" style="width:60pt;font-size:12pt;font-weight:bold; height:60pt"></label>-->
				
				 
				 <div class="btn waves-effect hidden waves-light btnScoreRefresh" style="background-color:#26a69a;margin-left:1%;width:150pt" type="submit" name="action">Refresh Scores</div>

				
				<div class="btn waves-effect hidden waves-light btnFinish" style="background-color:#26a69a;margin-left:1%;width:100pt" type="submit" name="action">Finish</div><br/>
		
			</div>
			
		</div>
	</div>
</div>
</main>
