<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<main >
<div class="container">
	<div class="row" >
	<!--
		<div class="col l12">
			<ul class="tabs">
				<li class="tab col s3">
					<a class="active" href="#test1">Paper 1</a>
				</li>
				<li class="tab col s3">
					<a  href="#test2">Paper 2</a>
				</li>
			</ul>
		</div>
		-->
		
		<div id="test1" class="col l12">
			<div class="card-panel">
				<div class="row">
					<div class="col l12 s8">
						<div class="card-panel" id="testDetails_SetupQuestionPaper">
							<div class="row">
								<div class="panel-heading blue-text text-darken-2 "><h3>Test Details</h3></div>
								<div class="label-field col s6">
									<?php
										$test_id = $_SESSION["selected_test_id"];
										//$paper_id = 1;
										$testDetails = dal_getTestDetails($test_id);
										$testDetailsType = "Main Paper"; //TO Do
										$testDetailsTimeHrs = 0;
										$testDetailsTimeMins = 0;
										$batch_details = dal_getCurrentBatch($_SESSION["user_id"]);
										$batch_id = $batch_details[0]['batch_id'];
										/*
										if($testDetails[1]==1)
										{
											$testDetailsType = "Main Paper";
										}
										elseif($testDetails[1]==2)
										{
											$testDetailsType = "Backup Paper"; 
										} */
										if(($testDetails["time_duration"]%3600) ==0)
										{
											$testDetailsTimeHrs = $testDetails["time_duration"]/3600;
										}
										else
										{
											$testDetailsTimeHrs = $testDetails["time_duration"]/3600;
											$seconds = $testDetails["time_duration"]%3600;
											$testDetailsTimeMins = $seconds/60;
										}
										
									?>
									<label for="testSubject_SetupQuestionPaper"><h4>Subject: <?php echo htmlspecialchars($testDetails["subject_name"]); ?></h4></label>
								</div>
								<div class="label-field col s6">
									
									<label for="testPaperType_SetupQuestionPaper"><h4>Type: <?php echo htmlspecialchars($testDetailsType); ?></h4></label>
								</div>
								<div class="label-field col s6">
									
									<label for="testMarksTotal_SetupQuestionPaper"><h4>Marks: <?php echo htmlspecialchars(dal_getTotalMarksOfQuestionPaper($batch_id)); ?></h4></label>
								</div>
								<div class="label-field col s6">
									<!--<input placeholder="Time" id="testTimeTotal_SetupQuestionPaper" type="text" class="validate"/>-->
									<label for="testTimeTotal_SetupQuestionPaper"><h4>Time: <?php echo htmlspecialchars(round($testDetailsTimeHrs)); ?> hrs  <?php echo htmlspecialchars(round($testDetailsTimeMins)); ?> mins</h4></label>
								</div>
							</div>
							<!--<div class="row">
								<div class="input-field col s6">
									<input placeholder="Number Of Objective Questions" id="testmarks" type="text" class="validate"/>
									<label for="testmarks"></label>
								</div>
								<div class="input-field col s6">
									<input placeholder="Number Of Subjective Questions" id="testmarks" type="text" class="validate"/>
									<label for="testmarks"></label>
								</div>
							</div>-->
						</div>
						
						<?php
							$questionCounter = 10;
						?>
						
						<div class="card-panel" id="questionCard_SetupQuestionPaper">
							<!-- Question no -->						
							<div class="row">
								<input type="hidden" id="questionNumber_SetupQuestionPaper" value="<?php echo $questionCounter ?>">
								<!--<label id="questionNumber_SetupQuestionPaper" style="border-color:transparent; font-size:14pt; font-weight:bold; font-color:black">
									Question 1
								</label>--> 
							</div>
							
							<!-- Question content -->
							<div class="row">
								<div class="input-field col s12">
									<textarea id="questionContent_SetupQuestionPaper" class="materialize-textarea" style="font-size:14px"></textarea>
									<label style="font-size:8pt" for="questionContent_SetupQuestionPaper">Question:</label>
								</div>
							</div>
							
							<!-- Difficulty level and marks -->
						<div class="row">
								<div class="input-field col s6">

									

									

								
								
									<input placeholder="Marks" id="marksPerQuestion_SetupQuestionPaper" type="number" class="validate"/>
									<label for="testmarks"></label>&nbsp; &nbsp;
									
									
								</div>
								
								<div class="input-field col s6">
									<label style="font-size:8pt">Difficulty Level</label><br/>
									<div>
										<div class="row" >
											<?php
												$difficulty_Level = dal_getDifficultyLevel();
												foreach($difficulty_Level as $D_level)
												{
											?>
													<input class="radio-input_difficulty" type="radio" value="<?php echo $D_level["lid"] ?>" id="<?php echo $D_level["lid"]; ?>" name="radio-category_difficulty">
														<label class="radio-label_difficulty" for="<?php echo $D_level["lid"]; ?>">
															<?php 
																echo htmlspecialchars($D_level["level"]);
															?>
														</label>
											<?php
												}
											?>
											
										</div >
									</div>
									
								</div>
						</div>
							
							<!-- Question type -->
							<div class="row" >
								<input class="radio-input" type="radio" id="radioObjectiveQuestion_SetupQuestionPaper" name="radio-category" value="false" />
								<label class="radio-label" for="radioObjectiveQuestion_SetupQuestionPaper">Objective</label>
								<input class="radio-input" type="radio" id="radioSubjectiveQuestion_SetupQuestionPaper" name="radio-category" checked="checked" />
								<label class="radio-label" for="radioSubjectiveQuestion_SetupQuestionPaper">Subjective</label>
							</div >
							
							<!-- Objective Question details -->
							<div class="row validate hide options">
							<div class=" card-panel col s6">
							<div class="row">
								<div class="input-field col s8">
									<input placeholder="Option 1" id="optionOne_SetupQuestionPaper" type="text" class="validate"/>
									<label for="optionOne_SetupQuestionPaper"></label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s8">
									<input placeholder="Option 2" id="optionTwo_SetupQuestionPaper" type="text" checked="checked" class="validate"/>
									<label for="optionTwo_SetupQuestionPaper"></label>
								</div>
							</div>
							<div class="row">

								<div class="input-field col s8">
									<input placeholder="Option 3" id="optionThree_SetupQuestionPaper" type="text" class="validate"/>
									<label for="optionThree_SetupQuestionPaper"></label>
								</div>
							</div>
							<div class="row">

								<div class="input-field col s8">
									<input placeholder="Option 4" id="optionFour_SetupQuestionPaper" type="text" class="validate"/>
									<label for="optionFour_SetupQuestionPaper"></label>
								</div>
							</div>
							<!--<div class="row">
								<div class="input-field col s8">
									<input placeholder="Option 5" id="option5" type="text" class="validate"/>
									<label for="option5"></label>
								</div>
							</div>-->
							</div>
							<div  class="card-panel col s6">
							<p style="font-size:12pt" >Correct answer:</p><br/>
										<p><input type="checkbox" name="option" id="correctAnswerOne_SetupQuestionPaper" value="optionOne"></input><label style="color:black;font-size:12pt;" for="correctAnswerOne_SetupQuestionPaper">option 1</label></p>
										<p><input type="checkbox" name="option" id="correctAnswerTwo_SetupQuestionPaper" value="optionTwo"/><label style="color:black;font-size:12pt;" for="correctAnswerTwo_SetupQuestionPaper">option 2</label></p>
										<p><input type="checkbox" name="option" id="correctAnswerThree_SetupQuestionPaper" value="optionThree"/><label style="color:black;font-size:12pt;" for="correctAnswerThree_SetupQuestionPaper"/>option 3<label/></p>
										<p><input type="checkbox" name="option" id="correctAnswerFour_SetupQuestionPaper" value="optionFour"/><label style="color:black;font-size:12pt;" for="correctAnswerFour_SetupQuestionPaper"/>option 4<label/></p>
									</div>
							</div>
							
							<!-- Buttons -->
							<div class="row">
								<div id="btnSaveQuestion_SetupQuestionPaper" class="waves-effect waves-light btn save-btn ">Save</div>
								<div id="btnClearQuestion_SetupQuestionPaper" class="waves-effect waves-light btn clearbtn ">clear</div>
								<div id="btnDeleteQuestion_SetupQuestionPaper" class="waves-effect waves-light btn clearbtn ">Load Question Paper</div>
							</div>
							
							<div class="row">
								<label style="font-size:14pt; font-weight:bold; color:Red" id="errorMessage_SetupQuestionPaper"> </label>
							</div>
						</div>
						
						<div class="card-panel" id="questionPaperCard_SetupQuestionPaper">
							<div class="row">
								<div class="col l12 s8">
									<div class="panel-body">
										<div class="panel-heading blue-text text-darken-2 text-center"><h3>Questions Paper</h3></div>
										<div id="questionPaper_SetupQuestionPaper">
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>

						<div class="waves-effect waves-light btn save-btn btnSavePaper_SetupQuestionPaper">Save Paper</div>
						</div>
					</div>
				</div>

	</div>
</div>
</main>				
<script>
$(document).ready(function() {
	$('select').material_select();
});
</script>