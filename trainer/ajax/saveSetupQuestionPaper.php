<?php
	require_once("../helper/dalFunctionsQ.php"); 
	$data = array();
	 
	  
	ob_start();
    include('questionPaper.php');
    $myStr = ob_get_contents();
    ob_end_clean();
	 $data['questionPaper'] = $myStr;
	
	//echo "Done";
	
	$qdetails = $_POST['question'];
	$qarray = (array) json_decode($qdetails,true);
	$question = $qarray['questionContent'];
	$marks = $qarray['marksAlloted'];
	$difficulty_level = $qarray['difficultyLevelId'];
	$result = dal_setSubjectiveQuestions($question,$marks,$difficulty_level);
	if($result)
		$data['response'] = "Done";
	 $json_data = json_encode($data);
	 echo $json_data;
	
?>