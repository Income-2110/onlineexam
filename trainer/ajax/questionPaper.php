<?php
require_once("../helper/dalFunctionsQ.php");
?>
	
<label class="blue-text text-darken-1" style="font-size:12pt;">Subjective Questions:</label>

<?php 
	//session_start();
	$paper_id = 11;//$_SESSION['selected_paper_id'];
	//echo $paper_id;
	$squestionid_array = dal_getSubjectiveQuestionsId($paper_id);

	$i = 1;
	
	foreach($squestionid_array as $row)
	foreach($row as $questionID)
	{
?>
		<div class = "row"> 
			<div class = "col l0 m0 s0">
				<label id="QuestionNo" style="font-size:12pt;">Q <?php echo "$i"?>. </label>
			</div>
			<?php
				$question_array = dal_getSubjectiveQuestionContent($questionID);
			?>		
			<div class = "col l7 m7 s7">					
				<label id="QuestionContent" style="font-size:12pt;"> <?php echo htmlspecialchars($question_array[0]);?></label>
			</div>
			<div class = "col l1 m1 s1">
				<label id="Marks" style="font-size:12pt;color:#2196F3;"><?php echo htmlspecialchars($question_array[1]);?></label>
			</div>
			<!-- Use index 2 for getting difficulty ID -->
			<div class = "col l1 m1 s1">
				<label id="DifficultyLevelName" style="font-size:12pt;color:#2196F3;"><?php echo htmlspecialchars($question_array[3]);?></label>
			</div>
			<div class = "col l2 m2 s2">
			<!--<div class="waves-effect waves-light btn Edit_SubjQuestionPaper" id="<?php echo $questionID?>" >Edit</div>&nbsp;<div class="waves-effect waves-light btn Delete_SetupQuestionPaper" id="<?php echo $questionID?>">Delete</div>-->
			</div>
		<?php
			$i++;
		?>
		</div>
<?php
	}
?>
	

<label class="blue-text text-darken-1" style="font-size:12pt;">Objective Questions:</label>
<div class = "row"> 
	
	<?php 
		//$paper_id = 9;
		$oquestionid_array = dal_getObjectiveQuestionId($paper_id);
		$i = 1;
		foreach($oquestionid_array as $row)
		foreach($row as $oquestionID)
		{
			$oquestion_array = dal_getObjectiveQuestionContent($oquestionID);	
			?>	
			
			<div class="row">	
			<div class="col l9 s9">
			<label id="QuestionContent" style="font-size:12pt;">Q <?php echo "$i"?>.  <?php echo htmlspecialchars($oquestion_array[0]);?></label></div>
			<div class="col l2 s2">
			<label style="font-size:12pt;font-weight:bold;color:#2196F3;"><?php echo htmlspecialchars($oquestion_array[3]);?> </label></div>
			<div class="col l1 s1">
			<label style="font-size:12pt;color:#2196F3;"><?php echo htmlspecialchars($oquestion_array[1]);?> </label></div></div>
			<div class="row" style="margin-left:10pt"><label style="font-size:12pt;font-weight:bold">Options: </label><p style="margin-left:20pt">
			<label id="option1" style="font-size:12pt;">1. <?php echo htmlspecialchars($oquestion_array[4]);?></label><br/>
			<label id="option1" style="font-size:12pt;">2. <?php echo htmlspecialchars($oquestion_array[5]);?></label><br/>
			<label id="option1" style="font-size:12pt;">3. <?php echo htmlspecialchars($oquestion_array[6]);?></label></br>
			<label id="option1" style="font-size:12pt;">4. <?php echo htmlspecialchars($oquestion_array[7]);?></label></br></p>
			<label style="font-size:12pt;">Correct Answer:</label>&nbsp;<label id="option1" style="font-size:12pt;color:#26a69a;"> <?php echo htmlspecialchars($oquestion_array[8]);?></label></div>
			<!--<div class="waves-effect waves-light btn Edit_ObjQuestionPaper">Edit</div>&nbsp;<div class="waves-effect waves-light btn Delete_ObjQuestionPaper">Delete</div><br/><br/>-->
			
			
	<?php
			$i++;
		}
	?>
	</div>

	

		
	

	





