<?php
	require_once("../helper/dalFunctionsQ.php");
	
	session_start();
	$trainee_id = $_SESSION['traineeid_PendingEvaluation'];
    $paper_id = $_SESSION['paperid_PendingEvaluation'];
	
	$mark = $_POST['mark'];
	$comment = $_POST['comment'];
	$max_mark = $_POST['max_mark'];
	$sq_id = $_POST['sq_id'];
	
	//Check if mark is within range
	if($mark < 0){
		die();
	}
	if($mark > $max_mark){
		die();
	}
	
	$res = dal_setMarkAndComment($trainee_id,$sq_id,$paper_id,$mark,$comment);
	
	$data = array();
	$data['status'] = "Done";
	$data['response'] = $res;	
	
	$json_data = json_encode($data);
    echo $json_data;
?>