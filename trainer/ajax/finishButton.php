<?php
	require_once("../helper/dalFunctionsQ.php");
	
	session_start();
	$trainee_id = $_SESSION['traineeid_PendingEvaluation'];
	$paper_id = $_SESSION['paperid_PendingEvaluation'];

	$data = array();
	$data['status'] = "Done";
	$data['response'] = true;	

	//Save the sub score
	$res = dal_setScoredMarksSubj($trainee_id,$paper_id);
	if($res == false)
		$data['response'] = false;
	
	//save total score
	$res = dal_setScoredTotalMarks($trainee_id,$paper_id);
	if($res == false)
		$data['response'] = false;
	
	$json_data = json_encode($data);
    echo $json_data;
?>