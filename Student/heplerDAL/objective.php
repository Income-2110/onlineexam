<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect1()
{
  $conx = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
    
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $conx;
}

	/* Retrive the ovbjective questions
	paper ID is the input here
	Returns the list of all objective questions as an array
	*/
	function dal_retrieveObjQuestion($testId)
	{
		//take main_paper from this particular testId
	    // connect to database, and get the connection handle
		$con = dbConnect();	
		// forming a query
		$select_query = "SELECT main_paper FROM ".DB_NAME.".master_test where test_id = ".$testId; // "." is for concatenation
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$main_paper = $result_set["main_paper"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
			//fetch all the questionId s from "test_question_obj"
		     //we got our paperId from above querry, now fucking fetch the question id using below function
			 $questionId = array();
			 if(dal_retrieveQuestionId($main_paper)==false)
			 {
				 return false;
			 }
			 else
			 {
				$questionId = dal_retrieveQuestionId($main_paper);
			 }			 
			//using above questionId fetch question from question_obj
			$NoOfQues = count($questionId);
			$question = array();
			
        foreach($questionId as $x)
        {
           
            array_push($question,dal_retrieveQuestion($x));
          //  $question= dal_retrieveQuestion($x);
          
        }

		return $question;
		
	}
	function dal_retrieveQuestionId($paperId)
	{
        $con = dbConnect();
		//It returns all the questionID which belongs to the paper with paperId = paperId
		$select_query = "SELECT question_id FROM ".DB_NAME.".test_question_obj where paper_id = ".$paperId; // "." is for concatenation
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
			
			
				$i=0;
				$question = array();
				while($result_set = mysqli_fetch_array($result))
				{           
					$question[$i] = $result_set["question_id"];			
					$i++;	
				}		
                return $question;
			}
            else
		{
		  // No rows present, which match the select query
		  return false;
		}
			
		}
	
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	}
	function dal_retrieveQuestion($QuestionId)
	{
        $con = dbConnect();
		//It returns all the questionID which belongs to the paper with paperId = paperId
		$select_query = "SELECT * FROM ".DB_NAME.".questions_obj where question_id = ".$QuestionId; // "." is for concatenation
           
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{		
				$question_list = array();
				$count=0;
				while($result_set = mysqli_fetch_array($result))
				{
					$question = array();
				   // print_r($result_set)."<br>";
					$question["id"] = $result_set["question_id"];
					$question["text"] = $result_set["question"];
					$question["op1"] = $result_set["op_1"];
					$question["op2"] = $result_set["op_2"];
					$question["op3"] = $result_set["op_3"];
					$question["op4"] = $result_set["op_4"];
                    $question["marks"] = $result_set["marks"];
			         
					// adding the admin_details ,to the admin array.		
					$question_list[$count] = $question;					
                     $count++;
                    
                    
			   }
			return $question_list;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}
