<?php

  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect1()
{
  $conx = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
    
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $conx;
}

function AnswerObjective($answer_id,$q_id,$inputAnswer)
{
	sort($inputAnswer);
   
    $con = dbConnect1();
    $sortedAnswer= implode(",", $inputAnswer);
    
    $get_answer = "SELECT answers,marks FROM ".DB_NAME.".questions_obj WHERE question_id=$q_id";
    //echo $get_answer;
    $result = mysqli_query($con, $get_answer);
    $result_set = mysqli_fetch_array($result);
    
    $answers = $result_set['answers'];
    $marks = $result_set['marks'];
    
    if($answers != $sortedAnswer)
    {
         $marks = 0;
    }
    

   
	if(dal_saveObjectiveAnswer($answer_id,$q_id,$sortedAnswer,$marks))
	{
		return true;
	}
	else
	{
		return false;
	}
}
	
function dal_saveObjectiveAnswer($answer_id,$oq_id,$answers,$marks)
{
    $con = dbConnect1();
	$sql2="UPDATE answer_detail_obj SET `answers`='$answers', marks='$marks' WHERE oq_id=$oq_id and answer_id=$answer_id";
	   
	if(mysqli_query($con,$sql2))
	{			
		return true;
	}
	else
	{
		return false;
	}
}
		
?>