<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}


function failSafe($traineeId,$testId)
{
    //retrievePaperID
    $paper_id = retrievePaperId($traineeId,$testId);
   
    //retrieve answer_id 
    $answer_id = retrieveAnswerId($traineeId,$paper_id);
   
    //using above answer_id retrieve all the answered questions with all the fields
    $answerArray = array();
    
    $answerArray = retieveAllAnswers($answer_id);
    
    return $answerArray;
       
}


//we have traineeId and TestId, we will determine paperId using these from result table

function retrievePaperId($traineeId,$testId)
{
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT paper_id FROM ".DB_NAME.".result where test_id = ".$testId ." AND trainee_id=".$traineeId; // "." is for                   concatenation

		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$paper_id = $result_set["paper_id"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
        return $paper_id;
}


//we will retrieve answer_id from answer_obj table 
function retrieveAnswerId($traineeId,$paperId)
{
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT answer_id FROM ".DB_NAME.".answers_obj where paper_id = ".$paperId ." AND trainee_id=".$traineeId; // "." is for                   concatenation
		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$answer_id = $result_set["answer_id"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
        return $answer_id;
}

function retieveAllAnswers($answer_id)
{
        $con = dbConnect();
		
		$select_query = "SELECT * FROM ".DB_NAME.".answer_detail_obj where answer_id = ".$answer_id; // "." is for concatenation
           
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{		
				$answerAray = array();
				$count=0;
				while($result_set = mysqli_fetch_array($result))
				{
					$answer = array();
				   // print_r($result_set)."<br>";
					$answer["answer_id"] = $result_set["answer_id"];
					$answer["oq_id"] = $result_set["oq_id"];
					$answer["answers"] = $result_set["answers"];
					$answer["marks"] = $result_set["marks"];
					$answer["marked_review"] = $result_set["marked_review"];
                    
					//adding answer to answerArray i.e. array of array 		
					$answerArray[$count] = $answer;					
                    $count++;
                    
                    
			   }
			return $answerArray;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}







