<?php
/*
    This file will contain all the helper functions required.
  */
//database name 
define("DB_NAME","onlineexam");
//database password
define("DB_PASS","");
//server name
define("DB_SERVER","localhost");
//username
define("DB_USERNAME","root");

// require_once("helper/config.php");
date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
    $conx = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);

    if(mysqli_connect_errno())
    {
        die("Could not connect to Database"); 
    }
    return $conx;
}
/* function to retrieve the test marks of the trainees*/




function get_studentTestMarks()
{
    // connect to database, and get the connection handle
    $con = dbConnect();

    //$traineeid = $_SESSION['user_id'];
    $trainee_id=101;
    // forming a query
    $select_query = "SELECT test_id,total_marks FROM ".DB_NAME.".result where trainee_id=".$trainee_id; // "." is for concatenation

    $result = mysqli_query($con,$select_query);

    if($result)
    {
        /* If query is successful.
	     Query is successful if it gets executed on the database server without any errors.
		 If no rows are present for the select query, then also it is successful, therefore
		 if we're expecting any results from a select query always check for number of rows
		 present, as shown below
	  */
        if(mysqli_num_rows($result) > 0)
        {

            $all_testlst = array(); // creating an batch array
            $i=0;
			

            while($result_set = mysqli_fetch_array($result))
            {
				$temp_array = array() ;
                $temp_array["id"]=$result_set['test_id'];
                $temp_array["marks"]=$result_set['total_marks'];    
                $all_testlst[$i]=$temp_array;
				$i++;
            }

            return $all_testlst;
        }
        else
        {
            // No rows present, which match the select query
            return false;
        }
    }
    else
    {
        /* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
        return false;
    }

}
  
?>
