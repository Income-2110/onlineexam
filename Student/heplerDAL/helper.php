<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}

	/*To retrive the test which are not taken by user 
	userid is input 
	returns the list of tests that are not yet taken by user
	*/
	function dal_getAllNotTakenTests($userId)
	{
		$traineename = array("Myrna", "Shruthi", "Reena");
		return $traineename; 
	}


	/* Retrive the ovbjective questions
	paper ID is the input here
	Returns the list of all objective questions as an array
	*/
	function dal_retrieveObjQuestion($testId)
	{
		//take main_paper from this particular testId
	    // connect to database, and get the connection handle
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT main_paper FROM ".DB_NAME.".master_test where test_id = ".$testId; // "." is for concatenation
		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$main_paper = $result_set["main_paper"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
			//fetch all the questionId s from "test_question_obj"
		     //we got our paperId from above querry, now fucking fetch the question id using below function
			 $questionId = array();
			 if(dal_retrieveQuestionId($main_paper)==false)
			 {
				 return false;
			 }
			 else
			 {
				$questionId = dal_retrieveQuestionId($main_paper);
			 }			 
			//using above questionId fetch question from question_obj
			$NoOfQues = count($questionId);
			$question = array();
			$y=0;
			for ($x = $NoOfQues; $x >= 0; $x--)
			{
				//the function which returns array with question and options
				$question = dal_retrieveQuestion($QuestionId[$y]);
				$y++;
			}
		
		
	}
	
	function dal_retrieveQuestion($QuestionId)
	{
		//It returns all the questionID which belongs to the paper with paperId = paperId
		$select_query = "SELECT * FROM ".DB_NAME.".question_obj where question_id = ".QuestionId; // "." is for concatenation
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{		
				$question_list = array();
				$i=0;
				while($result_set = mysqli_fetch_array($result))
				{
					$question = array();
				
					$question["id"] = $result_set["question_id"];
					$question["text"] = $result_set["question"];
					$question["op1"] = $result_set["op_1"];
					$question["op2"] = $result_set["op_2"];
					$question["op3"] = $result_set["op_3"];
					$question["op4"] = $result_set["op_4"];
			
					// adding the admin_details ,to the admin array.		
					$question_list[$i] = $question;
					$i++;
				}
				return $question_list;
			}			
			
		
			else
			{
				// No rows present, which match the select query
				return false;
			}
		}
		else
		{
			/* If the query fails. 
			Query fails if the query syntax is wrong or the connection is closed due to some
			network error		   
			*/
			return false;
		}	
	}
	


	
	/* To get subjective questions 
	Takes Paper Id as Input
	Returns the list of Question IDs(for that particular paper Id) as an array 
	*/
function dal_retrieveObjQuestion($testId)
	{
		//take main_paper from this particular testId
	    // connect to database, and get the connection handle
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT main_paper FROM ".DB_NAME.".master_test where test_id = ".testId; // "." is for concatenation
		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$main_paper = $result_set["main_paper"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
			//fetch all the questionId s from "test_question_obj"
		     //we got our paperId from above querry, now fucking fetch the question id using below function
			 $questionId = array();
			 if(dal_retrieveQuestionId($main_paper)==false)
			 {
				 return false;
			 }
			 else
			 {
				$questionId = dal_retrieveQuestionId($main_paper);
			 }			 
			//using above questionId fetch question from question_obj
			$NoOfQues = count($questionId);
			$question = array();
			$y=0;
			for ($x = $NoOfQues; $x >= 0; $x--)
			{
				//the function which returns array with question and options
				question = dal_retrieveQuestion($QuestionId[$y]);
				$y++;
			}
		
	}
	}


	
?>