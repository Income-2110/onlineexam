<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}


//this function rturns the comments
//it returns the comments array comment[0] = actual comment && comment[1] = date of comment;
//it returns string"No Commnts" if no comments are provided or comment visibility is 0
//returns false in querry failure
function retrieveComments($traineeId)
{
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT admin_comment,comment_date FROM ".DB_NAME.".trainee where comment_visibility = 1  AND trainee_id=".$traineeId; // "." is for                   concatenation

		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
            $comment = array();
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$comment["admin_comment"] = $result_set["admin_comment"];
                $comment["comment_date"] = $result_set["comment_date"];
			}
			else
			{
                
				return "No Comments"; 
			}
		}
		else
		{
			return false;
		}
        return $comment;
}

function retrieveRanking($testId)
{
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT trainee_id,total_marks FROM ".DB_NAME.".result where test_id=".$testId." order by total_marks"; // "." is for                   concatenation

		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
            $rank = array();
            $count = 0;
			if(mysqli_num_rows($result) > 0)
			{
	          	while($result_set = mysqli_fetch_array($result))
				{
					$answer = array();
				   // print_r($result_set)."<br>";
					$answer["trainee_id"] = $result_set["trainee_id"];
					$answer["total_marks"] = $result_set["total_marks"];
					
                    
					//adding answer to answerArray i.e. array of array 		
					$rank[$count] = $answer;					
                    $count++;
                    
                    
			   }
			}
			else
			{
                
				return false; 
			}
		}
		else
		{
			return false;
		}
        UltimateRankList($rank);
}
        //above rank array contains trainee id and marks
    // code below retieves trainee name, pofile pic and marks;
function UltimateRankList($rank)
{
    
    $con = dbConnect();
    
    $count1 = sizeof($rank);
    $i=0;
    while($i<$count1)
    {
        $select_query = "SELECT name,profile_pic FROM ".DB_NAME.".trainee where trainee_id=".$rank[$i]["trainee_id"];
        
        $result = mysqli_query($con,$select_query);
        $UltimateRank = array();
        
        if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
                $temp = array();
				$temp["name"] = $result_set["name"];
                $temp["profile_pic"] = $result_set["profile_pic"];
                $temp["total_marks"] = rank[$i]["total_marks"];
                
                $UltimateRank[$i] = $temp;					
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
        $i++;
    }
    return $UltimateRank;
}
