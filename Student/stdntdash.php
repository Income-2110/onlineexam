<?php
     require_once("helper/misscell.php");
        if(!is_logged_in())
        {
            header("Location:login.php");
            
        }
?>


<html>
    
    
    <head>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
              <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css"  media="screen,projection"/>
        <link rel="stylesheet" type="text/css" href="assets/css/StudentDashboard.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
        <style>
        html,body
            {
                
                width:100%;
                height: 100%;
            }
        </style>
    </head>  
    
    
    
    <body>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="assets/js/materialize.min.js"></script>
        <script  src='assets/Chart.js-master/Chart.js'></script>
        <script  src='assets/js/StudentDashboard.js'></script>
       <div class="row">
                <div class="page-section half bg-white pagesection">  
                    <div class="collapse navbar-collapse nav-main" id="main-nav">
                        <ul class="nav navbar-nav">
                            <li><img src="assets\img\logo.jpg"/></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-nav-bordered navbar-right">
                            <a class='dropdown-button btn drop drop-btn' href='#' data-activates='dropdown1'>
                                <img src="assets\img\guy-5.jpg" alt="Bill" class="img-circle" width="40"> Bill <span class="caret"></span>
                            </a>
                            <ul id='dropdown1' class='dropdown-content'>
                                <li><a href="#!">Logout</a></li>                           
                            </ul>
                        </ul> 
                    </div>               
                </div>
            </div>
        <div class="row" style="height:100%" >
           
                    <div class="col l2" style="height:100%">
              <div class="profile-sidebar">
				
                                    <div class="profile-userpic">
                                        <img src="assets\img\guy-5.jpg" class="img-responsive" alt="">
                                    </div>

                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name">
<!--                                            <h4>John Doe</h4>-->
                                            <h4><?php echo $_SESSION['name'] ?> </h4>
                                            
                                        </div>
                                        <div class="profile-usertitle-job">
                                            IT Consultant
                                        </div>
                                    </div>

                                    <div class="profile-usermenu">
                                        <ul class="nav">
                                            <li class="active">
                                                <a href="#">
                                                <i class="glyphicon glyphicon-home"></i>
                                                Overview </a>
                                            </li>
                                            <li>
                                                <a href="AccountSettings.php">
                                                <i class="glyphicon glyphicon-user"></i>
                                                Account Settings </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                <i class="glyphicon glyphicon-flag"></i>
                                                Help </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            </div>
             <div class="container">
            <div class="col l10" style="height:95%;">
            <div class="row" style="height:80%;  ">
                
<!--                test panel-->
                <div class="col l5"  style="height:auto;" >
                        <div class="panel panel-default testpanel" style="width:100%">
                            <div class="panel-heading score-header">Test Scores</div>
                            <!-- List group -->
                            <ul class="list-group">
                                <li class="list-group-item score_list"><h5 class="test_name_header">Cras justo odio</h5>
                                    <div class="score-display">11</div>
                                </li>
                                <li class="list-group-item score_list">Cras justo odio
                                    <p class="score-display">25</p>
                                </li>
                            </ul>
                        </div> 
                    </div>
                
               <!--                test panel--> 
                
                <!--                chart panel-->
                         <div class="col l5" style="">
                                <div class="card-panel graphpanel">
                                    
                                            <canvas id="buyers" class="testresult_graph"></canvas>
                                      
                                </div>
                            </div>
                
                
                <!--                chart panel-->
                
                                <!--                Taket panel-->
                   <div class="col l2" style="">
                                <div class="card-panel  taketest">
                                    <h2 class="white-text"><a href="ViewTests.php">Take Test</a></h2>
                                </div>
                            </div>
                                <!--                Taket panel-->
                
                
                </div>
                
<!--
               New ROW
-->
                                      <div class="row" style="height:50%;  ">
                                            <div class="col l6" style=" ">
                        <div class="panel panel-default classpanel">
                            <div class="panel-heading Current-rank-header"> 
                                <h2 class="panel-title Current-rank-title">Class Ranking</h2>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-3.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-6.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>         
                                                              <div class="col l6 commentcol" style=" ">
                        <div class="panel panel-default commentpanel">
                            <!-- Default panel contents -->
                            <div class="panel-heading"><h4 class="comment">Comments</h4></div>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-3.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                            <div class="text-light">
                                                Topic: <span class ="test_topic">AngularJS </span> &nbsp; 
                                                By: <span class ="comment_adminname">Adrian Demian</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-6.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                            <div class="text-light">
                                                Topic: <span class ="test_topic">AngularJS </span> &nbsp; 
                                                By: <span class ="comment_adminname">Adrian Demian</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
             
            </div>

        <script>
        var buyers = document.getElementById('buyers').getContext('2d');
        var myChart=new Chart(buyers).Line(buyerData);
        myChart.addData([17], "Test 5");
        </script>
        
    </body>
</html>