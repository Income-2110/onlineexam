<?php
   session_start();
 
   $_SESSION=array(); 
   //frees all the session variables currently registered
   session_unset();
   session_destroy();
   header("Location: login.php");
?>