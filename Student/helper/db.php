 <?php
define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect1()
{
  $conx = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
    
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $conx;
}

?>