<?php
  /*
     This file will contain all the helper functions required.
  */
  
    //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
 // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}

/* Traniee Login Verifier */

function trainee_login($username,$password)
{
    
	// connect to database, and get the connection handle
	$con = dbConnect();
	 // scrubbing user input
	 $username = mysqli_real_escape_string($con,$username);
	 $password = mysqli_real_escape_string($con,$password);
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".trainee WHERE trainee_id ='$username' AND passwd='$password' "; 
  //  echo $select_query;
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
		if(mysqli_num_rows($result) == 1)
		{
               // Correct details, login
               $result_set = mysqli_fetch_array($result);
			   $user_id = $result_set["trainee_id"];
			   $name= $result_set["name"];
			   $profile_pic= $result_set["profile_pic"];
			   $status=$result_set["active"];
			   
			   
			   session_start();
			   $_SESSION['user_id'] = $user_id;
			   $_SESSION['name'] = $name;
			   $_SESSION['profile_pic'] = $profile_pic;
			   $_SESSION['status'] = $status;
			   $_SESSION['type'] = "Trainee";
			   
			   return true;
	    }
		else
		{
			return false;
		}
	
  }
   else
	{
		return false;
	}
}

/* change accounts settings like username and password*/
//function change_settings($newpassword)
//{
//    	$con = dbConnect();
//        $password = mysqli_real_escape_string($con,$newpassword);
//        $updatequerry="Update ".DB_NAME.".trainee set passwd='newpassword' where trainee_id='" $_SESSION['user_id']"'";
//        if (mysqli_query($conn, $updatequerry)) 
//        {
//            echo "Record updated successfully";
//        } else 
//        {
//            echo "Error updating record: " . mysqli_error($conn);
//        }
//    
//}
    




?>