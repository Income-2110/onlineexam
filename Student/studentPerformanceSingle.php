<?php
 require_once("heplerDAL/chart.php");
?>
<html>
<head>
<meta charset="utf-8" />
<title>Chart.js demo</title>
    <script src="assets/js/jquery-2.1.4.min.js"> </script>
    <script  src='assets/Chart.js-master/Chart.js'></script>
   <script>
        var marksData = {
            labels: [],
            datasets : [
                {
                    fillColor: "rgba(255, 85, 0,0.4)",
                    strokeColor: "#ACC26D",
                    pointColor: "#fff",
                    pointStrokeColor: "#9DB86D",
                    data: [],
                    showScale: true,
                    scaleShowLabels: true,
                    scaleBeginAtZero: true,
					showTooltips: true,
					animationEasing: "easeOutQuart",													responsive: true,
					maintainAspectRatio:true,
                }   ]
        };
		</script>
		<script>
		var marks_array=[];
		function getMarks()
		{
			<?php
			$lst=get_studentTestMarks();
			foreach($lst as $element)
			{
				echo "marks_array.push('".$element["marks"]."');";
				
			}			
			?>
			var x;
			for (x in marks_array)
			{
				//alert(marks_array[x]);
				var y=parseInt(x)+1;
				var pointx=marks_array[x];
				myChart.addData([pointx],'Test '+y);
				
			}	
		}
		
		</script>

    </head>
<body>
<h2><center>Charul Mittal (21267)</center></h2>
<canvas id="marksGraph" width="900" height="400"></canvas> 
    <script>
    var marksId = document.getElementById('marksGraph').getContext('2d');
	var myChart = new Chart(marksId).Line(marksData); 
        $(window).load(function(){
			getMarks();
			});
		   
    </script>
    </body>
</html>