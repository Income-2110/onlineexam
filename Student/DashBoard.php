<?php
     require_once("helper/misscell.php");
        if(!is_logged_in())
        {
            header("Location:login.php");
            
        }
?>
<?php 

require_once "helper/SuperImport.php";

?>


    <main >
    <div class="container" style="width:100%" >
     
          <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
             <div class="panel panel-default testpanel" style="width:100%">
                            <div class="panel-heading score-header">Test Scores</div>
                            <!-- List group -->
                            <ul class="list-group">
                                <li class="list-group-item score_list"><h5 class="test_name_header">Cras justo odio</h5>
                                    <div class="score-display">11</div>
                                </li>
                                <li class="list-group-item score_list">Cras justo odio
                                    <p class="score-display">25</p>
                                </li>
                            </ul>
                        </div> 
              </div>
              <div class="col s12 m12 l6">
                  <div class="card-panel cyan darken-1 graphpanel">
                                    <div class="container-fluid">  <canvas id="buyers" class="testresult_graph"></canvas></div>
                                          
                                      
                                </div>
              </div>
           
              </div>
        
                    <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
            
                        <div class="panel panel-default classpanel">
                            <div class="panel-heading Current-rank-header"> 
                                <h2 class="panel-title Current-rank-title">Class Ranking</h2>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-3.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                                <li class="list-group-item rank_list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-6.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Myrna</h4>
                                            <div class="rank-display">#1</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                            </ul>
                        </div>
            
              </div>
              <div class="col s12 m12 l6">
                  
                        <div class="panel panel-default commentpanel">
                            <!-- Default panel contents -->
                            <div class="panel-heading"><h4 class="comment">Comments</h4></div>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-3.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                            <div class="text-light">
                                                Topic: <span class ="test_topic">AngularJS </span> &nbsp; 
                                                By: <span class ="comment_adminname">Adrian Demian</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object profile-pic" src="assets\img\woman-6.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                            <div class="text-light">
                                                Topic: <span class ="test_topic">AngularJS </span> &nbsp; 
                                                By: <span class ="comment_adminname">Adrian Demian</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
              </div>
        
              </div>
        
        
              </div>
              
        </div>
      
              
        </div>
 
        </main>
  <script>
        var buyers = document.getElementById('buyers').getContext('2d');
        var myChart=new Chart(buyers).Line(buyerData,{responsive: true,maintainAspectRatio: true,});
        </script>
        <script>
    $(".button-collapse").sideNav();
  // Show sideNav
  $('.button-collapse').sideNav('show');
  // Hide sideNav
  $('.button-collapse').sideNav('hide');
        
    </script>
    
    
    
</body>
</html>

