-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 07:14 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineexam`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_obj`
--

CREATE TABLE IF NOT EXISTS `answers_obj` (
  `answer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_obj` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_obj`
--

INSERT INTO `answers_obj` (`answer_id`, `trainee_id`, `paper_id`, `marks_obj`) VALUES
(1, 101, 1, '0'),
(2, 102, 2, '0'),
(3, 103, 3, '0'),
(4, 104, 4, '0'),
(5, 102, 3, '0'),
(6, 104, 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `answers_subj`
--

CREATE TABLE IF NOT EXISTS `answers_subj` (
  `sanswer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_subj` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_subj`
--

INSERT INTO `answers_subj` (`sanswer_id`, `trainee_id`, `paper_id`, `marks_subj`) VALUES
(1, 102, 4, '0'),
(2, 101, 3, '0'),
(3, 103, 4, '0'),
(4, 101, 3, '0'),
(5, 101, 1, '0'),
(6, 102, 2, '0'),
(7, 103, 3, '0'),
(8, 101, 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_obj`
--

CREATE TABLE IF NOT EXISTS `answer_detail_obj` (
  `answer_id` int(11) NOT NULL,
  `oq_id` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of answers marked',
  `marks` int(11) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_obj`
--

INSERT INTO `answer_detail_obj` (`answer_id`, `oq_id`, `answers`, `marks`, `marked_review`) VALUES
(1, 2, '1', 2, 1),
(2, 3, '1', 2, 0),
(4, 3, '1', 2, 1),
(3, 2, '1', 2, 0),
(6, 2, '1', 2, 0),
(4, 2, '1', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_subj`
--

CREATE TABLE IF NOT EXISTS `answer_detail_subj` (
  `sanswer_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `marks` decimal(10,0) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_subj`
--

INSERT INTO `answer_detail_subj` (`sanswer_id`, `sq_id`, `answer`, `marks`, `comments`, `marked_review`) VALUES
(1, 7777, 'egewg ', '4', 'good', 1),
(2, 8888, 'ddghh', '4', 'very good', 0),
(3, 8888, 'dffhjh', '4', 'great', 0),
(4, 9999, 'deftrht', '4', 'fine', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batch_info`
--

CREATE TABLE IF NOT EXISTS `batch_info` (
  `batch_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL COMMENT 'trainer teaching this batch',
  `batch_name` varchar(50) NOT NULL COMMENT 'friendly name for batch',
  `batch_date` date NOT NULL COMMENT 'batch start date',
  `max_marks` int(11) NOT NULL COMMENT 'max marks paper for batch',
  `active` tinyint(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_info`
--

INSERT INTO `batch_info` (`batch_id`, `trainer_id`, `batch_name`, `batch_date`, `max_marks`, `active`) VALUES
(1, 1, 'dotNET', '2015-10-12', 25, 0),
(2, 2, 'testing', '2015-10-12', 50, 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_trainee`
--

CREATE TABLE IF NOT EXISTS `batch_trainee` (
  `batch_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `barred` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains mapping of trainees to particular batch';

--
-- Dumping data for table `batch_trainee`
--

INSERT INTO `batch_trainee` (`batch_id`, `trainee_id`, `barred`) VALUES
(1, 101, 1),
(2, 102, 0),
(2, 103, 0),
(1, 104, 1);

-- --------------------------------------------------------

--
-- Table structure for table `difficulty_levels`
--

CREATE TABLE IF NOT EXISTS `difficulty_levels` (
  `dlevel_id` int(11) NOT NULL,
  `level_name` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficulty_levels`
--

INSERT INTO `difficulty_levels` (`dlevel_id`, `level_name`) VALUES
(1, 'easy'),
(2, 'tough'),
(3, 'medium');

-- --------------------------------------------------------

--
-- Table structure for table `login_admin`
--

CREATE TABLE IF NOT EXISTS `login_admin` (
  `admin_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_admin`
--

INSERT INTO `login_admin` (`admin_id`, `passwd`, `name`, `profile_pic`, `active`) VALUES
(1, '1234', 'Amit', 'kg.jpg', 1),
(2, '67dfg', 'asdfg', 'qwe', 0),
(3, 'login@123', 'Kaushik', 'kaushik.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_test`
--

CREATE TABLE IF NOT EXISTS `master_test` (
  `test_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `paper_1` int(11) NOT NULL,
  `paper_2` int(11) NOT NULL,
  `main_paper` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `time_duration` int(11) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_test`
--

INSERT INTO `master_test` (`test_id`, `subject_id`, `paper_1`, `paper_2`, `main_paper`, `batch_id`, `status_id`, `time_duration`, `password`) VALUES
(1, 2, 2, 3, 2, 2, 2, 1000, '1234'),
(2, 4, 3, 3, 3, 1, 5, 1000, '2222'),
(3, 4, 1, 4, 4, 1, 3, 1000, '3333');

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE IF NOT EXISTS `paper` (
  `paper_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper`
--

INSERT INTO `paper` (`paper_id`, `trainer_id`) VALUES
(1, 1),
(3, 1),
(2, 2),
(4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `paper_status`
--

CREATE TABLE IF NOT EXISTS `paper_status` (
  `pstat_id` int(11) NOT NULL,
  `status` varchar(15) NOT NULL COMMENT 'paper approved,live,etc'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper_status`
--

INSERT INTO `paper_status` (`pstat_id`, `status`) VALUES
(1, 'abcd'),
(2, 'aaaa'),
(3, 'bbbb'),
(4, 'cccc'),
(5, 'dddd');

-- --------------------------------------------------------

--
-- Table structure for table `questions_obj`
--

CREATE TABLE IF NOT EXISTS `questions_obj` (
  `question_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `op_1` varchar(1000) NOT NULL,
  `op_2` varchar(1000) NOT NULL,
  `op_3` varchar(1000) NOT NULL,
  `op_4` varchar(1000) NOT NULL,
  `difficulty_level` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of options, which are answers of this question',
  `marks` int(11) NOT NULL COMMENT 'total mark of this question'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_obj`
--

INSERT INTO `questions_obj` (`question_id`, `question`, `op_1`, `op_2`, `op_3`, `op_4`, `difficulty_level`, `answers`, `marks`) VALUES
(1, 'choose app?', 'aa', 'bb', 'cc', 'dd', 1, 'aa', 2),
(2, 'give ans?', '11', '22', '33', '44', 3, '33', 2),
(3, 'choose', '12', '23', '34', '45', 2, '23', 2),
(4, 'give ans', '99', '88', '77', '66', 2, '77', 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions_subj`
--

CREATE TABLE IF NOT EXISTS `questions_subj` (
  `sq_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `marks` int(11) NOT NULL,
  `difficulty_level` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_subj`
--

INSERT INTO `questions_subj` (`sq_id`, `question`, `marks`, `difficulty_level`) VALUES
(6666, 'give ex of rdbms', 4, 2),
(7777, 'what is rdbms', 4, 3),
(8888, 'give all testing methods?', 4, 2),
(9999, 'define sql?', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `trainee_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `total_marks` decimal(10,0) NOT NULL,
  `time_remaining` int(11) NOT NULL,
  `exam_completed` int(11) NOT NULL COMMENT '1- yes,0 - No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`trainee_id`, `test_id`, `paper_id`, `total_marks`, `time_remaining`, `exam_completed`) VALUES
(101, 1, 2, '75', 1500, 1),
(102, 1, 1, '75', 1500, 0),
(103, 1, 3, '75', 1500, 0),
(104, 1, 4, '75', 1500, 1),
(104, 1, 3, '75', 1500, 1),
(103, 1, 3, '75', 1500, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subject_detail`
--

CREATE TABLE IF NOT EXISTS `subject_detail` (
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_detail`
--

INSERT INTO `subject_detail` (`subject_id`, `subject_name`) VALUES
(1, 'dotNET'),
(2, 'HTML'),
(3, 'PHP'),
(4, 'WCF'),
(5, 'Javscript');

-- --------------------------------------------------------

--
-- Table structure for table `test_question_obj`
--

CREATE TABLE IF NOT EXISTS `test_question_obj` (
  `paper_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_obj`
--

INSERT INTO `test_question_obj` (`paper_id`, `question_id`) VALUES
(1, 1),
(2, 2),
(2, 3),
(3, 4),
(2, 3),
(3, 3),
(4, 3),
(2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `test_question_subj`
--

CREATE TABLE IF NOT EXISTS `test_question_subj` (
  `paper_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_subj`
--

INSERT INTO `test_question_subj` (`paper_id`, `sq_id`) VALUES
(3, 8888),
(1, 6666),
(2, 7777),
(3, 9999),
(3, 7777),
(4, 8888),
(2, 7777),
(4, 8888);

-- --------------------------------------------------------

--
-- Table structure for table `trainee`
--

CREATE TABLE IF NOT EXISTS `trainee` (
  `trainee_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `institute` varchar(200) NOT NULL,
  `joining_date` date NOT NULL,
  `stream` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `admin_comment` varchar(1000) NOT NULL,
  `comment_visibility` tinyint(1) NOT NULL,
  `comment_date` date NOT NULL COMMENT 'date when comment last modified',
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainee`
--

INSERT INTO `trainee` (`trainee_id`, `passwd`, `name`, `institute`, `joining_date`, `stream`, `profile_pic`, `admin_comment`, `comment_visibility`, `comment_date`, `active`) VALUES
(101, 'aA', 'swapnil', 'gla university', '2015-10-12', 'cs', 'swap.jpg', 'Very good', 1, '2015-11-18', 1),
(102, 'bB', 'Arpitha', 'St Josephs', '2015-10-09', 'IS', 'arp.jpg', 'Good', 1, '2016-01-02', 0),
(103, 'cC', 'Shivani', 'SJEC', '2015-10-13', 'EC', 'shiv.jpg', 'Great work', 0, '2016-01-01', 1),
(104, 'dD', 'anjali', 'gla university', '2015-10-12', 'cs', 'anji.jpg', 'nice', 0, '2016-01-20', 0),
(123, 'swap', 'swapnil', 'gla', '0000-00-00', 'cs', '', 'good', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE IF NOT EXISTS `trainer` (
  `trainer_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ph_no` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`trainer_id`, `passwd`, `name`, `ph_no`, `email`, `profile_pic`, `active`) VALUES
(1, 'dotnet@123', 'Ramnath', '8934572214', 'nishadramnath@gmail.com', 'ram.jpg', 1),
(2, 'testing@123', 'neha', '9877435633', 'neha22@gmail.com', 'neha.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD PRIMARY KEY (`sanswer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD KEY `oq_id` (`oq_id`),
  ADD KEY `answer_id` (`answer_id`);

--
-- Indexes for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD KEY `sanswer_id` (`sanswer_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD PRIMARY KEY (`batch_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `batch_id_2` (`batch_id`),
  ADD KEY `trainee_id_2` (`trainee_id`);

--
-- Indexes for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  ADD PRIMARY KEY (`dlevel_id`);

--
-- Indexes for table `login_admin`
--
ALTER TABLE `login_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `master_test`
--
ALTER TABLE `master_test`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `paper_1` (`paper_1`),
  ADD KEY `paper_2` (`paper_2`),
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`paper_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `paper_status`
--
ALTER TABLE `paper_status`
  ADD PRIMARY KEY (`pstat_id`);

--
-- Indexes for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD PRIMARY KEY (`sq_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `test_id` (`paper_id`),
  ADD KEY `test_id_2` (`paper_id`),
  ADD KEY `trainee_id_2` (`trainee_id`),
  ADD KEY `test_id_3` (`test_id`);

--
-- Indexes for table `subject_detail`
--
ALTER TABLE `subject_detail`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `trainee`
--
ALTER TABLE `trainee`
  ADD PRIMARY KEY (`trainee_id`),
  ADD KEY `institute` (`institute`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`trainer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_obj`
--
ALTER TABLE `answers_obj`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `answers_subj`
--
ALTER TABLE `answers_subj`
  MODIFY `sanswer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `batch_info`
--
ALTER TABLE `batch_info`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  MODIFY `dlevel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_admin`
--
ALTER TABLE `login_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_test`
--
ALTER TABLE `master_test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `paper`
--
ALTER TABLE `paper`
  MODIFY `paper_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `paper_status`
--
ALTER TABLE `paper_status`
  MODIFY `pstat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questions_obj`
--
ALTER TABLE `questions_obj`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `questions_subj`
--
ALTER TABLE `questions_subj`
  MODIFY `sq_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10000;
--
-- AUTO_INCREMENT for table `subject_detail`
--
ALTER TABLE `subject_detail`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trainer`
--
ALTER TABLE `trainer`
  MODIFY `trainer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD CONSTRAINT `answers_obj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_obj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD CONSTRAINT `answers_subj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD CONSTRAINT `answer_detail_obj_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answers_obj` (`answer_id`),
  ADD CONSTRAINT `answer_detail_obj_ibfk_2` FOREIGN KEY (`oq_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD CONSTRAINT `answer_detail_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `answer_detail_subj_ibfk_2` FOREIGN KEY (`sanswer_id`) REFERENCES `answers_subj` (`sanswer_id`);

--
-- Constraints for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD CONSTRAINT `batch_info_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD CONSTRAINT `batch_trainee_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `batch_trainee_ibfk_2` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`);

--
-- Constraints for table `master_test`
--
ALTER TABLE `master_test`
  ADD CONSTRAINT `master_test_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subject_detail` (`subject_id`),
  ADD CONSTRAINT `master_test_ibfk_2` FOREIGN KEY (`paper_1`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_3` FOREIGN KEY (`paper_2`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_4` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`),
  ADD CONSTRAINT `master_test_ibfk_5` FOREIGN KEY (`status_id`) REFERENCES `paper_status` (`pstat_id`);

--
-- Constraints for table `paper`
--
ALTER TABLE `paper`
  ADD CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD CONSTRAINT `questions_obj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD CONSTRAINT `questions_subj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `result_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `result_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `result_ibfk_3` FOREIGN KEY (`test_id`) REFERENCES `master_test` (`test_id`);

--
-- Constraints for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD CONSTRAINT `test_question_obj_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `test_question_obj_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD CONSTRAINT `test_question_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `test_question_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
