-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2016 at 03:33 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineexam`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_obj`
--

CREATE TABLE `answers_obj` (
  `answer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_obj` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_obj`
--

INSERT INTO `answers_obj` (`answer_id`, `trainee_id`, `paper_id`, `marks_obj`) VALUES
(7, 102, 1, '0'),
(8, 103, 3, '8'),
(9, 104, 1, '10'),
(10, 105, 1, '8'),
(11, 108, 1, '5'),
(12, 104, 6, '12'),
(17, 109, 2, '2'),
(18, 110, 2, '0'),
(19, 111, 4, '4'),
(20, 112, 2, '2'),
(21, 113, 2, '-1'),
(22, 102, 9, '-1'),
(23, 103, 9, '-1'),
(24, 104, 9, '-1'),
(25, 105, 9, '-1'),
(27, 106, 1, '6'),
(28, 106, 9, '-1'),
(29, 107, 9, '-1'),
(30, 108, 9, '-1'),
(31, 102, 13, '4'),
(32, 104, 13, '5'),
(33, 105, 13, '2'),
(34, 108, 13, '1'),
(35, 103, 13, '2'),
(36, 106, 13, '3'),
(37, 102, 15, '1'),
(38, 104, 15, '2'),
(39, 105, 15, '3'),
(40, 108, 15, '4'),
(41, 103, 15, '5'),
(42, 106, 15, '3'),
(43, 107, 13, '4'),
(44, 107, 13, '2');

-- --------------------------------------------------------

--
-- Table structure for table `answers_subj`
--

CREATE TABLE `answers_subj` (
  `sanswer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_subj` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_subj`
--

INSERT INTO `answers_subj` (`sanswer_id`, `trainee_id`, `paper_id`, `marks_subj`) VALUES
(9, 102, 1, '11'),
(10, 104, 1, '8'),
(11, 105, 1, '0'),
(12, 108, 1, '3'),
(13, 103, 3, '5'),
(14, 104, 6, '8'),
(19, 102, 9, '-1'),
(20, 103, 9, '-1'),
(21, 104, 9, '-1'),
(22, 105, 9, '-1'),
(24, 106, 1, '13'),
(25, 106, 9, '-1'),
(26, 107, 9, '-1'),
(27, 108, 9, '-1'),
(28, 102, 13, '15'),
(29, 104, 13, '13'),
(30, 105, 13, '10'),
(31, 108, 13, '5'),
(32, 103, 13, '15'),
(33, 106, 13, '20'),
(34, 102, 15, '10'),
(35, 104, 15, '20'),
(36, 105, 15, '15'),
(37, 108, 15, '9'),
(38, 103, 15, '12'),
(39, 106, 15, '16'),
(40, 107, 13, '9'),
(41, 107, 15, '13');

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_obj`
--

CREATE TABLE `answer_detail_obj` (
  `answer_id` int(11) NOT NULL,
  `oq_id` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of answers marked',
  `marks` int(11) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_obj`
--

INSERT INTO `answer_detail_obj` (`answer_id`, `oq_id`, `answers`, `marks`, `marked_review`) VALUES
(7, 2, 'Public', 0, 0),
(7, 1, 'None', 0, 0),
(7, 3, 'None', 0, 0),
(7, 4, '1', 0, 1),
(7, 5, 'System.UInt16', 0, 1),
(8, 6, 'sbyte', 4, 0),
(8, 7, 's1.Equals(s2)', 4, 0),
(8, 8, 'super', 0, 1),
(9, 1, 'common language runtime', 2, 0),
(9, 2, 'Protected', 0, 0),
(9, 3, 'Common time system', 2, 1),
(9, 4, '8', 3, 1),
(9, 5, 'System.UInt32', 3, 0),
(10, 1, 'common language runtime', 2, 0),
(10, 2, 'Protected', 0, 0),
(10, 3, 'None', 0, 1),
(10, 4, '8', 3, 0),
(10, 5, 'System.UInt32', 3, 0),
(11, 1, 'common language runtime', 2, 1),
(11, 2, 'Public', 0, 1),
(11, 3, 'None', 0, 1),
(11, 4, '8', 3, 0),
(11, 5, 'System.UInt64', 0, 0),
(22, 13, 'sn', 0, 0),
(22, 14, 'Garbage Collector', 0, 1),
(22, 15, 'All of the above', 0, 1),
(22, 16, 'SOA', 0, 1),
(23, 13, 'ildasm', 0, 1),
(23, 14, 'CLR', 0, 1),
(23, 15, 'All of the above', 0, 1),
(23, 16, 'SOA', 0, 1),
(24, 13, 'ngen', 0, 1),
(24, 14, 'Garbage Collector', 0, 1),
(24, 15, 'All of the above', 0, 1),
(24, 16, 'POA', 0, 1),
(25, 13, 'ngen', 0, 1),
(25, 14, 'Garbage Collector', 0, 1),
(25, 15, 'All of the above', 0, 1),
(25, 16, 'SOA', 0, 1),
(27, 1, 'None', 0, 0),
(27, 2, 'Public', 0, 0),
(27, 3, 'None', 0, 0),
(27, 4, '8', 3, 0),
(27, 5, 'System.UInt32', 3, 0),
(28, 13, 'sn', 0, 0),
(28, 14, 'CLR', 0, 0),
(28, 15, 'Page-level resources', 0, 0),
(28, 16, 'SOA', 0, 0),
(29, 13, 'ngen', 0, 0),
(29, 14, 'All of the above', 0, 0),
(29, 15, 'Page-level resources', 0, 0),
(29, 16, 'SOA', 0, 0),
(30, 13, 'ngen', 0, 0),
(30, 14, 'CLR', 0, 0),
(30, 15, 'All of the above', 0, 0),
(30, 16, 'POA', 0, 0),
(31, 20, 'All of the above', 2, 0),
(31, 21, 'cross-browser', 2, 0),
(31, 22, 'media', 0, 0),
(32, 20, 'all of the above', 2, 0),
(32, 21, 'cross-browser', 2, 0),
(32, 22, 'all of the above', 1, 0),
(33, 20, 'Linux', 0, 0),
(33, 21, 'cross-browser', 2, 0),
(33, 22, 'graphics', 0, 0),
(34, 20, 'linux', 0, 0),
(34, 21, 'all of the above', 0, 0),
(34, 22, 'all of the above', 1, 0),
(35, 20, 'all of the above', 2, 0),
(35, 21, 'all of the above', 0, 0),
(35, 22, 'graphics', 0, 0),
(36, 20, 'all of the above', 2, 0),
(36, 21, 'all of the above', 0, 0),
(36, 22, 'all of the above', 1, 0),
(37, 23, '.dbml', 1, 0),
(37, 24, 'Linear integrated query', 0, 0),
(37, 25, 'paragraph contentholder', 0, 0),
(38, 23, '.xml', 0, 0),
(38, 24, 'Language integrated query', 2, 0),
(38, 25, 'paragraph contentholder', 0, 0),
(39, 23, '.dbml', 1, 0),
(39, 24, 'Language integrated query', 2, 0),
(39, 25, 'paragraph integrated query', 0, 0),
(40, 23, '.xml', 0, 0),
(40, 24, 'Language integrated query', 2, 0),
(40, 25, 'content placeholder', 2, 0),
(41, 23, '.dbml', 1, 0),
(41, 24, 'Language Integrated query', 2, 0),
(41, 25, 'content placeholder', 2, 0),
(42, 23, '.dbml', 1, 0),
(42, 24, 'linear integrated value', 0, 0),
(42, 25, 'content placeholder', 2, 0),
(43, 20, 'All of the above', 2, 0),
(43, 21, 'cross-browser', 2, 0),
(43, 22, 'media', 0, 0),
(44, 23, '.xml', 0, 0),
(44, 24, 'Language integrated query', 2, 0),
(44, 25, 'paragraph placeholder', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_subj`
--

CREATE TABLE `answer_detail_subj` (
  `sanswer_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `marks` decimal(10,0) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_subj`
--

INSERT INTO `answer_detail_subj` (`sanswer_id`, `sq_id`, `answer`, `marks`, `comments`, `marked_review`) VALUES
(9, 10000, 'inner, outer', '5', ':-)', 1),
(9, 10001, 'Protocols ', '5', ':)', 1),
(9, 10002, 'Private, public,protected,internal,protected internal', '1', 'elaborate', 0),
(11, 10000, 'qqq', '0', 'gud', 1),
(11, 10001, 'AAA', '0', 'OK', 1),
(11, 10002, 'CCC', '0', 'bad answer', 0),
(12, 10000, 'CC', '0', '', 1),
(13, 10003, 'FO', '3', 'AVERAGE', 1),
(13, 10004, 'FU', '2', ':/', 1),
(10, 10000, 'oooooo', '3', '', 0),
(10, 10001, 'ooaaoa', '2', '', 0),
(10, 10002, 'D for Dog', '3', 'Very Good', 1),
(12, 10001, 'AA', '1', '', 0),
(12, 10002, 'ss', '2', 'Gud', 0),
(22, 10009, 'ooooo', '0', '', 1),
(22, 10010, 'oho', '0', '', 1),
(22, 10011, 'ohjoo', '0', '', 1),
(22, 10012, 'ohjoo', '0', '', 1),
(19, 10009, 'sa', '0', '', 1),
(19, 10010, 'sava', '0', '', 1),
(19, 10011, 'saahyayhu', '0', '', 1),
(19, 10012, 'saagyahyhjujj', '0', '', 1),
(20, 10009, 'sa', '0', '', 1),
(20, 10010, 'sa', '0', '', 1),
(20, 10011, 'sa', '0', '', 1),
(20, 10012, 'sa', '0', '', 1),
(21, 10009, 'saatttyyy', '0', '', 1),
(21, 10010, 'saswdhh', '0', '', 1),
(21, 10011, 'sabhhjuk', '0', '', 1),
(21, 10012, 'safrtyhyuhju', '0', '', 1),
(24, 10000, ' rrighjt thytj gbtrytyjmhy ytyikuh fgtjm\r\n', '5', '', 0),
(24, 10001, ' rrighjt thytj gbtrytyjmhy ytyikuh fgtjm\r\n', '5', '', 0),
(24, 10002, ' rrighjt thytj gbtrytyjmhy ytyikuh fgtjm\r\n', '3', '', 0),
(25, 10009, 'adfsdgdh', '0', '', 0),
(25, 10010, 'adfsdgdsdsah', '0', '', 0),
(25, 10011, 'adfsdedfdgdsdsah', '0', '', 0),
(25, 10012, 'fdafadfsdgdsdsah', '0', '', 0),
(26, 10009, 'dasfwegtrwegyre', '0', '', 0),
(26, 10010, 'dasfwegtrwegyre', '0', '', 0),
(26, 10011, 'dasfwegtrwegyre', '0', '', 0),
(26, 10012, 'ddfsasfwegtrwegyre', '0', '', 0),
(27, 10009, 'dasfwegtrwegyre', '0', '', 0),
(27, 10010, 'dasfwegtrwegyre', '0', '', 0),
(27, 10011, 'dasfwegtrwegyre', '0', '', 0),
(27, 10012, 'dasfwegtrwegyre', '0', '', 0),
(28, 10015, 'AAAAAsefweqtfrwegfrwgr wqfewqgfrwrw', '10', '', 0),
(28, 10016, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfrwrw', '5', 'not enough', 0),
(29, 10015, 'AAfsggfAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfrwrw', '3', 'not enough', 0),
(29, 10016, 'AAAAAsefdshadhfhfweqtfrwegfrwasdsfsdgr wqfewqgfrwrw', '10', 'good', 0),
(30, 10015, 'AAAAAsefweqtfrwefdhdghdghgfrwasdsfsdgr wqfewqgfrwrw', '5', 'not enough', 0),
(30, 10016, 'AAAAAsefweqtfrwegfrwasdsfhfdsgfhsdgr wqfewqgfrwrw', '5', 'not enough', 0),
(31, 10015, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqhdfhghfewqgfrwrw', '0', 'put efforts', 0),
(31, 10016, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfdfhgfhhrwrw', '5', 'not enough', 0),
(32, 10015, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfrwrdhfhdghgdw', '6', 'not enough', 0),
(32, 10016, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqhdhgshgfrwrw', '9', 'missed a main point', 0),
(33, 10015, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfrwrfgfdgfdhdw', '10', 'good', 0),
(33, 10016, 'AAAAAsefweqtfrwegfrwasdsfsdgr wqfewqgfdsgfsgfrwrw', '10', 'good', 0),
(34, 10017, 'fkdnegvifsngebnbklsdhfioahngil fsdgsdhdh', '5', 'not enough', 0),
(34, 10018, 'fkdnegvifsngebnbklsdhfdtytytruioahngil fsdgsdhdh', '5', 'not enough', 0),
(35, 10017, 'fkdnegvifsngebnbklsdhfwwjhewqfjkrejkqrjkebdtytytruioahngil fsdgsdhdh', '10', 'good', 0),
(35, 10018, 'fkdnegvifsngebnbklsdhfdfjdshnjidfhnjisdngjistytytruioahngil fsdgsdhdh', '10', 'good', 0),
(36, 10017, 'fkdnfgsdhggdhgdegvifsngebnbklsdhfdtytytruioahngil fsdgsdhdh', '7', 'not clearly answered', 0),
(36, 10018, 'fkdnegvifsfhgdhgjghjngebnbklsdhfdtytytruioahngil fsdgsdhdh', '8', 'explain completely!', 0),
(37, 10017, 'fkdnegvidfgfsngebnbklsdhfdtytytruioahngil fsdgsdhdh', '3', 'prepare well', 0),
(37, 10018, 'fkdnegvifsfgfhghjjngebnbklsdhfdtytytruioahngil sdsfsdgsdhdh', '6', 'not enough', 0),
(38, 10017, 'fkdnegvifsngebnbklsdhfdtytytruioahngil fsdgsdhdh', '6', 'not enough', 0),
(38, 10018, 'fkdnegvifsngebnbklsdhfdtytytruioahngil fsdgsdhdh', '6', 'not enough', 0),
(39, 10017, 'fkdnegvifsngebnbklsdhfdtytytruioahngil fssdqefedgfvdgsdhdh', '8', 'explain more!', 0),
(39, 10018, 'fkdnegvifsngebnbklsdhfdtytytruioahngil fetfqeesdgsdhdh', '8', 'explain more!', 0),
(40, 10015, 'ewgyrebhy thu tuzsreyh', '6', 'Not enough', 0),
(40, 10016, 'ewgyrebhy thu tuzsfdsfdsgreyh', '3', 'explain more', 0),
(41, 10017, 'dcbfjisdjighuifsdh fish uishuihfuisd', '6', 'not convincing', 0),
(41, 10018, 'dcfhgfdhgdbfjisdjighuifsdh fish uishuihfuisd', '7', 'not enough', 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_info`
--

CREATE TABLE `batch_info` (
  `batch_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL COMMENT 'trainer teaching this batch',
  `batch_name` varchar(50) NOT NULL COMMENT 'friendly name for batch',
  `batch_date` date NOT NULL COMMENT 'batch start date',
  `max_marks` int(11) NOT NULL COMMENT 'max marks paper for batch',
  `active` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_info`
--

INSERT INTO `batch_info` (`batch_id`, `trainer_id`, `batch_name`, `batch_date`, `max_marks`, `active`) VALUES
(1, 1, 'dotNET', '2015-10-12', 25, 1),
(2, 1, 'testing', '2015-10-12', 25, 0),
(3, 2, 'JAVA', '2015-10-12', 25, 1),
(4, 2, 'ADVJAVA', '2015-10-12', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_trainee`
--

CREATE TABLE `batch_trainee` (
  `batch_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `barred` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains mapping of trainees to particular batch';

--
-- Dumping data for table `batch_trainee`
--

INSERT INTO `batch_trainee` (`batch_id`, `trainee_id`, `barred`) VALUES
(1, 101, 1),
(1, 102, 0),
(1, 103, 0),
(1, 104, 0),
(1, 108, 0),
(1, 105, 0),
(1, 106, 0),
(1, 107, 0),
(3, 109, 0),
(3, 110, 0),
(3, 111, 0),
(3, 112, 0),
(3, 113, 0);

-- --------------------------------------------------------

--
-- Table structure for table `difficulty_levels`
--

CREATE TABLE `difficulty_levels` (
  `dlevel_id` int(11) NOT NULL,
  `level_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficulty_levels`
--

INSERT INTO `difficulty_levels` (`dlevel_id`, `level_name`) VALUES
(1, 'Easy'),
(2, 'Medium'),
(3, 'Difficult');

-- --------------------------------------------------------

--
-- Table structure for table `login_admin`
--

CREATE TABLE `login_admin` (
  `admin_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_admin`
--

INSERT INTO `login_admin` (`admin_id`, `passwd`, `name`, `profile_pic`, `active`) VALUES
(1, '1234', 'Amit', 'kg.jpg', 1),
(2, '67dfg', 'asdfg', 'qwe', 0),
(3, 'login@123', 'Kaushik', 'kaushik.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_test`
--

CREATE TABLE `master_test` (
  `test_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `paper_1` int(11) NOT NULL,
  `paper_2` int(11) NOT NULL,
  `main_paper` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `time_duration` int(11) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_test`
--

INSERT INTO `master_test` (`test_id`, `subject_id`, `paper_1`, `paper_2`, `main_paper`, `batch_id`, `status_id`, `time_duration`, `password`) VALUES
(1, 1, 1, 3, 1, 1, 3, 1000, '1234'),
(2, 2, 5, 6, 5, 1, 2, 1000, '2222'),
(3, 3, 9, 10, 9, 1, 3, 1000, '3333'),
(4, 4, 11, 12, 11, 1, 1, 2000, '123'),
(5, 5, 13, 14, 13, 1, 3, 1000, 'AA'),
(6, 6, 15, 16, 15, 1, 3, 1000, 'AA');

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE `paper` (
  `paper_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper`
--

INSERT INTO `paper` (`paper_id`, `trainer_id`) VALUES
(1, 1),
(3, 1),
(5, 1),
(6, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(2, 2),
(4, 2),
(7, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `paper_status`
--

CREATE TABLE `paper_status` (
  `pstat_id` int(11) NOT NULL,
  `status` varchar(15) NOT NULL COMMENT 'paper approved,live,etc'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper_status`
--

INSERT INTO `paper_status` (`pstat_id`, `status`) VALUES
(1, 'Preparing'),
(2, 'SentforApproval'),
(3, 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `questions_obj`
--

CREATE TABLE `questions_obj` (
  `question_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `op_1` varchar(1000) NOT NULL,
  `op_2` varchar(1000) NOT NULL,
  `op_3` varchar(1000) NOT NULL,
  `op_4` varchar(1000) NOT NULL,
  `difficulty_level` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of options, which are answers of this question',
  `marks` int(11) NOT NULL COMMENT 'total mark of this question'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_obj`
--

INSERT INTO `questions_obj` (`question_id`, `question`, `op_1`, `op_2`, `op_3`, `op_4`, `difficulty_level`, `answers`, `marks`) VALUES
(1, 'what is the full form of CLR?', 'call linear runtime', 'common language runtime', 'common link runtime', 'none', 1, 'common language runtime', 2),
(2, 'Which of the following is not a access modifier?', 'Private', 'Protected', 'Simple', 'Public', 3, 'Simple', 2),
(3, 'What is the full form of CTS?', 'Common time system', 'Common transaction system', 'Clear time system ', 'None', 2, 'Common time system', 2),
(4, 'How many Bytes are stored by ‘Long’ Datatype in C# .net?', '8', '4', '2', '1', 2, '8', 3),
(5, 'Choose “.NET class” name from which datatype “UInt” is derived ?', 'System.Int16', 'System.UInt32', 'System.UInt64', 'System.UInt16', 3, 'System.UInt32', 3),
(6, 'Which datatype should be more preferred for storing a simple number like 35 to improve execution speed of a program?', 'sbyte', 'short', 'int', 'long', 2, 'sbyte', 4),
(7, ' 	\r\nIf s1 and s2 are references to two strings, then which of the following is the correct way to compare the two references?', 's1.Equals(s2)', 's1 is s2', 'strcmp(s1, s2)', 's1 == s2', 2, 's1.Equals(s2)', 4),
(8, 'Choose the keyword which declares the indexer?', 'base', 'this', 'super', 'extract', 3, 'this', 4),
(9, 'From which of these classes, the character based output stream class Stream Writer is derived?', 'TextWriter', 'TextReader', 'Character Stream', 'All of the above', 2, 'TextWriter', 6),
(10, ' Which of these is a method used to clear all the data present in output buffers?', 'clear()', ' flush()', 'fflush()', 'close()', 3, ' flush()', 6),
(11, 'Which of the following is used to perform all input & output operations in C#?', 'streams', 'Variables', 'classes', 'Methods', 1, 'streams', 6),
(12, 'Which of these classes contains only floating point functions?', ' math', 'process', 'system', 'object', 1, 'math', 6),
(13, '	\r\nWhich of the following utilities can be used to compile managed assemblies into processor-specific native code?', 'gacutil', 'ildasm', 'sn', 'ngen', 3, 'ngen', 2),
(14, '	\r\nWhich of the following .NET components can be used to remove unused references from the managed heap?', 'Common Language Infrastructure', 'CLR', 'Garbage Collector', 'Class Loader', 2, 'Garbage Collector', 2),
(15, '	\r\nWhich of the following are scopes of resources?', 'Application Resources', 'Page-level resources', 'Element level resources', 'All of the above', 1, 'All of the above', 1),
(16, '	\r\nWhich architecture does WCF follow?', 'SOA', 'POA', 'Both A and B', 'None', 2, 'SOA', 2),
(17, 'Which of the following are types of styling?', 'All of the below', 'Inline', 'External', 'Embedded', 1, 'All of the below', 2),
(18, 'Which is used for making the page interactive?', 'Javascript', 'CSS', 'Both A and B', 'None', 2, 'Javascript', 2),
(19, 'Which of the following is non validating parsers?', 'XML', 'DTD', 'XSLT', 'None', 3, 'DTD', 1),
(20, 'Silverlight runs on which of the following?', 'windows', 'MAC', 'Linux', 'All of the above', 1, 'All of the above', 2),
(21, 'Which of the following are features of silverlight?', 'cross-browser', 'Platform dependent', 'Plug in contains full version of CLR', 'All of the above ', 2, 'cross-browser', 2),
(22, 'Which of the following are supported by  silverlight?', 'media', 'documents', 'graphics', 'All of the above ', 3, 'All of the above', 1),
(23, 'What is the extension of the file, when LINQ to SQL is used?', '.xml', '.db', '.dbml', '.exe', 2, '.dbml', 1),
(24, 'What is the full form of  LINQ?', 'Language integrated query', 'linear integrated query', 'language integrated query', 'None', 2, 'Language integrated query', 1),
(25, 'What does master page contain?', 'Content placeholder', 'paragraph place holder', 'Graphical place holder', 'None', 2, 'Content placeholder', 3);

-- --------------------------------------------------------

--
-- Table structure for table `questions_subj`
--

CREATE TABLE `questions_subj` (
  `sq_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `marks` int(11) NOT NULL,
  `difficulty_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_subj`
--

INSERT INTO `questions_subj` (`sq_id`, `question`, `marks`, `difficulty_level`) VALUES
(10000, 'What are the different joins in SQL?', 5, 1),
(10001, 'What are the differences between WCF and Web Services?', 3, 2),
(10002, 'what are the different access modifiers in C#?', 5, 3),
(10003, 'Describe Stored procedures in SQL?', 7, 3),
(10004, 'what are the different types of styling in CSS?', 6, 2),
(10005, 'Explain WPF?', 6, 3),
(10006, 'Explain the page life cycle of ASP.NET?', 7, 1),
(10007, 'Differenciate the connected and disconnected mode in ADO.NET? ', 7, 2),
(10008, 'Difference between XML and XAML?', 6, 2),
(10009, 'What are the different modes of binding?', 5, 1),
(10010, 'Explain State Management', 5, 2),
(10011, 'Explain Custom Validation', 4, 1),
(10012, 'Explain different types of data binding?', 3, 3),
(10013, 'Explain DTD and XSD in detail', 10, 3),
(10014, 'Explain different types of styling using javascript?', 10, 3),
(10015, 'Difference between WPF and silverlight?', 10, 3),
(10016, 'Give the features of silverlight', 10, 3),
(10017, 'What is the difference between LINQ and EF?', 10, 2),
(10018, 'Explain lambda expression and query expression', 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `trainee_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `total_marks` decimal(10,0) NOT NULL,
  `time_remaining` int(11) NOT NULL,
  `exam_completed` int(11) NOT NULL COMMENT '1- yes,0 - No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`trainee_id`, `test_id`, `paper_id`, `total_marks`, `time_remaining`, `exam_completed`) VALUES
(102, 1, 1, '11', 2000, 1),
(108, 1, 1, '8', 3000, 1),
(104, 1, 1, '18', 2000, 1),
(105, 1, 1, '8', 1000, 1),
(102, 3, 9, '-1', 2000, 1),
(103, 3, 9, '-1', 2000, 1),
(105, 3, 9, '-1', 2000, 1),
(104, 3, 9, '-1', 2000, 1),
(103, 1, 3, '13', 2000, 1),
(106, 1, 1, '-1', 2000, 1),
(106, 3, 9, '-1', 2000, 1),
(107, 3, 9, '-1', 2000, 1),
(108, 3, 9, '-1', 2000, 1),
(102, 5, 13, '19', 1000, 1),
(104, 5, 13, '18', 1000, 1),
(105, 5, 13, '12', 1000, 1),
(108, 5, 13, '6', 1000, 1),
(103, 5, 13, '17', 1000, 1),
(106, 5, 13, '23', 1000, 1),
(102, 6, 15, '16', 1000, 1),
(104, 6, 15, '22', 1000, 1),
(105, 6, 15, '18', 1000, 1),
(108, 6, 15, '13', 1000, 1),
(103, 6, 15, '17', 1000, 1),
(106, 6, 15, '19', 1000, 1),
(107, 5, 13, '13', 1000, 1),
(107, 6, 15, '15', 1000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject_detail`
--

CREATE TABLE `subject_detail` (
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_detail`
--

INSERT INTO `subject_detail` (`subject_id`, `subject_name`) VALUES
(1, 'C#'),
(2, 'HTML'),
(3, 'PHP'),
(4, 'WCF'),
(5, 'Silverlight'),
(6, 'LINQ');

-- --------------------------------------------------------

--
-- Table structure for table `test_question_obj`
--

CREATE TABLE `test_question_obj` (
  `paper_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_obj`
--

INSERT INTO `test_question_obj` (`paper_id`, `question_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(3, 6),
(3, 7),
(3, 8),
(6, 9),
(6, 10),
(5, 17),
(5, 18),
(9, 13),
(9, 14),
(9, 15),
(9, 16),
(5, 19),
(13, 20),
(13, 21),
(13, 22),
(15, 23),
(15, 24),
(15, 25);

-- --------------------------------------------------------

--
-- Table structure for table `test_question_subj`
--

CREATE TABLE `test_question_subj` (
  `paper_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_subj`
--

INSERT INTO `test_question_subj` (`paper_id`, `sq_id`) VALUES
(1, 10000),
(1, 10001),
(1, 10002),
(3, 10003),
(3, 10004),
(6, 10005),
(6, 10006),
(5, 10013),
(5, 10014),
(9, 10009),
(9, 10010),
(9, 10011),
(9, 10012),
(13, 10015),
(13, 10016),
(15, 10017),
(15, 10018);

-- --------------------------------------------------------

--
-- Table structure for table `trainee`
--

CREATE TABLE `trainee` (
  `trainee_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `institute` varchar(200) NOT NULL,
  `joining_date` date NOT NULL,
  `stream` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `admin_comment` varchar(1000) NOT NULL,
  `comment_visibility` tinyint(1) NOT NULL,
  `comment_date` date NOT NULL COMMENT 'date when comment last modified',
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainee`
--

INSERT INTO `trainee` (`trainee_id`, `passwd`, `name`, `institute`, `joining_date`, `stream`, `profile_pic`, `admin_comment`, `comment_visibility`, `comment_date`, `active`) VALUES
(101, 'aA', 'swapnil', 'gla university', '2015-10-12', 'cs', 'swap.jpg', 'Very good', 1, '2015-11-18', 1),
(102, 'bB', 'Arpitha', 'St Josephs', '2015-10-09', 'IS', 'arp.jpg', 'Good', 1, '2016-01-02', 0),
(103, 'cC', 'Shivani', 'SJEC', '2015-10-13', 'EC', 'shiv.jpg', 'Great work', 0, '2016-01-01', 1),
(104, 'dD', 'anjali', 'gla university', '2015-10-12', 'cs', 'anji.jpg', 'nice', 0, '2016-01-20', 0),
(105, 'EE', 'UB', 'SJEC', '2015-10-12', 'ECE', 'Koala.jpg', 'Good', 1, '2015-11-16', 1),
(106, 'EE', 'Niharika', 'SJEC', '2015-10-12', 'CS', 'Koala.jpg', 'HardWorker', 1, '2015-11-16', 1),
(107, 'ED', 'Manjappa', 'SJEC', '2015-10-12', 'ECE', 'Koala.jpg', 'Super Human', 1, '2015-12-08', 1),
(108, 'RR', 'Kriti', 'SJEC', '2015-10-12', 'ECE', 'Koala.jpg', 'Smart', 1, '2015-11-04', 1),
(109, 'EE', 'Tara', 'SJEC', '2015-10-12', 'ECE', 'Koala.jpg', 'lazy', 1, '2015-11-16', 1),
(110, 'WW', 'Shawn', 'SJEC', '2015-10-12', 'MECH', 'Koala.jpg', 'Notorius', 1, '2015-11-24', 1),
(111, 'WW', 'Chris', 'SJEC', '2015-10-12', 'MECH', 'Koala.jpg', 'Smart', 1, '2015-11-14', 1),
(112, 'qq', 'Parvathi', 'APUC', '2015-10-12', 'CSE', 'Koala.jpg', 'Good', 1, '2015-11-09', 1),
(113, 'qq', 'Elvis', 'APUC', '2015-10-12', 'ISE', 'Koala.jpg', 'Good', 1, '2015-11-19', 1),
(123, 'swap', 'swapnil', 'gla', '0000-00-00', 'cs', 'Koala.jpg', 'good', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE `trainer` (
  `trainer_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ph_no` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`trainer_id`, `passwd`, `name`, `ph_no`, `email`, `profile_pic`, `active`) VALUES
(1, 'login@123', 'Ramnath', '8934572214', 'nishadramnath@gmail.com', 'Koala.jpg', 1),
(2, 'testing@123', 'neha', '9877435633', 'neha22@gmail.com', 'neha.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD PRIMARY KEY (`sanswer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD KEY `oq_id` (`oq_id`),
  ADD KEY `answer_id` (`answer_id`);

--
-- Indexes for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD KEY `sanswer_id` (`sanswer_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD PRIMARY KEY (`batch_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `batch_id_2` (`batch_id`),
  ADD KEY `trainee_id_2` (`trainee_id`);

--
-- Indexes for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  ADD PRIMARY KEY (`dlevel_id`);

--
-- Indexes for table `login_admin`
--
ALTER TABLE `login_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `master_test`
--
ALTER TABLE `master_test`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `paper_1` (`paper_1`),
  ADD KEY `paper_2` (`paper_2`),
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`paper_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `paper_status`
--
ALTER TABLE `paper_status`
  ADD PRIMARY KEY (`pstat_id`);

--
-- Indexes for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD PRIMARY KEY (`sq_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `test_id` (`paper_id`),
  ADD KEY `test_id_2` (`paper_id`),
  ADD KEY `trainee_id_2` (`trainee_id`),
  ADD KEY `test_id_3` (`test_id`);

--
-- Indexes for table `subject_detail`
--
ALTER TABLE `subject_detail`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `trainee`
--
ALTER TABLE `trainee`
  ADD PRIMARY KEY (`trainee_id`),
  ADD KEY `institute` (`institute`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`trainer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_obj`
--
ALTER TABLE `answers_obj`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `answers_subj`
--
ALTER TABLE `answers_subj`
  MODIFY `sanswer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `batch_info`
--
ALTER TABLE `batch_info`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  MODIFY `dlevel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_admin`
--
ALTER TABLE `login_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_test`
--
ALTER TABLE `master_test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `paper`
--
ALTER TABLE `paper`
  MODIFY `paper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `paper_status`
--
ALTER TABLE `paper_status`
  MODIFY `pstat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questions_obj`
--
ALTER TABLE `questions_obj`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `questions_subj`
--
ALTER TABLE `questions_subj`
  MODIFY `sq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10019;
--
-- AUTO_INCREMENT for table `subject_detail`
--
ALTER TABLE `subject_detail`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `trainer`
--
ALTER TABLE `trainer`
  MODIFY `trainer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD CONSTRAINT `answers_obj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_obj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD CONSTRAINT `answers_subj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD CONSTRAINT `answer_detail_obj_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answers_obj` (`answer_id`),
  ADD CONSTRAINT `answer_detail_obj_ibfk_2` FOREIGN KEY (`oq_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD CONSTRAINT `answer_detail_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `answer_detail_subj_ibfk_2` FOREIGN KEY (`sanswer_id`) REFERENCES `answers_subj` (`sanswer_id`);

--
-- Constraints for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD CONSTRAINT `batch_info_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD CONSTRAINT `batch_trainee_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `batch_trainee_ibfk_2` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`);

--
-- Constraints for table `master_test`
--
ALTER TABLE `master_test`
  ADD CONSTRAINT `master_test_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subject_detail` (`subject_id`),
  ADD CONSTRAINT `master_test_ibfk_2` FOREIGN KEY (`paper_1`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_3` FOREIGN KEY (`paper_2`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_4` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`),
  ADD CONSTRAINT `master_test_ibfk_5` FOREIGN KEY (`status_id`) REFERENCES `paper_status` (`pstat_id`);

--
-- Constraints for table `paper`
--
ALTER TABLE `paper`
  ADD CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD CONSTRAINT `questions_obj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD CONSTRAINT `questions_subj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `result_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `result_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `result_ibfk_3` FOREIGN KEY (`test_id`) REFERENCES `master_test` (`test_id`);

--
-- Constraints for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD CONSTRAINT `test_question_obj_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `test_question_obj_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD CONSTRAINT `test_question_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `test_question_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
