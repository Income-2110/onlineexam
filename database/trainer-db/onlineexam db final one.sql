-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 06:29 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineexam3`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_obj`
--

CREATE TABLE `answers_obj` (
  `answer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_obj` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_obj`
--

INSERT INTO `answers_obj` (`answer_id`, `trainee_id`, `paper_id`, `marks_obj`) VALUES
(7, 102, 1, '0'),
(8, 103, 3, '-1'),
(9, 104, 1, '10'),
(10, 105, 1, '8'),
(11, 108, 1, '5'),
(12, 104, 6, '12'),
(13, 102, 5, '6'),
(14, 103, 5, '6'),
(15, 108, 5, '0'),
(16, 105, 5, '12'),
(17, 109, 2, '2'),
(18, 110, 2, '0'),
(19, 111, 4, '4'),
(20, 112, 2, '2'),
(21, 113, 2, '-1');

-- --------------------------------------------------------

--
-- Table structure for table `answers_subj`
--

CREATE TABLE `answers_subj` (
  `sanswer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `marks_subj` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers_subj`
--

INSERT INTO `answers_subj` (`sanswer_id`, `trainee_id`, `paper_id`, `marks_subj`) VALUES
(9, 102, 1, '11'),
(10, 104, 1, '8'),
(11, 105, 1, '0'),
(12, 108, 1, '3'),
(13, 103, 3, '-1'),
(14, 104, 6, '8'),
(15, 102, 5, '0'),
(16, 103, 5, '6'),
(17, 108, 5, '5'),
(18, 105, 5, '7');

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_obj`
--

CREATE TABLE `answer_detail_obj` (
  `answer_id` int(11) NOT NULL,
  `oq_id` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of answers marked',
  `marks` int(11) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_obj`
--

INSERT INTO `answer_detail_obj` (`answer_id`, `oq_id`, `answers`, `marks`, `marked_review`) VALUES
(7, 2, '11', 0, 0),
(7, 1, 'bb', 0, 0),
(7, 3, '34', 0, 0),
(7, 4, '99', 0, 1),
(7, 5, 'C', 0, 1),
(8, 6, 'You', 4, 0),
(8, 7, 'Cat', 4, 0),
(8, 8, 'finest', 0, 1),
(9, 1, 'aa', 2, 0),
(9, 2, '11', 0, 0),
(9, 3, '23', 2, 1),
(9, 4, '99', 3, 1),
(9, 5, 'A', 3, 0),
(10, 1, 'aa', 2, 0),
(10, 2, '33', 0, 0),
(10, 3, '23', 0, 1),
(10, 4, '77', 3, 0),
(10, 5, 'A', 3, 0),
(12, 9, 'not here', 6, 1),
(12, 10, 'there', 6, 0),
(13, 11, 'Wassup?', 6, 0),
(13, 12, 'I', 0, 1),
(14, 11, 'AB', 0, 1),
(15, 11, 'AB', 0, 1),
(15, 12, 'I', 0, 0),
(16, 11, 'Wassup?', 6, 0),
(16, 12, 'yeh', 6, 0),
(14, 12, 'yeh', 6, 0),
(11, 1, 'aa', 2, 1),
(11, 2, '11', 0, 1),
(11, 3, '12', 0, 1),
(11, 4, '77', 3, 0),
(11, 5, 'A', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `answer_detail_subj`
--

CREATE TABLE `answer_detail_subj` (
  `sanswer_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL,
  `answer` varchar(10000) NOT NULL,
  `marks` decimal(10,0) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `marked_review` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_detail_subj`
--

INSERT INTO `answer_detail_subj` (`sanswer_id`, `sq_id`, `answer`, `marks`, `comments`, `marked_review`) VALUES
(9, 10000, 'inner, outer', '5', ':-)', 1),
(9, 10001, 'Protocols ', '5', ':)', 1),
(9, 10002, 'Private, public,protected,internal,protected internal', '1', 'elaborate', 0),
(11, 10000, 'qqq', '0', 'gud', 1),
(11, 10001, 'AAA', '0', 'OK', 1),
(11, 10002, 'CCC', '0', 'bad answer', 0),
(14, 10005, 'asa', '4', 'gud', 0),
(14, 10006, 'asa', '4', 'gud', 0),
(15, 10007, 'po', '0', 'poor answer', 1),
(15, 10008, 'po', '0', 'poor answer', 1),
(16, 10007, 'AQA', '4', 'ok', 1),
(17, 10007, 'AKA', '2', '', 1),
(17, 10008, 'WWW', '3', '', 1),
(18, 10007, 'WWW', '3', 'not upto the mark', 1),
(18, 10008, 'RR', '4', 'Gud', 0),
(12, 10000, 'CC', '0', '', 1),
(13, 10003, 'FO', '3', 'AVERAGE', 1),
(13, 10004, 'FU', '2', ':/', 1),
(10, 10000, 'oooooo', '3', '', 0),
(10, 10001, 'ooaaoa', '2', '', 0),
(10, 10002, 'D for Dog', '3', 'Very Good', 1),
(12, 10001, 'AA', '1', '', 0),
(12, 10002, 'ss', '2', 'Gud', 0),
(16, 10008, 'FO', '2', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_info`
--

CREATE TABLE `batch_info` (
  `batch_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL COMMENT 'trainer teaching this batch',
  `batch_name` varchar(50) NOT NULL COMMENT 'friendly name for batch',
  `batch_date` date NOT NULL COMMENT 'batch start date',
  `max_marks` int(11) NOT NULL COMMENT 'max marks paper for batch',
  `active` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_info`
--

INSERT INTO `batch_info` (`batch_id`, `trainer_id`, `batch_name`, `batch_date`, `max_marks`, `active`) VALUES
(1, 1, 'dotNET', '2015-10-12', 25, 1),
(2, 1, 'testing', '2015-10-12', 25, 0),
(3, 2, 'JAVA', '2015-10-12', 25, 1),
(4, 2, 'ADVJAVA', '2015-10-12', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_trainee`
--

CREATE TABLE `batch_trainee` (
  `batch_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `barred` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains mapping of trainees to particular batch';

--
-- Dumping data for table `batch_trainee`
--

INSERT INTO `batch_trainee` (`batch_id`, `trainee_id`, `barred`) VALUES
(1, 101, 1),
(1, 102, 0),
(1, 103, 0),
(1, 104, 0),
(1, 108, 0),
(1, 105, 0),
(1, 106, 0),
(1, 107, 0),
(3, 109, 0),
(3, 110, 0),
(3, 111, 0),
(3, 112, 0),
(3, 113, 0),
(1, 124, 0);

-- --------------------------------------------------------

--
-- Table structure for table `difficulty_levels`
--

CREATE TABLE `difficulty_levels` (
  `dlevel_id` int(11) NOT NULL,
  `level_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficulty_levels`
--

INSERT INTO `difficulty_levels` (`dlevel_id`, `level_name`) VALUES
(1, 'Easy'),
(2, 'Medium'),
(3, 'Difficult');

-- --------------------------------------------------------

--
-- Table structure for table `login_admin`
--

CREATE TABLE `login_admin` (
  `admin_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_admin`
--

INSERT INTO `login_admin` (`admin_id`, `passwd`, `name`, `profile_pic`, `active`) VALUES
(1, '1234', 'Amit', 'kg.jpg', 1),
(2, '67dfg', 'asdfg', 'qwe', 0),
(3, 'login@123', 'Kaushik', 'kaushik.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_test`
--

CREATE TABLE `master_test` (
  `test_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `paper_1` int(11) NOT NULL,
  `paper_2` int(11) NOT NULL,
  `main_paper` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `time_duration` int(11) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_test`
--

INSERT INTO `master_test` (`test_id`, `subject_id`, `paper_1`, `paper_2`, `main_paper`, `batch_id`, `status_id`, `time_duration`, `password`) VALUES
(1, 1, 1, 3, 1, 1, 3, 1000, '1234'),
(2, 2, 5, 6, 5, 1, 3, 1000, '2222'),
(3, 3, 2, 4, 2, 3, 1, 1000, '3333'),
(4, 4, 7, 8, 7, 3, 2, 2000, 'YYYY');

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE `paper` (
  `paper_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper`
--

INSERT INTO `paper` (`paper_id`, `trainer_id`) VALUES
(1, 1),
(3, 1),
(5, 1),
(6, 1),
(2, 2),
(4, 2),
(7, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `paper_status`
--

CREATE TABLE `paper_status` (
  `pstat_id` int(11) NOT NULL,
  `status` varchar(15) NOT NULL COMMENT 'paper approved,live,etc'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper_status`
--

INSERT INTO `paper_status` (`pstat_id`, `status`) VALUES
(1, 'Preparing'),
(2, 'SentforApproval'),
(3, 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `questions_obj`
--

CREATE TABLE `questions_obj` (
  `question_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `op_1` varchar(1000) NOT NULL,
  `op_2` varchar(1000) NOT NULL,
  `op_3` varchar(1000) NOT NULL,
  `op_4` varchar(1000) NOT NULL,
  `difficulty_level` int(11) NOT NULL,
  `answers` varchar(10000) NOT NULL COMMENT 'csv of options, which are answers of this question',
  `marks` int(11) NOT NULL COMMENT 'total mark of this question'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_obj`
--

INSERT INTO `questions_obj` (`question_id`, `question`, `op_1`, `op_2`, `op_3`, `op_4`, `difficulty_level`, `answers`, `marks`) VALUES
(1, 'choose app?', 'aa', 'bb', 'cc', 'dd', 1, 'aa', 2),
(2, 'give ans?', '11', '22', '33', '44', 3, '33', 2),
(3, 'choose', '12', '23', '34', '45', 2, '23', 2),
(4, 'give ans', '99', '88', '77', '66', 2, '77', 3),
(5, 'What!?', 'A', 'B', 'C', 'D', 3, 'D', 3),
(6, 'Who are You?', 'Me', 'You', 'Him', 'Her', 2, 'You', 4),
(7, 'What are You?', 'Dog', 'Cat', 'Buffalo', 'Cow', 2, 'Cat', 4),
(8, 'How Are you?', 'OK', 'NOT ok', 'Fine', 'Finest', 3, 'Finest', 4),
(9, 'Where are you?', 'here', 'there', 'not here', 'not there', 2, 'not here', 6),
(10, 'Where do u stay?', 'here', 'there', 'not here', 'not there', 3, 'there', 6),
(11, 'Yo Yo?', 'Honey Singh', 'Wassup?', 'AB', 'AIB', 1, 'Wassup?', 6),
(12, 'Hey?', 'yeh', 'Hi', 'H', 'I', 1, 'yeh', 6);

-- --------------------------------------------------------

--
-- Table structure for table `questions_subj`
--

CREATE TABLE `questions_subj` (
  `sq_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `marks` int(11) NOT NULL,
  `difficulty_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_subj`
--

INSERT INTO `questions_subj` (`sq_id`, `question`, `marks`, `difficulty_level`) VALUES
(10000, 'What are the different joins in SQL?', 5, 1),
(10001, 'What are the differences between WCF and Web Services?', 3, 2),
(10002, 'what are the different access modifiers in C#?', 5, 3),
(10003, 'Describe Stored procedures in SQL?', 7, 3),
(10004, 'what are the different types of styling in CSS?', 6, 2),
(10005, 'Explain WPF?', 6, 3),
(10006, 'Explain the page life cycle of ASP.NET?', 7, 1),
(10007, 'Differenciate the connected and disconnected mode in ADO.NET? ', 7, 2),
(10008, 'Difference between XML and XAML?', 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `trainee_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `total_marks` decimal(10,0) NOT NULL,
  `time_remaining` int(11) NOT NULL,
  `exam_completed` int(11) NOT NULL COMMENT '1- yes,0 - No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`trainee_id`, `test_id`, `paper_id`, `total_marks`, `time_remaining`, `exam_completed`) VALUES
(102, 1, 1, '11', 2000, 1),
(108, 1, 1, '8', 3000, 1),
(103, 1, 3, '-1', 2000, 1),
(105, 1, 1, '8', 1000, 1),
(104, 1, 1, '18', 2000, 1),
(104, 2, 6, '20', 2000, 1),
(102, 2, 5, '6', 3000, 1),
(103, 2, 5, '12', 1500, 1),
(108, 2, 5, '5', 2000, 1),
(105, 2, 5, '19', 2500, 1),
(124, 1, 1, '-1', 1000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subject_detail`
--

CREATE TABLE `subject_detail` (
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_detail`
--

INSERT INTO `subject_detail` (`subject_id`, `subject_name`) VALUES
(1, 'dotNET'),
(2, 'HTML'),
(3, 'PHP'),
(4, 'WCF');

-- --------------------------------------------------------

--
-- Table structure for table `test_question_obj`
--

CREATE TABLE `test_question_obj` (
  `paper_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_obj`
--

INSERT INTO `test_question_obj` (`paper_id`, `question_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(3, 6),
(3, 7),
(3, 8),
(6, 9),
(6, 10),
(5, 11),
(5, 12);

-- --------------------------------------------------------

--
-- Table structure for table `test_question_subj`
--

CREATE TABLE `test_question_subj` (
  `paper_id` int(11) NOT NULL,
  `sq_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_question_subj`
--

INSERT INTO `test_question_subj` (`paper_id`, `sq_id`) VALUES
(1, 10000),
(1, 10001),
(1, 10002),
(3, 10003),
(3, 10004),
(6, 10005),
(6, 10006),
(5, 10007),
(5, 10008);

-- --------------------------------------------------------

--
-- Table structure for table `trainee`
--

CREATE TABLE `trainee` (
  `trainee_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `institute` varchar(200) NOT NULL,
  `joining_date` date NOT NULL,
  `stream` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `admin_comment` varchar(1000) NOT NULL,
  `comment_visibility` tinyint(1) NOT NULL,
  `comment_date` date NOT NULL COMMENT 'date when comment last modified',
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainee`
--

INSERT INTO `trainee` (`trainee_id`, `passwd`, `name`, `institute`, `joining_date`, `stream`, `profile_pic`, `admin_comment`, `comment_visibility`, `comment_date`, `active`) VALUES
(101, 'aA', 'swapnil', 'gla university', '2015-10-12', 'cs', 'swap.jpg', 'Very good', 1, '2015-11-18', 1),
(102, 'bB', 'Arpitha', 'St Josephs', '2015-10-09', 'IS', 'arp.jpg', 'Good', 1, '2016-01-02', 0),
(103, 'cC', 'Shivani', 'SJEC', '2015-10-13', 'EC', 'shiv.jpg', 'Great work', 0, '2016-01-01', 1),
(104, 'dD', 'anjali', 'gla university', '2015-10-12', 'cs', 'anji.jpg', 'nice', 0, '2016-01-20', 0),
(105, 'EE', 'UB', 'SJEC', '2015-10-12', 'ECE', 'C:\\Users\\training\\Pictures\\index.jpg', 'Good', 1, '2015-11-16', 1),
(106, 'EE', 'Niharika', 'SJEC', '2015-10-12', 'CS', 'C:\\Users\\training\\Pictures\\index.jpg', 'HardWorker', 1, '2015-11-16', 1),
(107, 'ED', 'Manjappa', 'SJEC', '2015-10-12', 'ECE', 'C:\\Users\\training\\Pictures\\index.jpg', 'Super Human', 1, '2015-12-08', 1),
(108, 'RR', 'Kriti', 'SJEC', '2015-10-12', 'ECE', 'C:\\Users\\training\\Pictures\\index.jpg', 'Smart', 1, '2015-11-04', 1),
(109, 'EE', 'Tara', 'SJEC', '2015-10-12', 'ECE', 'C:\\Users\\training\\Pictures\\index.jpg', 'lazy', 1, '2015-11-16', 1),
(110, 'WW', 'Shawn', 'SJEC', '2015-10-12', 'MECH', 'C:\\Users\\training\\Pictures\\Index.jpg', 'Notorius', 1, '2015-11-24', 1),
(111, 'WW', 'Chris', 'SJEC', '2015-10-12', 'MECH', 'C:\\Users\\training\\Pictures\\Index.jpg', 'Smart', 1, '2015-11-14', 1),
(112, 'qq', 'Parvathi', 'APUC', '2015-10-12', 'CSE', 'C:\\Users\\training\\Pictures\\index.jpg', 'Good', 1, '2015-11-09', 1),
(113, 'qq', 'Elvis', 'APUC', '2015-10-12', 'ISE', 'C:\\Users\\training\\Pictures\\index.jpg', 'Good', 1, '2015-11-19', 1),
(123, 'swap', 'swapnil', 'gla', '0000-00-00', 'cs', '', 'good', 1, '0000-00-00', 1),
(124, 'NN', 'Rose', 'SJEC', '0000-00-00', 'ECE', 'C:UsersPublicPicturesSample PicturesKoala.jpg', 'lazy', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE `trainer` (
  `trainer_id` int(11) NOT NULL,
  `passwd` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ph_no` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`trainer_id`, `passwd`, `name`, `ph_no`, `email`, `profile_pic`, `active`) VALUES
(1, 'login@123', 'Ramnath', '8934572214', 'nishadramnath@gmail.com', 'C:Users	rainingPicturesimages.jpg', 1),
(2, 'testing@123', 'neha', '9877435633', 'neha22@gmail.com', 'neha.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD PRIMARY KEY (`sanswer_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD KEY `oq_id` (`oq_id`),
  ADD KEY `answer_id` (`answer_id`);

--
-- Indexes for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD KEY `sanswer_id` (`sanswer_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD PRIMARY KEY (`batch_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `batch_id_2` (`batch_id`),
  ADD KEY `trainee_id_2` (`trainee_id`);

--
-- Indexes for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  ADD PRIMARY KEY (`dlevel_id`);

--
-- Indexes for table `login_admin`
--
ALTER TABLE `login_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `master_test`
--
ALTER TABLE `master_test`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `paper_1` (`paper_1`),
  ADD KEY `paper_2` (`paper_2`),
  ADD KEY `batch_id` (`batch_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`paper_id`),
  ADD KEY `trainer_id` (`trainer_id`);

--
-- Indexes for table `paper_status`
--
ALTER TABLE `paper_status`
  ADD PRIMARY KEY (`pstat_id`);

--
-- Indexes for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD PRIMARY KEY (`sq_id`),
  ADD KEY `difficulty_level` (`difficulty_level`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD KEY `trainee_id` (`trainee_id`),
  ADD KEY `test_id` (`paper_id`),
  ADD KEY `test_id_2` (`paper_id`),
  ADD KEY `trainee_id_2` (`trainee_id`),
  ADD KEY `test_id_3` (`test_id`);

--
-- Indexes for table `subject_detail`
--
ALTER TABLE `subject_detail`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `sq_id` (`sq_id`);

--
-- Indexes for table `trainee`
--
ALTER TABLE `trainee`
  ADD PRIMARY KEY (`trainee_id`),
  ADD KEY `institute` (`institute`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`trainer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_obj`
--
ALTER TABLE `answers_obj`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `answers_subj`
--
ALTER TABLE `answers_subj`
  MODIFY `sanswer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `batch_info`
--
ALTER TABLE `batch_info`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `difficulty_levels`
--
ALTER TABLE `difficulty_levels`
  MODIFY `dlevel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_admin`
--
ALTER TABLE `login_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_test`
--
ALTER TABLE `master_test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `paper`
--
ALTER TABLE `paper`
  MODIFY `paper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `paper_status`
--
ALTER TABLE `paper_status`
  MODIFY `pstat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questions_obj`
--
ALTER TABLE `questions_obj`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `questions_subj`
--
ALTER TABLE `questions_subj`
  MODIFY `sq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10010;
--
-- AUTO_INCREMENT for table `subject_detail`
--
ALTER TABLE `subject_detail`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trainer`
--
ALTER TABLE `trainer`
  MODIFY `trainer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers_obj`
--
ALTER TABLE `answers_obj`
  ADD CONSTRAINT `answers_obj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_obj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answers_subj`
--
ALTER TABLE `answers_subj`
  ADD CONSTRAINT `answers_subj_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `answers_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

--
-- Constraints for table `answer_detail_obj`
--
ALTER TABLE `answer_detail_obj`
  ADD CONSTRAINT `answer_detail_obj_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answers_obj` (`answer_id`),
  ADD CONSTRAINT `answer_detail_obj_ibfk_2` FOREIGN KEY (`oq_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `answer_detail_subj`
--
ALTER TABLE `answer_detail_subj`
  ADD CONSTRAINT `answer_detail_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `answer_detail_subj_ibfk_2` FOREIGN KEY (`sanswer_id`) REFERENCES `answers_subj` (`sanswer_id`);

--
-- Constraints for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD CONSTRAINT `batch_info_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `batch_trainee`
--
ALTER TABLE `batch_trainee`
  ADD CONSTRAINT `batch_trainee_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `batch_trainee_ibfk_2` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`);

--
-- Constraints for table `master_test`
--
ALTER TABLE `master_test`
  ADD CONSTRAINT `master_test_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subject_detail` (`subject_id`),
  ADD CONSTRAINT `master_test_ibfk_2` FOREIGN KEY (`paper_1`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_3` FOREIGN KEY (`paper_2`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `master_test_ibfk_4` FOREIGN KEY (`batch_id`) REFERENCES `batch_info` (`batch_id`),
  ADD CONSTRAINT `master_test_ibfk_5` FOREIGN KEY (`status_id`) REFERENCES `paper_status` (`pstat_id`);

--
-- Constraints for table `paper`
--
ALTER TABLE `paper`
  ADD CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`trainer_id`);

--
-- Constraints for table `questions_obj`
--
ALTER TABLE `questions_obj`
  ADD CONSTRAINT `questions_obj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `questions_subj`
--
ALTER TABLE `questions_subj`
  ADD CONSTRAINT `questions_subj_ibfk_1` FOREIGN KEY (`difficulty_level`) REFERENCES `difficulty_levels` (`dlevel_id`);

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `result_ibfk_1` FOREIGN KEY (`trainee_id`) REFERENCES `trainee` (`trainee_id`),
  ADD CONSTRAINT `result_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `result_ibfk_3` FOREIGN KEY (`test_id`) REFERENCES `master_test` (`test_id`);

--
-- Constraints for table `test_question_obj`
--
ALTER TABLE `test_question_obj`
  ADD CONSTRAINT `test_question_obj_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
  ADD CONSTRAINT `test_question_obj_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions_obj` (`question_id`);

--
-- Constraints for table `test_question_subj`
--
ALTER TABLE `test_question_subj`
  ADD CONSTRAINT `test_question_subj_ibfk_1` FOREIGN KEY (`sq_id`) REFERENCES `questions_subj` (`sq_id`),
  ADD CONSTRAINT `test_question_subj_ibfk_2` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
