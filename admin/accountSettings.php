<?php
    //require_once("helper/checkLogin.php");
	require_once("helper/helper.php");
	
	if(!is_logged_in())
	{
		header("Location:login.php");
	}
	
	require_once("helper/header.php");
	require_once("helper/nav.php");
	
?>
<main>
               
               <div class="container">
  
               <!--  For Tab -->
<div class="col l3 z-depth-6 card-panel  blue-color" >
      <span class="text-darken-2" style="font-Size:190%">Manage Account</span>

                   </div>
               <!-- The form-->
                <div class="col l3 z-depth-6 card-panel">
    <form class="col l12">
    <!--    
      <div class="row">
        <div class="input-field col l6  ">
          <input id="first_name" type="text" class="validate">
          <label for="first_name">First Name</label>
        </div>
        </div>
       -->
         
         <div class="row">
        <div class="input-field col l6">
          <input id="old_password" type="password" class="validate">
          <label for="old_password">Old Password</label>
        </div>
        </div>
         
         <div class="row">
        <div class="input-field col l6">
          <input id="new_password" type="password" class="validate">
          <label for="new_password">New Password</label>
        </div>
        </div>
         
         <div class="row">
        <div class="input-field col l6">
          <input id="cnfirm_password" type="password" class="validate">
          <label for="cnfirm_password">Confirm new Password</label>
        </div>
        </div>
        </form> 
                    
                    
                   <!--Button -->
                   <button class="btn waves-effect blue-color" type="submit" name="action" id="submit_btn">Submit
  </button>
               </div><!-- container-->
 </div>
         
               <!-- container-->
</main>
<?php
     require_once("helper/footer.php");
?>