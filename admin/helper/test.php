<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}

	/*To retrive the test which are not taken by user 
	userid is input 
	returns the list of tests that are not yet taken by user
	*/
	function dal_getAllNotTakenTests($userId)
	{
		$traineename = array("Myrna", "Shruthi", "Reena");
		return $traineename; 
	}


	/* Retrive the ovbjective questions
	paper ID is the input here
	Returns the list of all objective questions as an array
	*/
	function dal_retrieveObjQuestion($testId)
	{
		//take main_paper from this particular testId
	    // connect to database, and get the connection handle
		$con = dbConnect();
	
		// forming a query
		$select_query = "SELECT main_paper FROM ".DB_NAME.".master_test where test_id = ".testId; // "." is for concatenation
		
		$result = mysqli_query($con,$select_query);
		
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
	            $result_set = mysqli_fetch_array($result);
				$main_paper = $result_set["main_paper"];						
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
			//fetch all the questionId s from "test_question_obj"
		     //we got our paperId from above querry, now fucking fetch the question id using below function
			 $questionId = array();
			 if(dal_retrieveQuestionId($main_paper)==false)
			 {
				 return false;
			 }
			 else
			 {
				$questionId = dal_retrieveQuestionId($main_paper);
			 }			 
			//using above questionId fetch question from question_obj
			$NoOfQues = count($questionId);
			$question = array();
			$y=0;
			for ($x = $NoOfQues; $x >= 0; $x--)
			{
				//the function which returns array with question and options
				question = dal_retrieveQuestion($QuestionId[$y]);
				$y++;
			}
		
		
	}
	
	function dal_retrieveQuestion($QuestionId)
	{
		//It returns all the questionID which belongs to the paper with paperId = paperId
		$select_query = "SELECT * FROM ".DB_NAME.".question_obj where question_id = ".QuestionId; // "." is for concatenation
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{		
				$question_list = array();
				$i=0;
				while($result_set = mysqli_fetch_array($result))
				{
					$question = array();
				
					$question["id"] = $result_set["question_id"];
					$question["text"] = $result_set["question"];
					$question["op1"] = $result_set["op_1"];
					$question["op2"] = $result_set["op_2"];
					$question["op3"] = $result_set["op_3"];
					$question["op4"] = $result_set["op_4"];
			
					// adding the admin_details ,to the admin array.		
					$question_list[$i] = $question;
					$i++;
			   
			}			
			return $question_list;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	

	function dal_retrieveQuestionId($paperId)
	{
		//It returns all the questionID which belongs to the paper with paperId = paperId
		$select_query = "SELECT question_id FROM ".DB_NAME.".test_question_obj where paper_id = ".paperId; // "." is for concatenation
		$result = mysqli_query($con,$select_query);
		if($result)
		{
			if(mysqli_num_rows($result) > 0)
			{
			
			
				$i=0;
				$question = array();
				while($result_set = mysqli_fetch_array($result))
				{
					$question[$i] = $result_set["question_id"];			
					$i++;	
				}					
			}			
			return $question;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	

	}
	
	/* To get subjective questions 
	Takes Paper Id as Input
	Returns the list of Question IDs(for that particular paper Id) as an array 
	*/
	function dal_getSubjectiveQuestions($paper_id)
	{
		$sq_id=array("101","102","103");
		return $sq_id;
	}

	
	/* To get Subjective Answers*
	Takes trainee Id and paper Id as input
	Returns the list of IDs of the subjective answer as an array
	*/
	function dal_getSubjectiveAnswers($trainee_id,$paper_id)
	{
		$sanswer_id=array("201","202","203");
		return $sanswer_id;
	}


	/* To get the Difficulty Level
	Takes no Input
	Return the three difficulty levels as an array
	*/
	function dal_getDifficultyLevel()
	{
		$DifficultyLevel=array("Easy","Medium","Difficult");
		return $DifficultyLevel;
	}


	/* To set the Objective questions
	Takes as input
	Returns 
		true: If Insert is succesfull
		false: If Insert is unsuccesful
	*/
	function dal_setObjectiveQuestions($ques,$op_1,$op_2,$op_3,$op_4,$difficultyLevel,$answer, $marks)
	{
		$bool_result=true;
		return $bool_result;
	}

	
	/* To set the subjective questions
	Takes as input
	Returns 
		true: If Insert is succesfull
		false: If Insert is unsuccesful
	*/
	function dal_setSubjectiveQuestions($ques,$marks,$difficultyLevel)
	{
		$bool_result=true;
		return $bool_result;
	}


	/* To change password of trainer
	Takes the trainer Id and the new password as input
	Returns 
		true: If Insert is succesfull
		false: If Insert is unsuccesful
	*/
	function dal_changePassword($newPassword, $trainerId, $oldPassword)
	{
		$newPwd=true;
		return $newPwd;
	}


	/* To set the subjective questions
	Takes as input
	Returns 
		True: If Insert is succesfull
		false: If Insert is unsuccesful
	*/
	function dal_changeProfilePicture($newPicture,$trainerId)
	{
		$newPic=true;
		return $newPic;
	}



	/* To get the paper status
	Takes paper Id as input
	Returns the status of the paper if confirmed or cancelled or in progress
	*/
	function dal_getPaperStatus($paperId)
	{
		$status="Confirmed";
		return $status;
	}


	/* To set the marks and comments for the subjective Answers
	Takes trainee Id, Paper Id, Answer Id, Mark assigned and the comments written as input
	Returns 
	true: If Insert is succesfull
	false: If Insert is unsuccesful
	*/
	function dal_setMarkAndComments($traineeId, $paperId, $answerID, $mark, $comment)
	{
		$setMarkAndComment=true;
		return $setMarkAndComment;
	}



	/* To get the subjective Answers
	Takes question Id, trainee Id and Paper Id as input
	Returns the subjective answer as a string
	*/
	function dal_getSubjectiveAnswer($quesId, $traineeID, $paperId)
	{
		$subj_answer="This is a subjective answer";
		return $subj_answer;
	}



	/* To get the Trainer Details
	Takes Trainer Id as input
	Returns 
		name, profile picture, contact number and email id as an array
	*/
	function dal_getTrainerDetail($trainerId)
	{
		$trainer_detail=array("Myrna","1.jpg","9665478921","myrna@xyz.com");
	}
	

	function dal_saveSubjectiveAnswer($answer_id,$trainee_id,$paper_id,$sq_id,$answer_id)
	{
		$sql = "INSERT INTO answers_subj(sanswer_id, trainee_id, paper_id)
                VALUES ('".$answer_id."','".$trainee_id"','".$paper_id"')";
	    
		
		if(mysqli_query($con,$sql))
		{
			
			$sql2="INSERT INTO answer_detail_subj (sanswer_id, sq_id, answer)
                VALUES ('".$answer_id."','".$sq_id"','".$answer"')";
				if(mysqli_query($con,$sql2))
				{
					return true;
				}
				else
				{
					return false;
				}
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	function dal_saveObjectiveAnswer($answer_id,$trainee_id,$paper_id,$oq_id,$answers)
	{
		$sql = "INSERT INTO answers_obj(answer_id, trainee_id, paper_id)
                VALUES ('".$answer_id."','".$trainee_id"','".$paper_id"')";
	    
		
		if(mysqli_query($con,$sql))
		{
			
			$sql2="INSERT INTO answer_detail_obj (answer_id, oq_id, answers)
                VALUES ('".$answer_id."','".$oq_id"','".$answers"')";
				if(mysqli_query($con,$sql2))
				{
					return true;
				}
				else
				{
					return false;
				}
			return true;
		}
		else
		{
			return false;
		}
	}
	}
	
?>