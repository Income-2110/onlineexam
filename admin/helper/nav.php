<?php  

   $logged_in = is_logged_in();
?>
<!-- top header -->
	<header>
    <nav>
          <div class="nav-wrapper blue lighten-2">
      <a href="#" class="brand-logo left" style="height:inherit;"><img src="images/itcLogoTransparent.png" style="height:inherit;"></a>
      <?php if($logged_in): ?>
      
	  <ul id="nav-mobile" class="right ">
        <li><a href="logout.php">Logout</a></li>
      </ul>
	  <?php endif ?>
    </div>
    </nav>
    </header>

<!-- use only for sidenavigation use -->
<!-- if logged in, show this side nav -->
	<style>
   .itcfoot
    {
        position:fixed;
        bottom:0;
        height: 88px;
        width:100%;  
    }
    </style>
<?php if($logged_in): ?>
  
	<style>
    header,container,footer {
      padding-left: 240px;
    }
	main {
		padding-left : 250px;
	}
	.itcfoot
    {
        position:relative;
        bottom:0;
        height: 88px;
        width:100%;
    }

    @media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
		
      }
    }
	
	.navLinks{
		font-size : 1.2em;
		color : white;
	}
	
    </style>
    
      <ul id="slide-out" class="side-nav fixed grey darken-4">
        <li style="margin-top:20px">  <div class="profile-userpic">
                                        <img src="assets\img\guy-5.jpg" class="img-responsive" alt="">
                                    </div></li>
          <li>
          <div class="profile-usertitle">
                                        <div class="profile-usertitle-name">
                                       
                                           <!--<h4><?php echo $_SESSION['name'] ?> </h4>-->
                                            
                                        </div>
                                        <div class="profile-usertitle-job navLinks">
                                            Manager
                                        </div>
                                    </div>
        </li>
      <li><a href="dashboard.php"><i class="glyphicon glyphicon-home navLinks"></i> <span class="navLinks">Dashboard</span></a></li>
      <li><a href="batchdata.php"><i class="glyphicon glyphicon-pencil navLinks"></i> <span class="navLinks">Batches</span></a></li>
	  <li><a href="add_trainer.php"><i class="glyphicon glyphicon-pencil navLinks"></i><span class="navLinks"> Add Trainer</span></a></li>
	   <li><a href="add_trainee.php"><i class="glyphicon glyphicon-pencil navLinks"></i><span class="navLinks"> Add Trainee</span></a></li>
	  <li><a href="add_batch.php"><i class="glyphicon glyphicon-pencil navLinks"></i> <span class="navLinks">Add Batches</span></a></li>
    <li><a href="assign_batch.php"><i class="glyphicon glyphicon-pencil navLinks"></i> <span class="navLinks">Add Trainees to Batches</span></a></li>
      <li><a href="accountSettings.php"><i class="glyphicon glyphicon-cog navLinks"></i><span class="navLinks"> Settings</span></a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
	
<?php endif ?>