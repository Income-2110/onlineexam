<?php
  /*
    This file will contain all the helper functions required.
  */
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");
  
  // require_once("helper/config.php");
  date_default_timezone_set("Asia/Kolkata");

/*
  Connects to database and returns the connection handle.
  Stops execution if database connection is not successful
*/
function dbConnect()
{
  $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASS,DB_NAME);
  if(mysqli_connect_errno())
   {
     die("Could not connect to Database"); 
   }
  return $con;
}

/*
This is just a sample function for an example.
This function will return all the details of all
the "admins" present in the database

Returns : False  - in case of query error
          array  - An array of all the admins in the database
		  
		  void  - If the query is successful and there are no admins.
*/
function get_admin_details()
{
	// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".login_admin"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  /* If query is successful.
	     Query is successful if it gets executed on the database server without any errors.
		 If no rows are present for the select query, then also it is successful, therefore
		 if we're expecting any results from a select query always check for number of rows
		 present, as shown below
	  */
		if(mysqli_num_rows($result) > 0)
		{
			// if there is atleast 1 row, fetching the data in an array
			/*
			   Here the $result_set is an associated array which holds the data present
			   in the selected row. The key for accessing the array values is same as that
			   of the name of the column in the database table.
			   
			   A while loop is used, to fetch each row one after the other. If there are 
			   more than one rows, then first call to "mysqli_fetch_array" will retrieve
			   1st row; on second iteration the 2nd row and so on till all the rows are read.
			   
			   If the select query is such that only 1 row is expected as output, then there is no need for the while loop.
			*/
			
			$admins = array(); // creating an admin array
            while($result_set = mysqli_fetch_array($result))
			{
			   $admin_detail = array();
			   
			   $admin_detail["id"] = $result_set["admin_id"];
			   $admin_detail["name"] = $result_set["name"];
			   $admin_detail["status"] = $result_set["active"];
			
               // adding the admin_details ,to the admin array.		
			   $admins[] = $admin_detail;
			   
			   /*
			      Here, we're creating an array inside an array.
				  The "admins" array is contains details of the all the admins.
				  admins[0] -> will refer to details of admin 0
				  admins[1] -> will refer to details of admin 1 .....and so on
				  
				  inside the admins[0], we have another array which contains its details.
				  So name of admins[0] can be accessed as, admins[0]["name"]
				  
			   */
			   
			}
			
			return $admins;
		}
		else
		{
		  // No rows present, which match the select query
		  return true;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}


function admin_login($username,$password)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
	 $username = mysqli_real_escape_string($con,$username);
	 $password = mysqli_real_escape_string($con,$password);

	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".login_admin WHERE admin_id ='$username' AND passwd='$password' "; 
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
		if(mysqli_num_rows($result) == 1)
		{
               // Correct details, login
               $result_set = mysqli_fetch_array($result);
			   $user_id = $result_set["admin_id"];
			   $name= $result_set["name"];
			   $profile_pic= $result_set["profile_pic"];
			   $status=$result_set["active"];
			   
			   
			   session_start();
			   $_SESSION['user_id'] = $user_id;
			   $_SESSION['name'] = $name;
			   $_SESSION['profile_pic'] = $profile_pic;
			   $_SESSION['status'] = $status;
			   $_SESSION['type'] = "Admin";
			   
			   return true;
	    }
		else
		{
			return false;
		}
	
  }
   else
	{
		return false;
	}
}

/*
  checks if the given password matches the current userid's password 
  in the database
  Returns true if yes
  False if the given password do not match
*/
function check_pass($oldPass)
{
	// connect to database, and get the connection handle
	$con = dbConnect();
	$oldPass = mysqli_real_escape_string($con,$oldPass);
	 
	 if(!isset($_SESSION))
	 {
		 session_start();
	 }
	 
	 //checking if old password matches with the database
	 $userid = $_SESSION['user_id'];
	 $check_query="SELECT * FROM ".DB_NAME.".login_admin WHERE passwd='$oldPass' AND admin_id=$userid";
	 $result = mysqli_query($con,$check_query);
	 if($result)
	 {
		 if(mysqli_num_rows($result) == 1)
		 {
			 return true;
		 }
		 else
		 {
		   return false;
		 }
	 }
	 else
	 {
		 return false;
	 }
}
/*

This function changes the admin's password to the given password.
Returns true on success and false on failure
*/
function changePass($newPass)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
	 $newPass = mysqli_real_escape_string($con,$newPass);
	 
	 if(!isset($_SESSION))
	 {
		 session_start();
	 }
	 
	$userid = $_SESSION['user_id'];
	
	$update_query = "UPDATE ".DB_NAME.".login_admin SET passwd='$newPass' WHERE admin_id='$userid'";
	
	$result = mysqli_query($con,$update_query);	
	if($result)
	{
        //session_start();
	   $_SESSION['new_Pass'] = $newPass;
	   return true;
    }
   else
	{
		return false;
	}
}

/*
   starting a secure session. By default new session id is not generated
   Input: bool = (if true -> new session id generated else not )
          defaults to false
   
*/
function my_session_start($del_old_session=false)
{
  // session.use_strict_mode  - reject invalid session cookies and give new cookie	
  ini_set( "session_use_strict_mode" , 1);
  ini_set( "session_cookie_httponly" , 1);  
  // default is 1, so no need to set it again, otherwise use the below setting
  //ini_set('session.use_only_cookies', 1);
  
  $session_lifetime = 300 ; //5 minutes
  session_name("fs"); // inseated of PHPSESSID
  
  /*
   This is also not working
  
  session_set_cookie_params(time()+$session_lifetime,$path='/',$httponly=true);   // setting default parameters for session cookie 
  */
  
  session_start();
  
  // generating new session id ,use it after session start
  if($del_old_session)
  {	  
    session_regenerate_id(true); // generate new session id, deleting the old one
  }  
}

/*
Call only after setting the sessions yourself
Current usage, in the login script after setting the session variables

IMP: Assumes session is started
*/
function set_cookies()
{
    $user_id = $_SESSION['user_id'];   // user id of logged in user
    $name    = $_SESSION['name'];  // name of logged in user
    $profile_pic = $_SESSION['profile_pic'];  // prfile pic address of user
	$user_hash  = $_SESSION['user_hash'];  // user hash of logged in user
	$support_session_id = $_SESSION['support_session_id'];  // fs1 extra session cookie
	$security_hash = $_SESSION['security_hash'];  // fs2 hash of all the values sent in cookies

	setcookie("uid", $user_id,$expire=time()+60*5,$path="/");
    setcookie("uname",$name,$expire=time()+60*5,$path="/");
    setcookie("profile_pic",$profile_pic,$expire=time()+60*5,$path="/");
    setcookie("user_hash",$user_hash,$expire=time()+60*5,$path="/");
	setcookie("fs2",$security_hash,$expire=time()+60*5,$path="/");
	
	/* for some reason httponly parameter in setcookie is not working and is 
	   not being sent in subsequent requests. Therefore the folowing is not 
	   working and has been disabled.
	   setcookie("fs1",$support_session_id,$expire=time()+60*5,$httponly = true);
	   
	   When session_regenrate_id function  is used, only then http pnly cookies are being sent
	   
	*/
	setcookie("fs1",$support_session_id,$expire=time()+60*5,$path="/");
}

/*
   storing password in encrypted form
   $pass = password_hash(cleanMysql($con,$_POST['pass']),PASSWORD_BCRYPT);

*/
function loginAuthenticate($con,$email,$pass)
{
  $q = "SELECT * FROM ".DB_NAME.".login WHERE email='$email'";
  if($qf = mysqli_query($con,$q))
  {
    $res = mysqli_fetch_array($qf);
    $pass_check = $res['password'];
    if(password_verify($pass,$pass_check))
    {
      /*if(!isset($_SESSION))
      {
        my_session_start();  //start a new session, generate new id
      }
	  */
	  	  
	  // setting session values
      $t = $res['id'];
      $_SESSION['user_id']=$t;  //userid
	  $_SESSION['user_name'] = $res['username'];  // username
	  $_SESSION['name'] = $res['name'];
	  $_SESSION['user_hash'] = $res['hash_id'];  // unique user hash
	  $_SESSION['profile_pic'] = $res['imgsrc']; // image source	  
	  
	  // to hide the use of crypt use, as in the hash salt and other info is present
	  // support session_id and hash will also be sent as cookies and verfied on return for integrity
	  
	  $_SESSION['support_session_id'] = md5( crypt($res['username'].time(), "MYsalt" ));
      
	  // hash of the values sent in cookies
	  $cookie_values_sent = $res['name'].$res['id'].$res['imgsrc'].$res['hash_id'].$_SESSION['support_session_id'] ;
	  $_SESSION['security_hash'] = md5(crypt($cookie_values_sent , "MySalt2" ));
	  
	  // setting all the cookies
	  set_cookies();

      return true;
    }
    else
    return false;
  }
  else
  {
   return false;
  }
  return false;
}


function logged_in()
{
  if(!isset($_SESSION))
   {
     my_session_start(); // don't create a new session
   }
   
   // verify session information and cookie information
   
   // retrieving cookie information
   if( !isset($_COOKIE['uid']) || !isset($_COOKIE['uname']) || !isset($_COOKIE['profile_pic']) || !isset($_COOKIE['user_hash']) || !isset($_COOKIE['fs1']) || !isset($_COOKIE['fs2']) )
   {
  //   session_regenerate_id(true);
       session_unset();
	   return false;  // invalid session , cookies tampered
   }
 
   // checking session variables 
   if( !isset($_SESSION['user_id']) || !isset($_SESSION['name']) || !isset($_SESSION['profile_pic']) || !isset($_SESSION['user_hash']) || !isset($_SESSION['support_session_id']) || !isset($_SESSION['security_hash']) )
   {
    // session_regenerate_id(true);
	   session_unset();
	   return false; // invalid session , 
   }

   // retrieving cookie values
   $uid = $_COOKIE['uid'];
   $name = $_COOKIE['uname'];
   $profile_pic = $_COOKIE['profile_pic'];
   $user_hash = $_COOKIE['user_hash'];
   $support_session_id = $_COOKIE['fs1'];
   $security_hash = $_COOKIE['fs2'];
   
   // checking integrity of cookie
   $cookie_values_sent = $name.$uid.$profile_pic.$user_hash.$support_session_id ;
   $check_hash =  md5(crypt($cookie_values_sent , "MySalt2" ));
   
   if($check_hash != $security_hash )
   {
     //session_regenerate_id(true);
	   session_unset();
	   return false;
   }
   
   // extracting session variables
   $s_uid = $_SESSION['user_id'];
   $s_name = $_SESSION['name'];
   $s_profile_pic = $_SESSION['profile_pic'];
   $s_user_hash = $_SESSION['user_hash'];
   $s_support_session_id = $_SESSION['support_session_id'];
   $s_security_hash = $_SESSION['security_hash'];
   
   // comparing cookie and session values
   if($uid==$s_uid && $name==$s_name && $profile_pic==$s_profile_pic && $user_hash==$s_user_hash && $support_session_id==$s_support_session_id && $security_hash==$s_security_hash)
   {
	  // user logged in  
	  set_cookies();
 	  return $uid;  // user id of logged in user
   }
  else
  {
   // session_regenerate_id(true);
     session_unset();
     return false; // not logged in or invalid session
  }

}

//add trainee function
function add_trainee($traineeId)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $traineeId = mysqli_real_escape_string($con,$traineeId);
	  
	 //checking if old password matches with the database
	 $insert_query="INSERT INTO ".DB_NAME.".trainee (trainee_id,passwd,name,institute,joining_date,stream,profile_pic,admin_comment,comment_visibility,comment_date,active) VALUES ($traineeId,'','','','','','','This is comment field',0,'',1)";
	 //echo $insert_query;
	 if(mysqli_query($con,$insert_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}

//add trainer function

function add_trainer($trainerName, $trainerPh, $trainerEmail)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $trainerName = mysqli_real_escape_string($con,$trainerName);
	 $trainerPh = mysqli_real_escape_string($con,$trainerPh);
	 $trainerEmail = mysqli_real_escape_string($con,$trainerEmail);
	  
	 //checking if old password matches with the database
	 $insert_query="INSERT INTO ".DB_NAME.".trainer (passwd,name,ph_no,email,profile_pic,active) VALUES ('login@123','$trainerName','$trainerPh','$trainerEmail','',1)";
	
	 if(mysqli_query($con,$insert_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}

//retrieve batch id
function get_batchid()
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT batch_id,active FROM ".DB_NAME.".batch_info order by batch_date desc"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  /* If query is successful.
	     Query is successful if it gets executed on the database server without any errors.
		 If no rows are present for the select query, then also it is successful, therefore
		 if we're expecting any results from a select query always check for number of rows
		 present, as shown below
	  */
		if(mysqli_num_rows($result) > 0)
		{

			$batch = array(); // creating an batch array
			
            while($result_set = mysqli_fetch_array($result))
			{
				$batches=array();
				$batches["batch_id"]=$result_set["batch_id"];
				$batches["active"]=$result_set["active"];
				$batch[]=$batches;
			}
			
			return $batch;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}


function retrieve_batchDataByid($bid)
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".batch_info where batch_id='$bid'"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  
		if(mysqli_num_rows($result) > 0)
		{
			
			$batch_detail = array(); // creating an batch array
            $result_set = mysqli_fetch_array($result);
			{

			   $batch_detail["batch_id"] = $result_set["batch_id"];
			   $batch_detail["trainer_id"] = $result_set["trainer_id"];
			   $batch_detail["batch_name"] = $result_set["batch_name"];
			   $batch_detail["batch_date"] = $result_set["batch_date"];
			   $batch_detail["max_marks"] = $result_set["max_marks"];   
			}
			
			return $batch_detail;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}
function retrieve_trainerDataByid($trainer_id)
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".trainer where trainer_id='$trainer_id'"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  
		if(mysqli_num_rows($result) > 0)
		{
			
			$trainer_detail = array(); // creating an batch array
            $result_set = mysqli_fetch_array($result);
			{

			   $trainer_detail["trainer_id"] = $result_set["trainer_id"];
			   $trainer_detail["name"] = $result_set["name"];
			   $trainer_detail["ph_no"] = $result_set["ph_no"];
			   $trainer_detail["profile_pic"] = $result_set["profile_pic"];
			   $trainer_detail["email"] = $result_set["email"];   
			   $trainer_detail["active"] = $result_set["active"];
			}
			
			return $trainer_detail;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}

function retrieve_traineeDataByBatchid($bid)
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".batch_trainee where `batch_id`='$bid'"; // "." is for concatenation
	
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  
		if(mysqli_num_rows($result) > 0)
		
		{
			
			$trainee = array(); // creating an trainee array
           while( $result_set = mysqli_fetch_array($result))
			{
				
               $trainee_detail=array();
			   $trainee_detail["trainee_id"] =$result_set["trainee_id"];
               $trainee_detail["barred"] =$result_set["barred"];
			   //print_r($trainee_detail);
			   $trainee[]=$trainee_detail;
			}
			//print_r($trainee);
			return $trainee;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}

function retrieve_traineeDetailsByid($trainee_id)
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".trainee where trainee_id='$trainee_id'"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{
	  
		if(mysqli_num_rows($result) > 0)
		{
			
			$trainee_detail = array(); // creating an batch array
            $result_set = mysqli_fetch_array($result);
			{
			   $trainee_detail["trainee_id"] = $result_set["trainee_id"];
			   $trainee_detail["name"] = $result_set["name"];
			   $trainee_detail["institute"] = $result_set["institute"];
			   $trainee_detail["joining_date"] = $result_set["joining_date"];
			   $trainee_detail["profile_pic"] = $result_set["profile_pic"];   
			   $trainee_detail["stream"] = $result_set["stream"];
			   $trainee_detail["admin_comment"] = $result_set["admin_comment"];
			   $trainee_detail["comment_date"] = $result_set["comment_date"];
			   $trainee_detail["active"] = $result_set["active"];
				   
			}
			
			return $trainee_detail;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}

//barred trainee from test

function barred_trainee($traineeId,$batchid)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $traineeId = mysqli_real_escape_string($con,$traineeId);
	 $batchid = mysqli_real_escape_string($con,$batchid);
	 $check_query="select barred from ".DB_NAME.".batch_trainee where trainee_id='$traineeId' AND `batch_id`='$batchid'";
	 $result = mysqli_query($con,$check_query);
	if($result)
	{
		if(mysqli_num_rows($result) > 0)
		{
         $result_set = mysqli_fetch_array($result);
		 if($result_set["barred"]==0)
		 {
			 $value=1;
		 }
		 else
		 {
			 $value=0;
		 }
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	 $update_query="UPDATE ".DB_NAME.".batch_trainee set `barred`='$value' where `trainee_id` = '$traineeId' AND `batch_id`='$batchid'";
	
	 if(mysqli_query($con,$update_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}



// allowing trainee


function allowed_trainee($traineeId)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $traineeId = mysqli_real_escape_string($con,$traineeId);
	// $batchid = mysqli_real_escape_string($con,$batchid);
	$check_query="select active from ".DB_NAME.".trainee where trainee_id='$traineeId'";
	 $result = mysqli_query($con,$check_query);
	if($result)
	{
		if(mysqli_num_rows($result) > 0)
		{
         $result_set = mysqli_fetch_array($result);
		 if($result_set["active"]==0)
		 {
			 $value=1;
		 }
		 else
		 {
			 $value=0;
		 }
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	 $update_query="UPDATE ".DB_NAME.".trainee set `active`='$value' where `trainee_id` = '$traineeId'";
	
	 if(mysqli_query($con,$update_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}


//allowing trainer



function allowed_trainer($trainerId)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $trainerId = mysqli_real_escape_string($con,$trainerId);
	// $batchid = mysqli_real_escape_string($con,$batchid);
	$check_query="select active from ".DB_NAME.".trainer where trainer_id='$trainerId'";
	 $result = mysqli_query($con,$check_query);
	if($result)
	{
		if(mysqli_num_rows($result) > 0)
		{
         $result_set = mysqli_fetch_array($result);
		 if($result_set["active"]==0)
		 {
			 $value=1;
		 }
		 else
		 {
			 $value=0;
		 }
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	 $update_query="UPDATE ".DB_NAME.".trainer set `active`='$value' where `trainer_id` = '$trainerId'";
	
	 if(mysqli_query($con,$update_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}

//allowing batch

function allowed_batch($bId)
{
	
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $bId = mysqli_real_escape_string($con,$bId);
	// $batchid = mysqli_real_escape_string($con,$batchid);
	$check_query="select active from ".DB_NAME.".batch_info where batch_id='$bId'";
	
	 $result = mysqli_query($con,$check_query);
	if($result)
	{
		if(mysqli_num_rows($result) > 0)
		{
         $result_set = mysqli_fetch_array($result);
		 if($result_set["active"]==0)
		 {
			 $value=1;
		 }
		 else
		 {
			 $value=0;
		 }
		}
		else
		{   
			return false;
		}
	}
	else
	{    
		return false;
	}
	 $update_query="UPDATE ".DB_NAME.".batch_info set `active`='$value' where `batch_id` = '$bId'";
	
	 if(mysqli_query($con,$update_query))
	 {
		 return true;
	 }

   else
	{   
         
		return false;
	}
}

//updating new comment
function add_comment($traineeId,$comment)
{
	
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $traineeId = mysqli_real_escape_string($con,$traineeId);
	  $comment = mysqli_real_escape_string($con,$comment);
	  //echo $comment;
	 // echo $traineeId;
	 $update_query="UPDATE ".DB_NAME.".trainee set `admin_comment`='$comment' where `trainee_id`='$traineeId'";
	
	 if(mysqli_query($con,$update_query))
	 {
		 return true;
	 }

   else
	{   
         
		return false;
	}
}


//add batch function
function add_batch($batchName, $batchDate, $batchMarks,$batchTrainer)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $batchName = mysqli_real_escape_string($con,$batchName);
	 $batchDate = mysqli_real_escape_string($con,$batchDate);
	 $batchMarks = mysqli_real_escape_string($con,$batchMarks);
	 $batchTrainer = mysqli_real_escape_string($con,$batchTrainer);
	  
	 //checking if old password matches with the database
	 $insert_query="INSERT INTO ".DB_NAME.".batch_info (trainer_id,batch_name,batch_date,max_marks,active) VALUES ('$batchTrainer','$batchName','$batchDate','$batchMarks',1)";
	
	 if(mysqli_query($con,$insert_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}

/*
  Gives all the active batches for assignment
*/
function get_activeBatches()
{
// connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	// active = 1 (batch is active)
	// actvie = 0 ( batch is inactive)
	$select_query = "SELECT * FROM ".DB_NAME.".batch_info WHERE active=1 order by batch_id desc"; // "." is for concatenation
	$result = mysqli_query($con,$select_query);
	
	
	if($result)
	{

		if(mysqli_num_rows($result) > 0)
		{

			$batches = array(); // creating an batch array
			$i = 0;
            while($result_set = mysqli_fetch_array($result))
			{
				$batch=array();
				$batch["batch_id"]=$result_set["batch_id"];
				$batch["name"] = $result_set["batch_name"];
				$batch["date"] = $result_set["batch_date"]; // timestamp
				$batches[]=$batch;
			}
			return $batches;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}


//assign batches
function assign_batch($traineeId,$batch_id)
{
	// connect to database, and get the connection handle
	$con = dbConnect();

	 // scrubbing user input
 	 $traineeId = mysqli_real_escape_string($con,$traineeId);
	 $batchId = mysqli_real_escape_string($con,$batch_id);
	  
	 //checking if old password matches with the database
	 $insert_query="INSERT INTO ".DB_NAME.".batch_trainee (batch_id,trainee_id,barred) VALUES ($batchId,$traineeId,0)";
	 
	 if(mysqli_query($con,$insert_query))
	 {
		 return true;
	 }

   else
	{
		return false;
	}
}


function get_trainers_list()
{
    // connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT * FROM ".DB_NAME.".trainer order by trainer_id desc";
	
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{

		if(mysqli_num_rows($result) > 0)
		{

			$trainers = array(); // creating an batch array
			$i = 0;
            while($result_set = mysqli_fetch_array($result))
			{
				$t=array();
				$t["id"]=$result_set["trainer_id"];
				$t["name"] = $result_set["name"];
				
				$trainers[]=$t;
			}
			return $trainers;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}

//retrieving data for chart institute wise

function chart_institute()
{
    // connect to database, and get the connection handle
	$con = dbConnect();
	
	// forming a query
	$select_query = "SELECT result.test_id as test_id, avg(result.total_marks) as avg_marks,trainee.institute as institute FROM ".DB_NAME.".result inner join ".DB_NAME.".trainee on(".DB_NAME.".result.trainee_id=".DB_NAME.".trainee.trainee_id)group by ".DB_NAME.".trainee.institute";
	
	$result = mysqli_query($con,$select_query);
	
	if($result)
	{

		if(mysqli_num_rows($result) > 0)
		{

			$marks = array(); // creating an batch array
			//$i = 0;
            while($result_set = mysqli_fetch_array($result))
			{
				$t=array();
				$t["test_id"]=$result_set["test_id"];
				$t["avg_marks"] = $result_set["avg_marks"];
				$t["institute"] = $result_set["institute"];
				$marks[]=$t;
			}
			return $marks;
		}
		else
		{
		  // No rows present, which match the select query
		  return false;
		}
	}
	else
	{
		/* If the query fails. 
		   Query fails if the query syntax is wrong or the connection is closed due to some
           network error		   
		*/
		return false;
	}
	
}

function is_logged_in()
{
	if(!isset($_SESSION))
    {
     session_start();
    }
	
	 if(isset($_SESSION['user_id']))
	 {
		 $user_id = $_SESSION['user_id'];
		 return $user_id;
	 }
	 
	 else
	 {
		 return false;
	 }
}


?>