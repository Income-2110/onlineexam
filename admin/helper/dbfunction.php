
<?php
/*
Functions needed by the admin
This function will return the name and profile pic of the searched "trainer"
false if query is wrong or trainer searched is not present in the database
*/
function search_trainer($trainer_id)
{}
/*
This function is used to check whether trainer is available or left the company
*/
function disable_trainer($trainer_id)
{}
/*
This function will return the name and profile pic of the searched "trainee"
false if query is wrong or trainee searched is not present in the database
*/
function search_trainee($trainee_id)
{}
/*
This function is used to check whether trainee is available or left the company
*/
function disable_trainee($trainee_id)
{}
/*
This function will return the name and profile pic of the trainees present in the searched "batch"
false if query is wrong or batch searched is not present in the database
*/
function search_batch($batch_id)
{}
/*
This function is used to check whether batch is currently active or not
*/
function disable_batch($batch_id)
{}
/*
This function is used to reset the data of admin,trainer and trainee
false if query is wrong or when credentials are not correct
*/
function reset_data($usertype,$uid)
{}
/*
This function is used to verify that the test paper is approved by the admin or not
If paper is approved than make it main paper
*/
function approve_test($paper_id)
{}
/*
This function is used to display the result of the trainee batch wise
*/
function trainee_result($batch_id,$trainee_id)
{}
/*
This function is used to check whether the trainee is allowed to give the test or not
*/
function allowed_test($test_id,$trainee_id)
{}
?>