<?php
  /*
    Database config file - This file will contains the database login details
  */
 
  //database name 
  define("DB_NAME","onlineexam");
  //database password
  define("DB_PASS","");
  //server name
  define("DB_SERVER","localhost");
  //username
  define("DB_USERNAME","root");

  //site link  
  define("MAINPATH", "localhost/onlineexam/");
  
  define("MAINPATHADMIN","/home/username/public_html/");
  
?>