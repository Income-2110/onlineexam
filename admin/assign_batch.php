<?php

  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
     require_once("helper/header.php");
	 require_once("helper/nav.php");
	 
	 $batches = get_activeBatches();
?>
<style>
.select-wrapper input.select-dropdown {font-size: larger; padding-left:-10%;}
  </style>
  <main>
  
	<div class="container">
	<div class="col l3 z-depth-1 card-panel  blue-color" >
	<h1>Add Trainees to Batches</h1>
	</div>
	<div class="col l3 z-depth-6 card-panel">
	<div class="input-field col s12">
	Select Batch:<select  id="selectbatchId">
	         <?php foreach($batches as $batch) { ?>
					<option value="<?php echo $batch["batch_id"]; ?>">
					<?php echo $batch["name"]." - ".$batch["date"]; ?></option>
			 <?php } ?>
				</select>
				</div>
	
	Enter PSID of Trainees<input id="traineeLst" type="text" required />
	<br>
	<button type="submit" id="assign_batches">Assign Batch</button></br>
	</div>
	</div>
	</main>
<?php
     require_once("helper/footer.php");
?>