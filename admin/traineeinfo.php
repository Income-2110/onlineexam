<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
	require_once("helper/header.php");
	require_once("helper/nav.php");
$trainee_id=$_GET['traineeid'];
$trainee_details=retrieve_traineeDetailsByid($trainee_id);
?>
<main>
<!--styling of toggle -->
    <style>
    .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    }
    .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    }
    .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    }
    .switch-label:before, .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    }
    .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
    }
    .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label {
    background: #E1B42B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
    }
    .switch-input:checked ~ .switch-label:before {
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label:after {
    opacity: 1;
    }
    .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
    }
    .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
    }
    .switch-input:checked ~ .switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
    }
     
    /* Transition
    ========================== */
    .switch-label, .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
    }
    </style>
<div>
<h1>Trainee Details</h1>
<span>Id : <?php echo $trainee_details["trainee_id"];?></span><br/><br/>
<span>Name : <?php echo $trainee_details["name"];?></span><br/><br/>
<span>Institute : <?php echo $trainee_details["institute"];?></span><br/><br/>
<span>Profile Pic : <?php echo $trainee_details["profile_pic"];?></span><br/><br/>
<span>Joining Date : <?php echo $trainee_details["joining_date"];?></span><br/><br/>
<span>Stream : <?php echo $trainee_details["stream"];?></span><br/><br/>
<span>Admin Comment : <span id="comment_text"><?php echo $trainee_details["admin_comment"];?></span>
<div><input type="button" id="btn_edit" value="Edit" width="80px" height="60px"/>
<input class="trainee_update_btn_class" type="button" id="btn_update_<?php echo $trainee_details["trainee_id"];?>" value="Update" width="80px" height="60px" style="display:none;"/>
</div><br/><br/>
<div><textarea id="comment_textarea" rows="10" cols="25" style="width:100px; display:none;" ></textarea></div>
</span><br/><br/>
<span>Comment Date : <?php echo $trainee_details["comment_date"];?></span><br/><br/>
<span>
<label class="switch switch-left-right">
    <input id="traineeallowingToggle_<?php echo $trainee_details["trainee_id"];?>" class="switch-input traineeallowingToggle"  type="checkbox" <?php if($trainee_details["active"]==1){ echo "checked=\"checked\"";} ?> />
    <span class="switch-label" data-on="Enable" data-off="Disable"></span>
    <span class="switch-handle"></span>
     </label><br/><br/>
	 </span>
</div>
</main>
<?php
require_once("helper/footer.php");
?>