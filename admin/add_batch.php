<?php

  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
     require_once("helper/header.php");
	 require_once("helper/nav.php");
	 $trainers = get_trainers_list();
?>
  <main>
	<div class="container">
	<div class="col l3 z-depth-1 card-panel  blue-color" >
	<h1>Add Batch Details</h1>
	</div>
	<div class="col l3 z-depth-6 card-panel">
	Batch Name :<input  id="new_batch_name" type="text" required/><br>
	Batch Start Date:<input id="new_batch_date" type="date" required><br>	
	Max Marks<input id="new_batch_marks" type="text" required /><br>

	Select Trainer:
    <select name="selected_trainer" id="select_trainers_list">
    <?php foreach($trainers as $trainer) { ?>
					<option value="<?php echo $trainer["id"]; ?>">
					<?php echo $trainer["name"]; ?></option>
			 <?php } ?>
    </select>
	<br>
	<button type="submit" id="add_batch_btn">Add Batch</button></br>
	</div>
	</div>
	</main>
<?php
     require_once("helper/footer.php");
?>