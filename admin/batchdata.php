<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
    require_once("helper/header.php");
	require_once("helper/nav.php");
?>
<main>
<div>
<h1>BATCHES</h1>
<br/><br/>
  </div>
  <!--styling of toggle -->
    <style>
    .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    }
    .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    }
    .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    }
    .switch-label:before, .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    }
    .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
    }
    .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label {
    background: #E1B42B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
    }
    .switch-input:checked ~ .switch-label:before {
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label:after {
    opacity: 1;
    }
    .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
    }
    .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
    }
    .switch-input:checked ~ .switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
    }
     
    /* Transition
    ========================== */
    .switch-label, .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
    }
    </style>
<?php
$batchids=get_batchid();
?>
  <style>
    .barredtabs
        {
            overflow:hidden;
            
        }
        
         .profile-pic{
                    height: 50px;
                    width: 50px;
             border-radius:50%;
                }
        
    </style>
    <div class="container-fluid">
      <div class="row">
        <div class="panel panel-default col l6">
		<!--headings--->
        <div class="col l12">
          <ul class="tabs barredtabs">
            <li class="tab col l3"><a class="active" href="#activeBatches">Active batches</a></li>
            <li class="tab col l3"><a  href="#inactiveBatches">Inactive Batches</a></li>
          </ul>
        </div>          
    <div id="activeBatches" class="col s12">
	
      <div id="active_inside_div" class="panel panel-default col l12">
           <ul class="list-group">
             <?php
	      foreach($batchids as $key=>$value) { if ($value["active"]==1){
	  ?>
	  
		<li id="active_listitem_<?php echo $value["batch_id"]?>" class="list-group-item rank_list">
		
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                            <img class="media-object profile-pic" src="images/batch-icon.JPG" alt="Batch Icon">
                            </a>
                        </div>

                    <div class="media-body">
                       <h4 class="media-heading col l6"><a href="batchinfo.php?bid=<?php if($value["active"]==1){echo $value["batch_id"];}?>"><?php if($value["active"]==1){ echo "Batch ".$value["batch_id"];}?></a>
					   </h4>
                        
						<div class="col l6">
						
						<label class="switch switch-left-right col l6">
					
    <input id="batchallowingToggle_<?php echo $value["batch_id"] ;?>" class="switch-input batchallowingToggle"  type="checkbox" <?php if($value["active"]==0){ echo "checked=\"checked\"";} ?> />
    <span class="switch-label" data-on="Allow" data-off="Deny"></span>
    <span class="switch-handle"></span>
     </label>
						</div>
						
						
                    </div>
                <br/>
            </div>
          </li>
		  <?php }}?>
          </ul>
        </div>
		
      
    </div>
            
            
    <div id="inactiveBatches" class="col s12">
              <div id="inactive_inside_div"  class="panel panel-default col l12">
                  

         

      <ul class="list-group">
	  
	  <?php
	      foreach($batchids as $key=>$value) { if($value["active"]==0) {
	  ?>
		<li id="inactive_listitem_<?php echo $value["batch_id"]?>" class="list-group-item rank_list">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                            <img class="media-object profile-pic" src="images/batch-icon.JPG" alt="Batch Icon">
                            </a>
                        </div>

                    <div class="media-body">
                       <h4 class="media-heading col l6"><a href="batchinfo.php?bid=<?php if($value["active"]==0){ echo $value["batch_id"]; }?>"><?php if($value["active"]==0){ echo "Batch ".$value["batch_id"];} ?></a>
					   </h4>
                        
						<div class="col l6">
						<label class="switch switch-left-right col l6">
    <input id="batchallowingToggle_<?php echo $value["batch_id"] ;?>" class="switch-input batchallowingToggle"  type="checkbox" <?php if($value["active"]==0){ echo "checked=\"checked\"";} ?> />
    <span class="switch-label" data-on="Allow" data-off="Deny"></span>
    <span class="switch-handle"></span>
     </label>
						</div>
						
                    </div>
                <br/>
            </div>
          </li>
		  <?php }}?>
	  
      </ul>
    </div>
</div>
      </div>
  
</div>
    </div>
    
      <script>
        $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'tab_id');
  });
      </script>
	<!--  </div>-->

<br>
<br>
<br>
<br>
<br>
<br>
</main>
<?php
	require_once("helper/footer.php");
?>