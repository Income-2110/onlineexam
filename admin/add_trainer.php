<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
     require_once("helper/header.php");
	 require_once("helper/nav.php");
?>
  <main>
	<div class="container">
	<div class="col l3 z-depth-1 card-panel  blue-color" >
	<h1>Add Trainer Details</h1>
	</div>
	<div class="col l3 z-depth-6 card-panel">
	Name :<input  id="new_trainer_name" type="text" required/><br>
	Ph No:<input id="new_trainer_ph" type="text" required><br>
	Email<input id="new_trainer_email" type="email" required /><br>
	<button type="submit" id="add_trainer_btn">Add Trainer</button></br>
	</div>
	</div>
	

	</main>
<?php
     require_once("helper/footer.php");
?>