// Custom JS file

//login for manager
$("#login_btn").click(function() {
	
	var username = $("#TraineePsid").val();
	var password = $("#password").val();
	
//	alert(username + " " + password);
	$.post("ajax/login.php",
	{
		userid : username,
		passwd : password 
	}, function(response)
	{
		if(response == "Success")
		{
			// correct password
		//alert("welcome");
		//document.load("onlineexam/admin/dashboard1.php");
		// document.load("accountSettings.php");
	window.location.replace("dashboard.php");
		}
		else
		{
			// wrong password
			alert("Invalid username or password");
			
		}
		
	});
});
	
	//accountSettings for manager : new password
	
$("#submit_btn").click(function() {
	
	//var name=$("#first_name").val();
	var newPass=$("#new_password").val();
	var oldPass=$("#old_password").val();
	var cnfrmPass=$("#cnfirm_password").val();
	
	if(newPass == "" || oldPass == "" || cnfrmPass == "")
	{
		alert("Password fields cannot be blank");
		return false;
	}
	
	if(newPass!=cnfrmPass)
	{
		alert("Passwords do not match");
		return false;
	}
	
	
	//checking password constraints
	if(newPass.length < 8 )
	{
		alert("New Password should atleast be 8 characters long");
		return false;
	}
	
	var upper = 0;
	var lower = 0;
	var number = 0;
	var symbol = 0;
	
	for(var i=0; i<newPass.length ; i++)
	{
		if (newPass[i].match(/[A-Z]/g)) {upper++;}
		if (newPass[i].match(/[a-z]/g)) {lower++;}
        if (newPass[i].match(/[0-9]/g)) {number++;}
        if (newPass[i].match(/(.*[!,@,#,$,%,^,&,*,?,_,~])/)) {symbol++;} 
	}
	
	if(upper == 0)
	{
		alert("Password should contain atleast 1 upper case character");
		return false;
	}
	if(lower == 0)
	{
		alert("Password should contain atleast 1 lower case character");
		return false;
	}
	if(number == 0)
	{
		alert("Password should contain atleast 1 numeric character");
		return false;
	}
	if(symbol == 0)
	{
		alert("Password should contain atleast 1 special character");
		return false;
	}
	
	
	
	$.post("ajax/accountSettings.php",
	{
		oldPswd:oldPass,
		newPswd:newPass,
		cnfrmPswd:cnfrmPass
		
	}, function(response)
	{
		if(response == "Success")
		{
			// correct password
		alert("Password updated");
		$("#new_password").val('');
		$("#old_password").val('')
		$("#cnfirm_password").val('');
		//document.load("onlineexam/admin/dashboard1.php");
		//window.location.replace("dashboard1.php");
		}
		else
		{
			// wrong password
			alert(response);
			
		}
		
	}); 
});






// add trainee request
$("#add_trainee_btn").click(function() {
	
	var traineeId = $("#new_trainee_id").val();
	
	
	alert(traineeId);
	$.post("ajax/addtrainee.php",
	{
		tId : traineeId,
		
	}, function(response)
	{
		if(response == "Success")
		{
			// correct password
		  alert("Trainee " + traineeId + " added.");
		  $("#new_trainee_id").val('');
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	});
});


// add trainee list
$("#add_trainee_btn2").click(function() {
	
	var traineeIdLst = $("#new_trainee_idtxt").val();
	
	
	//alert(traineeIdLst);
	$.post("ajax/addtraineeLst.php",
	{
		tIdLst : traineeIdLst,
		
	}, function(response)
	{
		if(response == "Success")
		{
			// correct password
		  alert("Trainee " + traineeIdLst + " added.");
		  $("#new_trainee_idLst").val('');
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	});
});


// add trainer request
$("#add_trainer_btn").click(function() {
	
	
	var trainerName = $("#new_trainer_name").val();
	var trainerPh = $("#new_trainer_ph").val();
	var trainerEmail = $("#new_trainer_email").val();
	
	if(trainerName == "" || trainerPh== "" || trainerEmail == "")
	{
		alert("Please fill in all the details");
		return false;
		
	}

	$.post("ajax/addtrainer.php",
	{
		trainerName : trainerName,
		trainerPh : trainerPh,
		trainerEmail : trainerEmail,
		
	}, function(response)
	{
		if(response == "Success")
		{
			
		  alert("Trainer " + trainerName + " added.");
		  $("#new_trainer_Name").val('');
		}
		else
		{
			alert("Error adding trainer");
		}
		
	});
});

//add trainer list

$("#add_trainer_btn2").click(function() {
	
	var trainerIdLst = $("#new_trainer_idtxt").val();
	
	
	alert(trainerIdLst);
	$.post("ajax/addtrainerLst.php",
	{
		trIdLst : trainerIdLst,
		
	}, function(response)
	{
		if(response == "Success")
			
		{
			// correct password
		  alert("Trainer " + trainerIdLst + " added.");
		  $("#new_trainer_idLst").val('');
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	});
});


//getting cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

//barring trainee
$(".traineebarringToggle").change(function() {
	
	//$("#barred_value").val($(".traineebarringToggle").val());
/* 	if ($(this).is(':checked')) {
    $(this).siblings('label').html('checked');
  } else {
    $(this).siblings('label').html(' not checked');
  } */
	var id = this.id;
	var id1 = $(this);
	var temp = id.split("_");
	var id = temp[1];
    var batchid = getCookie("batchid");	
	//if(id1.is(':checked'))
	//{
	$.post("ajax/baarredtrainee.php",
	{
		
		tid: id,
		bid: batchid,
		//barred : barr_value,
	
	}, function(response)
	{
		if(response == "Success")
			
		{
			// correct password
			if(id1.is(':checked'))
			{
		       alert("Trainee " + id + " barred.");	
			}
          else
		  {
	           alert("Trainee " + id + " allowed.");	
		  }
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	}); 
	//}
	

});



//allowing trainee

$(".traineeallowingToggle").change(function() {
	
	//alert("hello");
	var id = this.id;
	 //  alert(id);
	var id1 = $(this);
	var temp = id.split("_");
	var id = temp[1];

	$.post("ajax/allowedtrainee.php",
	{
		
		trid: id,
		//bid: batchid,
		//barred : barr_value,
	
	}, function(response)
	{
		if(response == "Success")
			
		{
			// correct password
			if(id1.is(':checked'))
			{
		       alert("Trainee " + id + " enabled.");	
			}
          else
		  {
	           alert("Trainee " + id + " disabled.");	
		  }
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	}); 
	
	

});

//allowing trainer



$(".trainerallowingToggle").change(function() {
	
	//alert("hello");
	var id = this.id;
	 //  alert(id);
	var id1 = $(this);
	var temp = id.split("_");
	var id = temp[1];

	$.post("ajax/allowedtrainer.php",
	{
		
		tid: id,
		//bid: batchid,
		//barred : barr_value,
	
	}, function(response)
	{
		if(response == "Success")
			
		{
			// correct password
			if(id1.is(':checked'))
			{
		       alert("Trainer " + id + " enabled.");	
			}
          else
		  {
	           alert("Trainer " + id + " disabled.");	
		  }
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	}); 
	
	

});

var bb;
var bb1;
//allowing batches

$(".batchallowingToggle").change(function() {
	
	//alert("hello");
	var id = this.id;
	 //  alert(id);
	var id1 = $(this);
	//var id2=this.id;
	var temp = id.split("_");
	var id2 = temp[1];

	$.post("ajax/allowedbatch.php",
	{
		
		bid: id2,
		//bid: batchid,
		//barred : barr_value,

	}, function(response)
	{
		if(response == "Success")
			
		{
			// correct password
			if(id1.is(':checked'))  //batch disabled....active to inactive
			{
		       alert("Batch " + id2 + " enabled.");	
			   bb = $("#"+id).parents("li");
               var list_id = bb.attr("id");
               var temp1 = list_id.split("_");
			  //  alert(temp1);
			   if(temp1[0]="active")
				 {
					 temp1[0]="inactive";
				 }
			  else
				 {
					 temp1[0]="active";
				 }	
				// alert(temp1);
					
			   $("#inactiveBatches .list-group").append(bb);
			 // bb.remove();
		
			}
          else
		  {
	           alert("Batch " + id + " disabled.");	
			   bb1 = $("#"+id).parents("li");
               var list_id1 = bb1.attr("id");
               var temp11 = list_id1.split("_");
			   if(temp11[0]="active")
				 {
					 temp11[0]="inactive";
				 }
			  else
				 {
					 temp11[0]="active";
				 }	
			   $("#activeBatches .list-group").append(bb1);
		  }
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	}); 
	
	

});



$("#btn_edit").click(function() {
	
	$("#comment_textarea").show();
	$("#btn_edit").hide();
	var a=location.search;
	//alert(a);
	var b=a.split("=");
	var temp=b[1];
	//alert(temp);
	$("#btn_update_"+temp).show();
	var comment = $("#comment_text").text();
//	alert(comment);
	$("#comment_textarea").val(comment);
	//$("#comment_textarea").toggle();
	/*
	$.post("ajax/addtrainerLst.php",
	{
		trIdLst : trainerIdLst,
		
	}, function(response)
	{
		
		
	});
	*/
});

$(".trainee_update_btn_class").click(function() {
	
	var id1 = this.id;
	 //  alert(id);
	//var id1 = $(this);
	var temp = id1.split("_");
	var id = temp[2];
	//alert(id);
	var new_comment = $("#comment_textarea").val();
	//$("#comment_textarea").val(comment);
	//$("#comment_textarea").toggle();
	//alert(new_comment);
	$.post("ajax/addNewComment.php",
	{
	comment:new_comment,
	trainee_id:id,
		
	}, function(response)
	{
		if(response=="Success")
		{
			alert("new comment added");
		    var updated_comment=$("#comment_textarea").val();
			//alert(updated_comment);
		    $("#comment_text").text(updated_comment);
	        $("#comment_textarea").hide();
			var a=location.search;
	        var b=a.split("=");
	        var temp=b[1];
	        $("#btn_update_"+temp).hide();
	        $("#btn_edit").show();
			
		}
		else
		{
			alert("new comment not added");
		}
		
	});
	
});


//for selector in add batch pageX
/* $(document).ready(function() {
	alert("I'm ready");
    $('select').material_select();
  }); */

// add batch details
// add trainer request
$("#add_batch_btn").click(function() {
		
	var batchName = $("#new_batch_name").val();
	var batchDate = $("#new_batch_date").val();
	var batchMarks = $("#new_batch_marks").val();
	var batchTrainer = $("#select_trainers_list").val();
	
	if(batchName == "" || batchDate== "" || batchMarks == "")
	{
		alert("Please fill in all the details");
		return false;
	}

	$.post("ajax/addbatch.php",
	{
		batchName  : batchName,
		batchDate  : batchDate,
		batchMarks : batchMarks,
		batchTrainer : batchTrainer
		
	}, function(response)
	{
		if(response == "Success")
		{
		  alert("Batch " + batchName + " Created.");
		  $("#new_batch_name").val('');
		  $("#new_batch_date").val('');
		  $("#new_batch_marks").val('');
		  
		}
		else
		{
			alert("Error creating batch");
		}
		
	});
});


// assigning batches
$("#assign_batches").click(function() {
	
	var batchId = $("#selectbatchId").val();
    var traineeIdLst = $("#traineeLst").val();
	//alert(traineeIdLst);
	$.post("ajax/assignBatchLst.php",
	{
		traineeIdLst : traineeIdLst,
		batchID : batchId
	}, function(response)
	{
		if(response == "Success")
		{
			// correct password
		  alert("Trainee " + traineeIdLst + " added.");
		  $("#new_trainee_idLst").val('');
		}
		else
		{
			// wrong password
			alert("Invalid username");
		}
		
	});
});