<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
require_once("helper/header.php");
require_once("helper/nav.php");
?>
<script  src="assets/js/Chart.js"></script>
    <script>
	//function to get random color 
	//for different institutes
	function getRandomColor() 
	{
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) 
		{
        color += letters[Math.floor(Math.random() * 16)];
		}
    return color;
	}
	
	//function to add labels i.e. Test1,Test2 etc on x-axis of chart
	function addLabels()
		{
		//alert("labels to be added");
		var Lst=getMarks();
		//alert(Lst[0].length);
		var labelLst=[];
		var x=0;
		for(i=0;i<Lst[0].length;i++)
		{
		var y=parseInt(i)+1;
		labelLst[i]="\"Test "+ y + "\"";
		}
		return labelLst;
		}
//creating chat data
  var marksData = {
            labels: addLabels(),//["Test 1 ","Test 2","Test 4 ","Test 4","Test 5 ","Test 6","Test 7 "],
			datasets: []
				};
				
		/*getMarks() returns a 2d array univMarks[i][j] with
		"i" being a TEST and "j" being average of marks of all
the students of "i" college in a particular test*/
		function getMarks()
		{
		//alert("Getmarks mein hu");
		var univMarks=[[40,57,46,75,34,87,56],[67,45,78,74,39,95,75],[57,75,98,73,30,35,55],[40,57,46,75,34,87,56],[67,45,78,74,39,95,75],[57,75,98,73,30,35,55],[40,57,46,75,34,87,56],[67,45,78,74,39,95,75],[57,75,98,73,30,35,55]];
		return univMarks;
		//alert(univMarks[0]);
		}
		//function to add data to chart
		function addAnotherData()
		{
		//var newdataset=[];
		var marksLst=getMarks();
		for(i=0;i<=marksLst.length;i++)
		{
		var newdataset=  {
            fillColor: getRandomColor(),
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: marksLst[i] 
        };
		marksData.datasets.push(newdataset);
		}
		myChart.destroy();
		var newChart=new Chart(marksId).Bar(marksData,{ showTooltips: true,
														animationEasing: "easeOutQuart",
														scaleShowLabels: true,
														responsive: true,
														maintainAspectRatio: false}
													);
		}
		</script>

<canvas id="canvasId" width="100" height="400"></canvas>
	<script>
		 var marksId = document.getElementById('canvasId').getContext('2d');
		var myChart=new Chart(marksId).Bar(marksData);
		window.onload=function(){
		addAnotherData();
		};
    </script>
<?php
require_once("helper/footer.php");
?>