<?php
     require_once("helper/helper.php");
	 
	 if(is_logged_in())
	 {
		 header("Location:dashboard.php");
	 }
	 require_once("helper/header.php");
	 require_once("helper/nav.php");
?>
    <div class="container" style="margin-left: 50%;	margin-right: 50%;">
      <div class="row">

				<div class="col l4 z-depth-6 card-panel" style="padding-top: 2%;
				padding-left:2%;height: 300px; width: 300px;">
							                <div class="blue panel-heading white-text adminhead">Admin Login</div>

				 <div class="input-field col l7">
              <input name="psid_txt" id="TraineePsid" type="text" class="validate">
              <label for="first_name" class="">PSID</label>
            </div>

          <div class="row">
            <div class="input-field col l7">
              <input name="passwd_txt" id="password" type="password" class="validate">
              <label for="password" class="">Password</label>
            </div>
          </div>
       <button id="login_btn" class="waves-effect waves-light btn">Login</button>
            <br>
              <br>
            </div>
      </div>
    </div>
<?php
     require_once("helper/footer.php");
?>