<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
	require_once("helper/header.php");
	require_once("helper/nav.php");
   
   $trainer_id=$_GET['trainerid'];
   $trainer_details=retrieve_trainerDataByid($trainer_id);
?>
<main>
<!--styling of toggle -->
    <style>
    .switch {
    position: relative;
    display: block;
    vertical-align: top;
    width: 100px;
    height: 30px;
    padding: 3px;
    margin: 0 10px 10px 0;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
    border-radius: 18px;
    box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    }
    .switch-input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    }
    .switch-label {
    position: relative;
    display: block;
    height: inherit;
    font-size: 10px;
    text-transform: uppercase;
    background: #eceeef;
    border-radius: inherit;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    }
    .switch-label:before, .switch-label:after {
    position: absolute;
    top: 50%;
    margin-top: -.5em;
    line-height: 1;
    -webkit-transition: inherit;
    -moz-transition: inherit;
    -o-transition: inherit;
    transition: inherit;
    }
    .switch-label:before {
    content: attr(data-off);
    right: 11px;
    color: #aaaaaa;
    text-shadow: 0 1px rgba(255, 255, 255, 0.5);
    }
    .switch-label:after {
    content: attr(data-on);
    left: 11px;
    color: #FFFFFF;
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label {
    background: #E1B42B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
    }
    .switch-input:checked ~ .switch-label:before {
    opacity: 0;
    }
    .switch-input:checked ~ .switch-label:after {
    opacity: 1;
    }
    .switch-handle {
    position: absolute;
    top: 4px;
    left: 4px;
    width: 28px;
    height: 28px;
    background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
    background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
    border-radius: 100%;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
    }
    .switch-handle:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -6px 0 0 -6px;
    width: 12px;
    height: 12px;
    background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
    background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
    border-radius: 6px;
    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
    }
    .switch-input:checked ~ .switch-handle {
    left: 74px;
    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
    }
     
    /* Transition
    ========================== */
    .switch-label, .switch-handle {
    transition: All 0.3s ease;
    -webkit-transition: All 0.3s ease;
    -moz-transition: All 0.3s ease;
    -o-transition: All 0.3s ease;
    }
    </style>
<div>
    <h1>Trainer Details</h1>
<span>Name : <?php echo $trainer_details["name"];?></span><br/><br/>
<span>Phone no : <?php echo $trainer_details["ph_no"];?></span><br/><br/>
<span>Profile Pic : <?php echo $trainer_details["profile_pic"];?></span><br/><br/>
<span>Email : <?php echo $trainer_details["email"];?></span><br/><br/>
<span>
<label class="switch switch-left-right">
    <input id="trainerallowingToggle_<?php echo $trainer_details["trainer_id"];?>" class="switch-input trainerallowingToggle"  type="checkbox" <?php if($trainer_details["active"]==1){ echo "checked=\"checked\"";} ?> />
    <span class="switch-label" data-on="Enable" data-off="Disable"></span>
    <span class="switch-handle"></span>
     </label><br/><br/>
	 </span>
</div>
</main>
<?php
require_once("helper/footer.php");
?>