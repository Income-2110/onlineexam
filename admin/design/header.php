<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<link href="assets/import.css" rel="stylesheet">
	<link href="css/simple-sidebar.css" rel="stylesheet">
    <style>
	/*Header*/     
        .img{
            width: 145px;
            height: 80px;
        } 
        .navbarlink{
            margin-top:17%;
        }    
        .drop{
            background: white;
            color: #48A7F2;
            box-shadow: none;

            padding: 0;
            padding-right: 30%;
        }
        .drop-btn{
            margin-top: 20%;
        }
        .drop-btn:hover{
            background: white;
            box-shadow: none;
        }
        .dropdown-content li>a, .dropdown-content li>span{
            color: #48A7F2;
        }
        .pagesection{
            border-bottom: 1px solid #ebebeb;
        }
        .row1{
            position: fixed;
            margin-left: -2%;
            z-index: 10000;
            background-color: white;
        }
        /*End Header*/
	</style>
</head>
    
    
<body class="header1">   
   <div class="panel panel-default col 16">  
       <div class="page-section half bg-white pagesection">  
           <div class="collapse navbar-collapse nav-main" id="main-nav">
               <ul class="nav navbar-nav">
                   <li>  <img class="img" src="Images/itclogo.jpg" width="320px" height="120px"></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                   <li class="tab col 13 navbarlink">  <a href="#" ><span class="glyphicon glyphicon-user"></span>Trainee</a></li>
                   <li class="tab col l3 navbarlink">  <a href="#"><span class="glyphicon glyphicon-user"></span>Trainer</a></li>
                   <li class="tab col l3 navbarlink">   <a href="#"><span class="glyphicon glyphicon-user"></span>Admin</a></li>                        
                </ul>
           </div>
       </div> 
    </div>
    