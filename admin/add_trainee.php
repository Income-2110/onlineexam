<?php
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
     require_once("helper/header.php");
	 require_once("helper/nav.php");
?>
	<main>
	<div class="container">
	
	<div class="col l3 z-depth-1 card-panel  blue-color" >
	<h1>Enter Trainee PSIDs</h1></div>
	Enter the PSIDs separated by commas. eg: 21220,21221
	<textarea id="new_trainee_idtxt" rows="20" cols="50"></textarea>
	<button type="submit" id="add_trainee_btn2"  name="add1">Add Trainees</button>
	</div>
	</main>
<?php
     require_once("helper/footer.php");
?>