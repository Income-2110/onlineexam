<?php
 
     require_once("../helper/helper.php");
	// $name=$_POST['admin_name'];
	 $oldPass=$_POST['oldPswd'];
	 $newPass=$_POST['newPswd'];
	 $cnfrmPass=$_POST['cnfrmPswd'];

	 if($newPass!=$cnfrmPass)
	 {
		 echo "New Passwords do not match";
	 }
	
	 if(!check_pass($oldPass))
	 {
		 echo "Old password do not match. Please try again";
	 }
	 else
	 {
	    if(changePass($newPass))
	    {
		  echo "Success";
	    }
	   else
	   {
		 echo "Error updating the password";
	   }
	 }
?>