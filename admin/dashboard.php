<?php
  
  require_once("helper/helper.php");
  if(!is_logged_in())
  {
	  header("Location: login.php");
  }
  
  require_once("helper/header.php");
  require_once("helper/nav.php");
?>
<style>
   html
   {
       font-size:10px;
   }
    </style>
<main>
  <div class="container" style="width:100%" >
     
    <div class="row" >
        <div class="col s12 m12 l6" style="margin-top:auto">
			<!--Graph -->
			<div class="panel-heading graph_title">
				<h1>Welcome <?php echo $_SESSION['name'];?></h1>
			</div>
			<!--<div id="graph_container" class="card-panel graphpanel graph_style" style="height:300pt" >
				<canvas id="buyers" class="testresult_graph"></canvas>  
			</div>-->
          </div>
    <div class="col s12 m12 l6"  style="overflow:auto" >
           
                        <!-- List group -->
						<ul class="list-group" >
							<div class="row" style="height:50%;";>
                               <div class="col l6" style=" ">
                                 <div class="panel panel-default testpannel">
                                    <div class="panel-heading Upcoming-Tests"> 
                                     <h2 class="panel-title Upcoming-Tests-title">Upcoming Tests</h2>
                                 </div>
                                    <ul class="list-group">
                                      <li class="list-group-item tests_list">
                                           <div class="media-body">
                                            <h4 class="media-heading">C#</h4>
                                              <div class="tests-display">25 marks test - both objective as well as subjectives</div>
                                               </div>
                                            <br/>
                                           </div>
                                </li>
                                <li class="list-group-item tests_list">
                                   
                                        <div class="media-body">
                                            <h4 class="media-heading">SQL</h4>
                                            <div class="rank-display">50 marks test - only subjective</div>
                                        </div>
                                        <br/>
                                    </div>
									
									
									<div class="col l6" style=" ">
                        <div class="panel panel-default testpannel">
                            <div class="panel-heading Upcoming-Tests"> 
                                <h2 class="panel-title Upcoming-Tests-title">Recent Tests Details</h2>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item tests_list">
                                            <div class="media-body">
                                            <h4 class="media-heading">HTML</h4>
                                            <div class="tests-display">Was conducted on 12th December</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                                <li class="list-group-item tests_list">
                                   
                                        <div class="media-body">
                                            <h4 class="media-heading">ADO.Net</h4>
                                            <div class="rank-display">Was conducted on 27th December</div>
                                        </div>
                                        <br/>
                                    </div>
									
									
                                </li>
                               
                            </ul>
                        </div>
						</ul>
                    </div>    
						
               
            
     <div class="col s12 m12 l6"  style="overflow:auto" >
                        <!-- List group -->
						<ul class="list-group" >
							<div class="row" style="height:50%; ">
                    <div class="col l6" style=" ">
                        <div class="panel panel-default testpannel">
                            <div class="panel-heading Upcoming-Tests"> 
                                <h2 class="panel-title Upcoming-Tests-title">Approval Requests</h2>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item tests_list">
                                            <div class="media-body">
                                            <h4 class="media-heading">WCF and WPF</h4>
                                            <div class="tests-display">Approval of questions for test which is scheduled on 4th January</div>
                                        </div>
                                        <br/>
                                    </div>
                                </li>
                                                               
                            </ul>
                        </div>
						</ul>
                    </div>    
						
                </div>
            </div>
		

    
    
  <!-- <div class="container" style="border:1px solid black">
     
        <div class="row" style="height:50%">
        <div class="col s12 m12 l6" style="border:1px solid red;height:100%;margin-top:auto">
             <div class="panel panel-default testpanel" style="width:100%">
                            <div class="panel-heading score-header">Test Scores</div>
                            <!-- List group 
                            <ul class="list-group">
                                <li class="list-group-item score_list"><h5 class="test_name_header">Cras justo odio</h5>
                                    <div class="score-display">11</div>
                                </li>
                                <li class="list-group-item score_list">Cras justo odio
                                    <p class="score-display">25</p>
                                </li>
                            </ul>
                        </div> 
              </div>
              <div class="col s12 m12 l6" style="border:1px solid red;height:100%">
                  <div class="card-panel graphpanel">
                                    
                                            <canvas id="buyers" class="testresult_graph"></canvas>
                  </div>
              </div>
           
              </div>
              </div>
    <div class="row" style="height:50%">
        <div class="col s12 m12 l6" style="border:1px solid red;height:100%;margin-top:auto">
            
        </div>
		<div class="col s12 m12 l6" style="border:1px solid red;height:100%">
					  
		</div>
        
    </div>
        </div>
              
        </div> -->
</main>

<?php
  require_once("helper/footer.php");
?>