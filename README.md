# README #

### How do I start? ###

* Click on Clone (left sidebar, under ACTIONS)
* Copy the HTTPS url
* Open Git bash at C:\xampp\htdocs\
* Paste the url and click enter
* The repository will be created in a folder called onlineexam. If xampp is running, you can run the code at [localhost/onlineexam](http://localhost/onlineexam/)

### Setup local (Do this only once) ###
* Open git bash in C:\xampp\htdocs\onlineexam
* Name **git config user.name "<your name>"**
* Email **git config user.email "<your email>"**
* Setup the origin, copy the https url at the top right of the page **git remote add origin <paste the url here>**
* If there is an error, then use origin1 instead. And use origin1 while push and pull also
### What is this repository for? ###

This repository is only for the developers of the online exam portal.
You are expected to have a copy of this repo in your local system, make changes to it, and put the put the changes back to this online repo.

### Contribution guidelines ###

* Make changes to your part of the code locally (in your system)
* Open git bash in C:\xampp\htdocs\onlineexam
* Add to staging area ** git add --all** 
* Commit with meaningful commit messages **git commit -m "<some meaningful message>"**
* Pull the latest version of the code **git pull origin master**
* Merge your changes to it. A text editor opens, click **esc** key and type **:wq**
* Push back to the online repository **git push origin master**. You may be asked to give your password again.

### Who do I talk to? ###

* Search engine - Google
* Repo owner